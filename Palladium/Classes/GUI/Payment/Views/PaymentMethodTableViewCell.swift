//
//  PaymentMethodTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {
    
    @IBOutlet weak var typeImgView: UIImageView!
    @IBOutlet weak var numberLbl: UILabel!

    func adjustTo(card: PaymentCard) {
        typeImgView.image = UIImage(cardType: card.type)
        numberLbl.text = card.number
    }

}
