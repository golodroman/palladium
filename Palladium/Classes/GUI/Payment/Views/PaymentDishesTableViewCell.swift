//
//  PaymentDishesTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PaymentDishesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var waveView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        waveView.image = UIImage(asset: .Line_Wave).resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .tile)
    }

    func adjustTo(payment: Payment?) {
        guard let payment = payment else {return}
        stackView.clear()
        for session in payment.detail?.sessions ?? [] {
            for dish in session.dishes ?? [] {
                let view: PaymentDishView = UIView.fromXIB()
                view.adjustTo(dish: dish)
                stackView.addArrangedSubview(view)
            }
        }
    }

}
