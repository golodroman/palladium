//
//  PaymentTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PaymentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var dropdownBtn: UIButton!
    
    var isExpanded: Bool = false {
        didSet {
            dropdownBtn.transform = CGAffineTransform(rotationAngle: CGFloat(isExpanded ? Double.pi : 0.0))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dropdownBtn.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
    }
    
    func adjustTo(payment: Payment?) {
        guard let payment = payment else {return}
        let string = NSMutableAttributedString()
        string.append(NSAttributedString(string: payment.date.string(style: .medium) + ", ", attributes: [
            .font           : UIFont(name: "AvenirNext-Medium", size: 14)!,
            .foregroundColor: UIColor.darkGray
            ]))
        string.append(NSAttributedString(string: "№" + (payment.detail?.name ?? "") + " ", attributes: [
            .font           : UIFont(name: "AvenirNext-Medium", size: 14)!,
            .foregroundColor: "D0AD72".toUIColor()
        ]))
        infoLbl.attributedText = string
    }
    
}
