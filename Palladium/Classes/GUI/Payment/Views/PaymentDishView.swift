//
//  PaymentDishView.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PaymentDishView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    func adjustTo(dish: PaymentDish) {
        titleLabel.text = dish.name
        textLabel.text = Double(dish.amount).priceString(currency: "KZT".currencySymbol)
    }

}
