//
//  PaymentDetailsViewController.swift
//  Palladium
//
//  Created by Igor Danich on 9/6/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PaymentDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var logoView: MediaView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var bonusesTxtFld: UITextField!
    @IBOutlet weak var bonusesSlider: UISlider!
    @IBOutlet weak var bonusesLeftLbl: UILabel!
    
    @IBOutlet weak var bonusesOwnedLbl: UILabel!
    @IBOutlet weak var amountToPayLbl: UILabel!
    
    @IBOutlet weak var discountTxtLbl: UILabel!
    
    fileprivate var isExpanded = true
    fileprivate let queue = OperationQueue()
    fileprivate var currency = ""
    
    var service: PaymentService!
    fileprivate let cards = PaymentCardsService()

    override func viewDidLoad() {
        super.viewDidLoad()
        makeBackButton()
        
        queue.maxConcurrentOperationCount = 1
        
        logoView.url = service.payment?.place.logo
        titleLbl.text = service.payment?.place.name?.uppercased()
        detailLbl.text = service.payment?.place.categories?.first?.name
        addressLbl.text = service.payment?.place.address
        currency = service.payment.quote.currencySymbol
        
        amountLbl.text = Double(service.payment.orderSum - service.payment.discountSum).priceString(currency: currency)
        let discount = Double(service.payment.detail?.discountSum ?? 0)
        discountLbl.text = (discount < 0 ? -discount : discount).priceString(currency: currency)
        bonusesSlider.value = 0
        if service.maxBonus < 1 {
            bonusesSlider.isEnabled = false
        }
        if service.payment.discountSum > 0 {
            discountTxtLbl.text = "pay.details.service".localized
        }
        reloadData()
    }
    
    fileprivate func reloadData(updateBonuses: Bool = true) {
        let maxBonus = Double(service.maxBonus)
        var bonusesSliderValue = Double(bonusesSlider.value)
        var bonus = maxBonus*bonusesSliderValue
        let orderSum = service.payment.orderSum
        let quoteBonus: Double = service.payment.quote.bonus
        
        let discount = Double(service.payment.detail?.discountSum ?? 0)
        
        if discount > 0 && bonus > Double(orderSum) - discount {
            let margin = (discount/maxBonus)/2
            let amountValue = (Double(orderSum) - discount)/maxBonus
            if Float(amountValue + margin) < bonusesSlider.value {
                bonus = Double(orderSum)
            } else {
                bonus = Double(orderSum) - discount
            }
            bonusesSlider.value = Float(bonus/maxBonus)
        }
        bonusesSliderValue = Double(bonusesSlider.value)
        let value = bonus.priceString(currency: currency)
        bonusesTxtFld.text = value
        
        let left = (maxBonus*(1.0 - bonusesSliderValue)).priceString(currency: currency)
        let owned = (quoteBonus*Double((orderSum - Int(bonus)))).priceString(currency: currency)
        let amount = (Double(orderSum) - bonus).priceString(currency: currency)
        
        bonusesLeftLbl.text = left
        bonusesOwnedLbl.text = owned
        amountToPayLbl.text = amount
    }
    
    @IBAction func onBonusesChanged(_ : AnyObject?) {
        if bonusesTxtFld.isFirstResponder {
            bonusesTxtFld.resignFirstResponder()
        }
        reloadData()
    }
    
    fileprivate func pay(card: PaymentCard?) {
        showLoading()
        service.pay(card: card, bonuses: Int(Double(service.maxBonus)*Double(bonusesSlider.value))) { [weak self](error) in
            self?.hideLoading()
            if error == nil || error?.code == 200 {
                let ctrl = StoryboardScene.Payment.instantiatePaymentSuccess()
                ctrl.bonuses = self?.service.result?.balanceEarnings.priceString() ?? "0"
                ctrl.onResult = { [weak self] in
                    self?.navigationController?.popToRootViewController(animated: false)
                }
                self?.present(ctrl, animated: false, completion: nil)
            } else {
                let ctrl = StoryboardScene.Payment.instantiatePaymentFailed()
                ctrl.onResult = { [weak self] result in
                    switch result {
                    case .cancel:   break
                    case .retry:    self?.showMethods()
                    }
                }
                self?.present(ctrl, animated: false, completion: nil)
            }
        }
    }
    
    fileprivate func showMethods() {
        let ctrl = StoryboardScene.Payment.instantiatePaymentMethod()
        ctrl.service = cards
        ctrl.onResult = { [weak self] result in
            switch result {
            case .cancel:           break
            case .cash:             self?.pay(card: nil)
            case .card(let card):   self?.pay(card: card)
            }
        }
        present(ctrl, animated: false, completion: nil)
    }
    
    @IBAction func onSubmit(_ : AnyObject?) {
        if cards.items.count == 0 {
            showLoading()
            cards.fetch { [weak self]_ in
                self?.hideLoading()
                self?.showMethods()
            }
        } else {
            showMethods()
        }
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isExpanded ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: PaymentTableViewCell = tableView.dequeueReusableCell(identifier: "info", indexPath: indexPath)
            cell.isExpanded = isExpanded
            cell.adjustTo(payment: service.payment)
            return cell
        case 1:
            let cell: PaymentDishesTableViewCell = tableView.dequeueReusableCell(identifier: "dishes", indexPath: indexPath)
            cell.adjustTo(payment: service.payment)
            return cell
        default: return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:     return 50
        case 1:     return UITableViewAutomaticDimension
        default:    return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        isExpanded = !isExpanded
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
}

extension PaymentDetailsViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = ((textField.text ?? "") as NSString).replacingCharacters(in: range, with: string)
        if text.toDouble() > service.maxBonus.toDouble()/100 {
            textField.text = service.maxBonus.priceString()
            bonusesSlider.value = 1
            
        } else {
            textField.text = text
            bonusesSlider.value = Float((100.0*text.toDouble())/service.maxBonus.toDouble())
        }
        reloadData(updateBonuses: false)
        return false
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let bonus = Double(service.maxBonus)*Double(bonusesSlider.value)
        if bonus == 0 {
            textField.text = nil
        } else {
            textField.text = bonus.priceString().replacingOccurrences(of: " ", with: "")
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        reloadData()
    }
    
}
