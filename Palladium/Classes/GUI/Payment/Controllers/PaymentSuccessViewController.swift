//
//  PaymentSuccessViewController.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PaymentSuccessViewController: UIViewController {
    
    @IBOutlet weak var bonusesLbl: UILabel!
    
    var onResult: (() -> Void)?
    
    var bonuses: String = "0"

    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
        bonusesLbl.text = bonuses
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1
        }
    }
    
    @IBAction func onSubmit(_ : AnyObject?) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0
        }) { _ in
            self.dismiss(animated: false, completion: nil)
            self.onResult?()
        }
    }

}
