//
//  PaymentFailedViewController.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PaymentFailedViewController: UIViewController {
    
    enum Result {
        case cancel
        case retry
    }
    
    var onResult: ((_ result: Result) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1
        }
    }
    
    fileprivate func finish(result: Result) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0
        }) { _ in
            self.dismiss(animated: false, completion: nil)
            self.onResult?(result)
        }
    }
    
    @IBAction func onCancel(_ : AnyObject?) {
        finish(result: .cancel)
    }
    
    @IBAction func onRetry(_ : AnyObject?) {
        finish(result: .retry)
    }

}
