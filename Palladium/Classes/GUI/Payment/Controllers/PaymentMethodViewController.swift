//
//  PaymentTypeSelectViewController.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contentHeight: NSLayoutConstraint!
    
    var service: PaymentCardsService!
    
    enum Result {
        case cancel
        case cash
        case card(card: PaymentCard)
    }
    
    var onResult: ((_ result: Result) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
        tableView.reloadData()
        contentHeight.constant = tableView.originY + tableView.contentSize.height + 30
        view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1
        }
    }
    
    fileprivate func finish(result: Result) {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 0
        }) { _ in
            self.dismiss(animated: false, completion: nil)
            self.onResult?(result)
        }
    }
    
    @IBAction func onCancel(_ : AnyObject?) {
        finish(result: .cancel)
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return service.items.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < service.items.count {
            let cell: PaymentMethodTableViewCell = tableView.dequeueReusableCell(identifier: "card", indexPath: indexPath)
            cell.adjustTo(card: service.items[indexPath.row])
            return cell
        } else {
            return tableView.dequeueReusableCell(withIdentifier: "cash", for: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row < service.items.count {
            finish(result: .card(card: service.items[indexPath.row]))
        } else {
            finish(result: .cash)
        }
    }
    
}
