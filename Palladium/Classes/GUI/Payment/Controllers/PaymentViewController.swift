//
//  PaymentViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/31/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PaymentViewController: UIViewController {
    
    @IBOutlet weak var codeTxtFld: MaskTextField!
    
    fileprivate let service = PaymentService()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.tabBarItem.title = L10n.GeneralTextPayment.string
        codeTxtFld.font = UIFont(name: "AvenirNext-Medium", size: 24)!
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        codeTxtFld.text = "4360#3904"
//        onSubmit(nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func onSubmit(_ : AnyObject?) {
        view.endEditing(true)
        
//        let ctrl = StoryboardScene.Payment.instantiatePaymentDetails()
//        service.payment = Payment.test()
//        ctrl.service = service
//        navigationController?.pushViewController(ctrl, animated: true)
        
        showLoading()
        service.apply(code: codeTxtFld.text) { [weak self](error) in
            self?.hideLoading()
            self?.codeTxtFld.text = ""
            ShowAlert(error: error, inViewController: self)
            guard let service = self?.service else {return}
            if error == nil {
                if service.payment.amount == 0 || service.payment.status == "closed" {
                    let ctrl = StoryboardScene.Payment.instantiatePaymentSuccess()
                    ctrl.bonuses = (Double(service.payment.quote.bonus)*Double(service.payment.orderSum)).priceString(currency: service.payment.quote.currencySymbol)
                    self?.present(ctrl, animated: false, completion: nil)
                } else {
                    let ctrl = StoryboardScene.Payment.instantiatePaymentDetails()
                    ctrl.service = self?.service
                    self?.navigationController?.pushViewController(ctrl, animated: true)
                }
            }
        }
    }

}
