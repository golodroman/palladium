//
//  MainController.swift
//  Addzer
//
//  Created by Eugene Donov on 2/25/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class MainController: UIViewController {
    
    fileprivate var contentViewController: UIViewController?
    
    deinit {
        NotificationRemove(observer: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [
            .font             : UIFont(name: "AvenirNext-DemiBold", size: 14)!,
            .foregroundColor  : UIColor.darkGray
        ]
        UIBarButtonItem.appearance().setTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: -2), for: .default)
        UIBarButtonItem.appearance().setTitleTextAttributes([
            NSAttributedStringKey.foregroundColor  : UIColor.darkGray,
            NSAttributedStringKey.font             : UIFont(name: "AvenirNext-DemiBold", size: 14)!
        ], for: .normal)
        
        NotificationAdd(observer: self, name: UserService.didLogin) { [weak self]_ in
            self?.updateScreens(animated: true)
        }
        NotificationAdd(observer: self, name: UserService.didLogout) { [weak self]_ in
            self?.updateScreens(animated: true)
        }
        
        updateScreens(animated: false)
    }
    
    override var prefersStatusBarHidden: Bool {
        return contentViewController?.prefersStatusBarHidden ?? false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return contentViewController?.preferredStatusBarStyle ?? .default
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentViewController?.view.frame = view.bounds
    }
    
    fileprivate func updateScreens(animated: Bool) {
        let ctrl = UserService.shared.isAuthorized ? StoryboardScene.Main.instantiateMenu() : StoryboardScene.Authorization.instantiateWelcome()
        view.insertSubview(ctrl.view, at: 0)
        addChildViewController(ctrl)
        UIView.animate(withDuration: animated ? 0.2 : 0, animations: { 
            self.contentViewController?.view.alpha = 0
        }) { _ in
            self.contentViewController?.view.removeFromSuperview()
            self.contentViewController?.removeFromParentViewController()
            self.contentViewController = ctrl
        }
    }
    
}

extension UIViewController {
    
    var mainController: MainController {
        return UIApplication.shared.windows.first!.rootViewController as! MainController
    }
    
}
