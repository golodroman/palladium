//
//  MenuViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/21/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class MenuViewController: UIViewController, UINavigationControllerDelegate, UITabBarDelegate {
    
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var paymentBtn: UIButton!
    
    fileprivate var viewControllers = [UIViewController]() {
        didSet {
            var items = [UITabBarItem]()
            for ctrl in viewControllers {
                if let ctrl = ctrl as? UINavigationController {
                    _ = ctrl.viewControllers.first?.view
                    ctrl.delegate = self
                }
                items << ctrl.tabBarItem
            }
            tabBar.items = items
        }
    }
    
    fileprivate var selectedIndex: Int {
        get {
            if let item = tabBar.selectedItem {
                return tabBar.items?.index(of: item) ?? -1
            }
            return -1
        }
        set {
            tabBar.selectedItem = tabBar.items?[newValue]
            selectedViewController = viewControllers[safe: newValue]
        }
    }
    
    fileprivate var _selectedViewController: UIViewController?
    fileprivate var selectedViewController: UIViewController? {
        get {
            return viewControllers[safe: selectedIndex]
        }
        set {
            _selectedViewController?.removeFromParentViewController()
            _selectedViewController?.view.removeFromSuperview()
            _selectedViewController = nil
            if let ctrl = newValue {
                ctrl.view.frame = CGRect(x: 0, y: 0, width: view.width, height: view.height - tabBar.height)
                ctrl.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                addChildViewController(ctrl)
                view.insertSubview(ctrl.view, at: 0)
                _selectedViewController = ctrl
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBarItem.appearance().setTitleTextAttributes([
            .font             : UIFont(name: "HelveticaNeue", size: 11)!,
            .foregroundColor  : "AAAAAA".toUIColor()
        ], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([
            .font             : UIFont(name: "HelveticaNeue", size: 11)!,
            .foregroundColor  : UIColor.black
        ], for: .selected)
        viewControllers = [
            StoryboardScene.Places.initialViewController(),
            StoryboardScene.Promo.initialViewController(),
            StoryboardScene.Payment.initialViewController(),
            StoryboardScene.History.initialViewController(),
            StoryboardScene.Profile.initialViewController()
        ]
        view.addSubview(paymentBtn)
        
        paymentBtn.isSelected = true
        selectedIndex = 2
    }
    
    @IBAction func onPayment(_ : AnyObject?) {
        paymentBtn.isSelected = true
        selectedIndex = 2
    }
    
    // MARK: - UITabBarDelegate
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        selectedViewController = viewControllers[safe: tabBar.items?.index(of: item) ?? -1]
        paymentBtn.isSelected = item == tabBar.items?[2]
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return (selectedViewController as? UINavigationController)?.viewControllers.last?.preferredStatusBarStyle ?? .default
    }
    
    // MARK: - UINavigationControllerDelegate
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        UIView.animate(withDuration: 0.1) { 
            if navigationController.viewControllers.first == viewController {
                self.paymentBtn.alpha = 1
                self.tabBar.alpha = 1
                self.selectedViewController?.view.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height - self.tabBar.height)
            } else {
                self.paymentBtn.alpha = 0
                self.tabBar.alpha = 0
                self.selectedViewController?.view.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height)
            }
        }
    }
    
}
