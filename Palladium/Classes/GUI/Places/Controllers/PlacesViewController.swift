//
//  PlacesViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/21/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PlacesViewController: UIViewController {
    
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var toolbar: UIToolbar!
    
    fileprivate var isList = true
    
    fileprivate let service = PlacesService()
    
    fileprivate var listCtrl: PlacesListViewController {
        return (childViewControllers.makeArray { (_, ctrl) in
            return ctrl is PlacesListViewController ? ctrl : nil
        } as! [PlacesListViewController]).first!
    }
    
    fileprivate var mapCtrl: PlacesMapViewController {
        return (childViewControllers.makeArray { (_, ctrl) in
            return ctrl is PlacesMapViewController ? ctrl : nil
            } as! [PlacesMapViewController]).first!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.tabBarItem.title = L10n.PlacesAroundTitle.string
        navigationController?.tabBarItem.image = UIImage(asset: .Menu_Places)
        navigationController?.tabBarItem.selectedImage = UIImage(asset: .Menu_Places_Selected)
        
        toolbar.setShadowImage(UIImage.draw(size: toolbar.size) { _,_ in}, forToolbarPosition: .any)
        
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imgView.image = UIImage(asset: .Palladium_Logo)
        imgView.contentMode = .scaleAspectFit
        navigationItem.titleView = imgView
        
        listCtrl.service = service
        mapCtrl.service = service
        listCtrl.reload(clear: true)
        adjustLayout(animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = L10n.PlacesAroundTitle.string
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    fileprivate func adjustLayout(animated: Bool) {
        var button: UIBarButtonItem!
        if isList {
            button = UIBarButtonItem(image: UIImage(asset: .Places_Map).withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(PlacesViewController.onSwitch))
            listView.originY = view.height - listView.height/2
            listCtrl.refresh()
            view.sendSubview(toBack: listView)
        } else {
            button = UIBarButtonItem(image: UIImage(asset: .Places_List).withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(PlacesViewController.onSwitch))
            mapView.originY = 64 - mapView.height/2
            mapCtrl.refresh()
            view.sendSubview(toBack: mapView)
        }
        toolbar.setItems([UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), button], animated: animated)
        UIView.animate(withDuration: 0.25) {
            if self.isList {
                self.listView.originY = 64
                self.listView.alpha = 1
                self.mapView.alpha = 0
            } else {
                self.mapView.originY = 64
                self.mapView.alpha = 1
                self.listView.alpha = 0
            }
        }
    }
    
    @objc func onSwitch() {
        isList = !isList
        adjustLayout(animated: true)
    }

}
