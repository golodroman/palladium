//
//  PlaceGalleryViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PlaceGalleryViewController: UIViewController, PlaceContainerAdjustable, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var place: Place!
    var onHeightChanged: ((CGFloat) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.reloadData()
        Timer.scheduled(0.1) { [weak self]_ in
            guard let height = self?.collectionView.contentSize.height else {return}
            self?.view.height = height
            self?.onHeightChanged?(height)
        }
    }
    
    // MARK: - UICollectionViewDelegate, UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return place.images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PlaceGalleryCollectionViewCell = collectionView.dequeueReusableCell(identifier: "cell", indexPath: indexPath)
        cell.mediaView.url = place.images?[safe: indexPath.row]?.file
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let ctrl = GalleryViewController()
        ctrl.items = place.images?.makeArray { (_, image) in
            return image.file
        } ?? []
        ctrl.currentIndex = indexPath.row
        present(UINavigationController(rootViewController: ctrl), animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.width - 42)/3, height: 0.72*((collectionView.width - 42)/3))
    }

}
