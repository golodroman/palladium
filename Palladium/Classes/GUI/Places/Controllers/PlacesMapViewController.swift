//
//  PlacesMapViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/23/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI
import GoogleMaps

extension Place {
    
    var coordinates: CLLocationCoordinate2D? {
        guard let lat = addresses?.first?.latitude else {return nil}
        guard let lon = addresses?.first?.longitude else {return nil}
        return CLLocationCoordinate2D(latitude: Double(lat), longitude: Double(lon))
    }
    
    var marker: GMSMarker? {
        if let coordinates = coordinates {
            let marker = GMSMarker(position: coordinates)
            marker.userData = self
            return marker
        }
        return nil
    }
    
}

class PlacesMapViewController: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var mediaView: MediaView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var categoryImgView: MediaView!
    @IBOutlet weak var infoPosition: NSLayoutConstraint!
    
    fileprivate var mapView : GMSMapView!
    fileprivate var markers = [GMSMarker]()
    fileprivate var selected: Place?
    
    var service: PlacesService?
    var place: Place?

    override func viewDidLoad() {
        super.viewDidLoad()
        makeBackButton()
        mapView = GMSMapView(frame: CGRect(x: 0, y: 0, width: view.width, height: view.height))
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        view.insertSubview(mapView, at: 0)
        refresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if parent == navigationController {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        mapView.settings.myLocationButton = true
        mapView.subviews.forEach { (view) in
            if NSStringFromClass(view.classForCoder) == "GMSUISettingsView" {
                view.subviews.forEach { (view) in
                    if view.accessibilityIdentifier == "my_location" {
                        if self.parent is UINavigationController {
                            view.center = CGPoint(x: self.view.width - 40, y: 40 + 64)
                        } else {
                            view.center = CGPoint(x: self.view.width - 40, y: 40)
                        }
                    }
                }
            }
        }
    }
    
    func refresh() {
        for marker in markers {
            marker.map = nil
        }
        markers = []
        var center: CLLocationCoordinate2D?
        if let location = LocationService.shared.location?.coordinate {
            center = location
        }
        var places = [Place]()
        
        if let service = service {
            places << service.places.items
        } else if let place = place {
            places << place
            title = place.name?.uppercased()
            if let coordinates = place.coordinates {
                center = coordinates
            }
        }
        for place in places {
            guard let marker = place.marker else {continue}
            marker.iconView = PlaceMarkerView(place: place)
            marker.map = mapView
            markers << marker
        }
        if let center = center {
            mapView.camera = GMSCameraPosition.camera(withTarget: center, zoom: 12)
        }
    }
    
    @IBAction func onAction(_:AnyObject?) {
        guard let place = selected else {return}
        let ctrl = StoryboardScene.Places.instantiatePlace()
        ctrl.place = place
        navigationController?.pushViewController(ctrl, animated: true)
    }
    
    // MARK: - GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let place = marker.userData as? Place else {return false}
        selected = place
        mediaView.url = place.background?.large
        titleLbl.text = place.name?.uppercased()
        detailLbl.text = place.typeName
        timeLbl.text = place.addresses?.first?.workTime
        categoryImgView.url = place.categoryIcon
        distanceLbl.text = place.distanceString
        infoPosition.constant = 5
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
        return true
    }
    
}
