//
//  PlaceReviewsViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PlaceReviewsViewController: UIViewController, PlaceContainerAdjustable {
    
    var place: Place!
    var onHeightChanged: ((CGFloat) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
