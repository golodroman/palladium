//
//  PlaceViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

protocol PlaceContainerAdjustable: NSObjectProtocol {
    var place: Place! {set get}
    var onHeightChanged: ((CGFloat) -> Void)? {set get}
}

enum PlaceSection: Int {
    case about
    case menu
    case gallery
    case reviews
    
    var text: String {
        switch self {
        case .about:    return L10n.GeneralTextAbout.string
        case .menu:     return ""
        case .gallery:  return "place.details.gallery".localized
        case .reviews:  return ""
        }
    }
    
}

class PlaceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lightBar: UINavigationBar!
    @IBOutlet weak var darkBar: UINavigationBar!
    
    fileprivate enum Row: Int {
        static let count = 2
        
        case location
        case contacts
        
        func cellHeight(place: Place) -> CGFloat {
            switch self {
            case .location: return 52
            case .contacts: return (place.addresses?.count ?? 0)*52.0 + 80.0
            }
        }
    }
    
    fileprivate var sections: [PlaceSection] = [.about, .gallery]
    fileprivate var selectedSection = 0
    fileprivate var viewControllers = [PlaceSection:UIViewController]()
    fileprivate var statusBarStyle = UIStatusBarStyle.default {
        didSet {
            if oldValue != statusBarStyle {
                mainController.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }
    
    var place: Place!

    override func viewDidLoad() {
        super.viewDidLoad()
        lightBar.clear()
        darkBar.topItem?.title = place.name
        title = place.name?.uppercased()
        makeBackButton()
        (tableView.tableHeaderView as? PlaceHeaderView)?.adjustTo(place: place)
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        statusBarStyle = .lightContent
        adjustNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        statusBarStyle = .default
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    @IBAction func onBack(_ : AnyObject?) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onOrder(_ : AnyObject?) {
        guard let phone = place.addresses?.first?.phones?.first else {return}
        UIApplication.shared.openURL(phone.phoneCallURL)
    }
    
    @IBAction func onFacebook(_ : AnyObject?) {
        if !UIApplication.shared.openURL(place.facebookIdURL) {
            UIApplication.shared.openURL(place.facebookURL)
        }
    }
    
    @IBAction func onInstagram(_ : AnyObject?) {
        UIApplication.shared.openURL(place.instagramURL)
    }
    
    fileprivate func viewController(section: PlaceSection) -> UIViewController {
        if let ctrl = viewControllers[section] {return ctrl}
        var ctrl: UIViewController!
        switch section {
        case .about:    ctrl = StoryboardScene.Places.instantiatePlaceAbout()
        case .gallery:  ctrl = StoryboardScene.Places.instantiatePlaceGallery()
        case .menu:     ctrl = StoryboardScene.Places.instantiatePlaceMenu()
        case .reviews:  ctrl = StoryboardScene.Places.instantiatePlaceReviews()
        }
        addChildViewController(ctrl)
        if let ctrl = ctrl as? PlaceContainerAdjustable {
            ctrl.place = place
            ctrl.onHeightChanged = { [weak self]_ in
                self?.tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
            }
        }
        viewControllers[section] = ctrl
        return ctrl
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let ctrl = segue.destination as? PlacesMapViewController else {return}
        ctrl.place = place
    }
    
    fileprivate func adjustNavigationBar() {
        let value = tableView.contentOffset.y - (tableView.tableHeaderView?.height ?? 0 - 64)
        let offset: CGFloat = 64
        if value < -offset {
            darkBar.alpha = 0
            lightBar.alpha = 1
            tableView.contentInset = UIEdgeInsets.zero
            statusBarStyle = .lightContent
        } else if value >= -offset && value <= 0 {
            darkBar.alpha = 1 - -1*value/offset
            lightBar.alpha = -1*value/offset
            statusBarStyle = .default
            tableView.contentInset = UIEdgeInsets.zero
        } else {
            statusBarStyle = .default
            darkBar.alpha = 1
            lightBar.alpha = 0
            tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        }
    }
    
    fileprivate func adjustBackImage() {
        guard tableView.contentOffset.y < 0 else {return}
        (tableView.tableHeaderView as? PlaceHeaderView)?.offset = tableView.contentOffset.y < 0 ? -tableView.contentOffset.y : 0
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:     return Row.count
        case 1:     return 1
        default:    return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch Row(rawValue: indexPath.row)! {
            case .location:
                let cell: PlaceLocationTableViewCell = tableView.dequeueReusableCell(identifier: "location", indexPath: indexPath)
                cell.adjustTo(place: place)
                return cell
            case .contacts:
                let cell: PlaceContactsTableViewCell = tableView.dequeueReusableCell(identifier: "contacts", indexPath: indexPath)
                cell.adjustTo(place: place)
                return cell
            }
        case 1:
            let cell: PlaceContainerTableViewCell = tableView.dequeueReusableCell(identifier: "container", indexPath: indexPath)
            cell.view = viewController(section: sections[selectedSection]).view
            return cell
        default: return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section == 1 else {return nil}
        var view: PlaceSectionsHeaderView? = tableView.dequeueReusableHeaderFooterView(withIdentifier: "sections") as? PlaceSectionsHeaderView
        if view == nil {
            view = PlaceSectionsHeaderView(reuseIdentifier: "sections")
        }
        view?.selectedIndex = selectedSection
        view?.sections = sections
        view?.onChange = { [weak self]index in
            guard self?.selectedSection != index else {return}
            self?.selectedSection = index
            self?.tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
            Timer.scheduled(0.05) { [weak self]_ in
                self?.adjustNavigationBar()
            }
        }
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 60.0 : 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:     return Row(rawValue: indexPath.row)!.cellHeight(place: place)
        case 1:     return viewController(section: sections[selectedSection]).view.height
        default:    return 0
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        adjustNavigationBar()
        adjustBackImage()
    }

}
