//
//  PlaceAboutViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PlaceAboutViewController: UIViewController, PlaceContainerAdjustable {
    
    @IBOutlet weak var textView: UITextView!
    var place: Place!
    var onHeightChanged: ((CGFloat) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.height = 100
        textView.text = place.additionalInfo
        textView.sizeToFit()
        view.height = textView.height + 32
    }
}
