//
//  PlacesListViewController.swift
//  Palladium
//
//  Created by Igor Danich on 10/13/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PlacesListViewController: UIViewController {

    @IBOutlet weak var sliderView: PlacesSliderView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var service: PlacesService!
    fileprivate let favorites = FavoritesService()
    fileprivate let refreshControl = UIRefreshControl()
    
    fileprivate var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(PlacesListViewController.onRefresh), for: .valueChanged)
        tableView.addSubview(refreshControl)
        sliderView.onSelect = { [weak self] index in
            guard let place = self?.favorites.places.items[safe: index] else {return}
            let ctrl = StoryboardScene.Places.instantiatePlace()
            ctrl.place = place
            self?.parent?.navigationController?.pushViewController(ctrl, animated: true)
        }
        favorites.fetch { [weak self]_ in
            if let places = self?.favorites.places.items {
                self?.sliderView.adjustTo(places: places)
            } else {
                self?.tableView.tableHeaderView?.height = 80
            }
        }
    }
    
    @objc func onRefresh() {
        reload(clear: true)
    }
    
    func refresh() {
        tableView.reloadData()
    }
    
    func reload(clear: Bool) {
        refreshControl.endRefreshing()
        if clear {
            tableView.alpha = 0
            indicator.startAnimating()
        }
        service.fetch(clear: clear) { [weak self]error in
            ShowAlert(error: error)
            self?.indicator.stopAnimating()
            self?.tableView.reloadData()
            UIView.animate(withDuration: 0.2) {
                self?.tableView.alpha = 1
            }
        }
    }
    
    @IBAction func onDetails(btn : UIButton) {
        guard let id = service.places.items[safe: btn.tag]?.id else {return}
        showLoading()
        service.fetchPlace(id: id) { [weak self](error, place) in
            self?.hideLoading()
            ShowAlert(error: error)
            if let place = place {
                let ctrl = StoryboardScene.Places.instantiatePlace()
                ctrl.place = place
                self?.navigationController?.pushViewController(ctrl, animated: true)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let ctrl = segue.destination as? PlacesMapViewController else {return}
        ctrl.service = service
    }

}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PlacesListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return service.hasPlacesAvailable ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:     return service.places.items.count == 0 ? 1 : service.places.items.count
        case 1:     return 1
        default:    return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath:  IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if service.places.items.count == 0 {
                return tableView.dequeueReusableCell(withIdentifier: "empty", for: indexPath)
            } else {
                let cell: PlaceTableViewCell = tableView.dequeueReusableCell(identifier: "cell", indexPath: indexPath)
                cell.tag = indexPath.row
                cell.adjustTo(place: service.places.items[indexPath.row])
                cell.isExpanded = indexPath == selectedIndexPath
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "load")!
            (cell.view(identifier: "indicator") as! UIActivityIndicatorView).startAnimating()
            return cell
        default: return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.reuseIdentifier == "load" {
            reload(clear: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:     return service.places.items.count == 0 ? 200 : PlaceTableViewCell.cellHeight(isExpanded: indexPath == selectedIndexPath)
        case 1:     return 30
        default:    return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        var reload = [IndexPath]()
        reload << selectedIndexPath
        if indexPath != selectedIndexPath {
            reload << indexPath
        }
        selectedIndexPath = indexPath == selectedIndexPath ? nil : indexPath
        tableView.reloadRows(at: reload, with: .middle)
        Timer.scheduled(0.25) { [weak self]_ in
            if let ip = self?.selectedIndexPath {
                tableView.scrollToRow(at: ip, at: .middle, animated: true)
            }
        }
    }
    
}
