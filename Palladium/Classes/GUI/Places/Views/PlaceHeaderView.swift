//
//  PlaceHeaderView.swift
//  Palladium
//
//  Created by Igor Danich on 8/31/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PlaceHeaderView: UIView {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backViewPos: NSLayoutConstraint!
    @IBOutlet weak var backMediaView: MediaView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var timeHeight: NSLayoutConstraint!
    
    var offset: CGFloat = 0.0 {
        didSet {
            backViewPos.constant = -offset
            backView.layoutIfNeeded()
        }
    }
    
    func adjustTo(place: Place) {
        backMediaView.url = place.background?.large
        nameLbl.text = place.name?.uppercased()
        typeLbl.text = place.typeName
        timeLbl.text = place.addresses?.first?.workTime
        timeLbl.sizeToFit()
        timeHeight.constant = timeLbl.height + 16
        layoutIfNeeded()
    }

}
