//
//  PlaceSectionsHeaderView.swift
//  Palladium
//
//  Created by Igor Danich on 8/31/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PlaceSectionsHeaderView: UITableViewHeaderFooterView {
    
    fileprivate var buttons = [UIButton]()
    fileprivate var lineView: UIView!
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = "F7F7F7".toUIColor()
        lineView = UIView(frame: CGRect.zero)
        lineView.backgroundColor = "CFAD73".toUIColor()
        contentView.addSubview(lineView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var onChange: ((Int) -> Void)?
    
    var selectedIndex: Int = 0
    
    var sections = [PlaceSection]() {
        didSet {
            for btn in buttons {btn.removeFromSuperview()}
            for (idx,item) in sections.enumerated() {
                let btn = UIButton(type: .custom)
                btn.titleLabel?.font = UIFont(name: "AvenirNext-Medium", size: 14)
                btn.setTitleColor("9C9C9C".toUIColor(), for: .normal)
                btn.setTitleColor(UIColor.black, for: .selected)
                btn.setTitle(item.text, for: .normal)
                btn.addTarget(self, action: #selector(PlaceSectionsHeaderView.onClick(btn:)), for: .touchUpInside)
                btn.tag = idx
                contentView.addSubview(btn)
                buttons << btn
            }
            for btn in buttons {
                btn.isSelected = btn.tag == selectedIndex
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let lineWidth: CGFloat = width/CGFloat(sections.count)
        lineView.frame = CGRect(x: CGFloat(selectedIndex)*lineWidth, y: height - 2, width: lineWidth, height: 2)
        for btn in buttons {
            btn.frame = CGRect(x: CGFloat(btn.tag)*lineWidth, y: 0, width: lineWidth, height: height)
        }
    }
    
    @objc func onClick(btn: UIButton) {
        selectedIndex = btn.tag
        for btn in buttons {
            btn.isSelected = btn.tag == selectedIndex
        }
        UIView.animate(withDuration: 0.2) {
            self.lineView.frame = CGRect(x: CGFloat(self.selectedIndex)*btn.width, y: self.height - 2, width: btn.width, height: 2)
        }
        onChange?(btn.tag)
    }
    
}
