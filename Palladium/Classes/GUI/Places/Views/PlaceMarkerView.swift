//
//  PlaceMarkerView.swift
//  Palladium
//
//  Created by Igor Danich on 8/23/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PlaceMarkerView: UIView {
    
    fileprivate let margin: CGFloat = 2
    fileprivate let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 35, height: 56))
    fileprivate var mediaView: MediaView!

    init(place: Place) {
        super.init(frame: imageView.frame)
        
        mediaView = MediaView(frame: CGRect(x: margin, y: margin, width: width - margin*2, height: width - margin*2))
        mediaView.contentMode = .scaleAspectFill
        mediaView.backgroundColor = UIColor.white
        mediaView.clipsToBounds = true
        mediaView.cornerRadius = mediaView.height/2
        mediaView.indicatorColor = UIColor.darkGray
        mediaView.isUserInteractionEnabled = false
        mediaView.url = place.logo
        addSubview(mediaView)
        
        imageView.image = UIImage(asset: .Map_Pin)
        addSubview(imageView)
        backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
