//
//  PlaceTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 8/23/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundImgView: MediaView!
    @IBOutlet weak var imgView: MediaView!
    @IBOutlet weak var categoryImgView: MediaView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var detailBtn: UIButton!
    
    @IBOutlet weak var topHeight: NSLayoutConstraint!
    @IBOutlet weak var leftCenter: NSLayoutConstraint!
    @IBOutlet weak var rightCenter: NSLayoutConstraint!
    
    static func cellHeight(isExpanded: Bool) -> CGFloat {
        return isExpanded ? 461 : 72
    }
    
    var isExpanded: Bool = false {
        didSet {
            topHeight.constant = isExpanded ? 250 : 72
            leftCenter.constant = isExpanded ? topHeight.constant/2 - 12 : 0
            rightCenter.constant = isExpanded ? topHeight.constant/2 - 12 : 0
            layoutIfNeeded()
        }
    }
        
    override func awakeFromNib() {
        super.awakeFromNib()
        detailBtn.backgroundColor = UIColor.clear
        detailBtn.setBackgroundImage(UIImage.orangeGradient(size: detailBtn.size), for: .normal)
    }
    
    func adjustTo(place: Place) {
        detailBtn.tag = tag
        titleLbl.text = place.name?.uppercased()
        categoryLbl.text = place.categories?.first?.name
        backgroundImgView.url = place.background?.large
        categoryImgView.url = place.categoryIcon
        imgView.url = place.logo
        distanceLbl.text = place.distanceString
        
    }

}
