//
//  PlacesSliderView.swift
//  Palladium
//
//  Created by Igor Danich on 10/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PlacesSliderView: UIView {
    
    fileprivate var items = [MediaView]()
    fileprivate let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    fileprivate let scrollView = UIScrollView()
    fileprivate let pageControl = PageControl()
    fileprivate var timer: Timer?
    
    var autoScrollEnabled = true
    
    var onSelect: ((_:Int) -> Void)?
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        scrollView.delegate = self
        scrollView.frame = CGRect(x: 0, y: 0, width: width, height: height)
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        addSubview(scrollView)
        
        indicator.center = CGPoint(x: width/2, y: height/2)
        indicator.hidesWhenStopped = true
        addSubview(indicator)
        
        pageControl.frame = CGRect(x: 0, y: height - 40, width: width, height: 40)
        pageControl.color = UIColor.white
        pageControl.autoresizingMask = [.flexibleWidth]
        pageControl.translatesAutoresizingMaskIntoConstraints = true
        pageControl.backgroundColor = UIColor.clear
        addSubview(pageControl)
    }

    func adjustTo(places: [Place]) {
        indicator.stopAnimating()
        for view in items {
            view.removeFromSuperview()
        }
        scrollView.contentSize = CGSize(width: scrollView.width*CGFloat(places.count), height: 0)
        pageControl.numberOfPages = places.count
        for (idx,place) in places.enumerated() {
            let view = MediaView(frame: CGRect(x: scrollView.width*CGFloat(idx), y: 0, width: scrollView.width, height: scrollView.height))
            view.contentMode = .scaleAspectFill
            view.tag = idx
            view.url = place.background?.large
            view.addTarget(self, action: #selector(PlacesSliderView.onClick(view:)), for: .touchUpInside)
            scrollView.addSubview(view)
            items << view
        }
        timer?.invalidate()
        timer = nil
        if places.count > 0 {
            timer = Timer.scheduled(3, repeats: true) { [weak self]_ in
                guard self?.autoScrollEnabled == true && self?.scrollView.isDragging == false else {return}
                guard let numberOfPages = self?.pageControl.numberOfPages else {return}
                guard var page = self?.pageControl.currentPage else {return}
                guard let width = self?.scrollView.width else {return}
                if page + 1 < numberOfPages {
                    page += 1
                } else {
                    page = 0
                }
                self?.scrollView.setContentOffset(CGPoint(x: width*CGFloat(page), y: 0), animated: true)
            }
        }
    }
    
    @objc func onClick(view: MediaView) {
        onSelect?(view.tag)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView.contentSize = CGSize(width: scrollView.width*CGFloat(items.count), height: 0)
        for (idx,view) in items.enumerated() {
            view.frame = CGRect(x: scrollView.width*CGFloat(idx), y: 0, width: scrollView.width, height: scrollView.height)
        }
    }
    
}

// MARK - UIScrollViewDelegate
extension PlacesSliderView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(CGFloat(pageControl.numberOfPages)*(scrollView.contentOffset.x + scrollView.width/2)/scrollView.contentSize.width)
    }
    
}
