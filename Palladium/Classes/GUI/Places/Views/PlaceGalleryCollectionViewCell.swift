//
//  PlaceGalleryCollectionViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 8/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PlaceGalleryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mediaView: MediaView!
    
}
