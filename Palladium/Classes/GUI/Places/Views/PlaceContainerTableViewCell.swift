//
//  PlaceContainerTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 8/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PlaceContainerTableViewCell: UITableViewCell {

    var view: UIView? {
        didSet {
            oldValue?.removeFromSuperview()
            view?.translatesAutoresizingMaskIntoConstraints = true
            view?.frame = CGRect(x: 0, y: 0, width: width, height: height)
            view?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            if let view = view {
                addSubview(view)
            }
        }
    }

}
