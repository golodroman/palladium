//
//  PlaceLocationTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 8/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PlaceLocationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var addressLbl: UILabel!
    
    func adjustTo(place: Place) {
        addressLbl.text = place.address
    }
    
}
