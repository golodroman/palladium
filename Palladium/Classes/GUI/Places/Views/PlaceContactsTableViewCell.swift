//
//  PlaceContactsTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 8/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PlaceContactsTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var contactBtn: UIButton!
    
    fileprivate var items = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contactBtn.setBackgroundImage(UIImage.orangeGradient(size: contactBtn.size), for: .normal)
    }
    
    func adjustTo(place: Place) {
        items.removeAll()
        for address in place.addresses ?? [] {
            for phone in address.phones ?? [] {
                items << phone
            }
        }
        collectionView.reloadData()
    }
    
    // MARK: - UICollectionViewDelegate, UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PlaceContactCollectionViewCell = collectionView.dequeueReusableCell(identifier: "cell", indexPath: indexPath)
        cell.textLabel.text = items[indexPath.row]
        cell.line.isHidden = (indexPath.row == items.count - 1 && items.count%2 == 1) || indexPath.row%2 == 1
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (indexPath.row == items.count - 1 && items.count%2 == 1) ? width : width/2, height: 52)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        UIApplication.shared.openURL(items[indexPath.row].phoneCallURL)
    }
    
}
