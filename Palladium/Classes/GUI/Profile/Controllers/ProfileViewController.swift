//
//  ProfileViewController.swift
//  Palladium
//
//  Created by Igor Danich on 9/13/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var avatarView: MediaView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var segCtrl: SegmentedControl!
    
    fileprivate let service = PaymentCardsService()

    override func viewDidLoad() {
        super.viewDidLoad()
        segCtrl.font = UIFont(name: "AvenirNext-Regular", size: 13)
        navigationController?.tabBarItem.title = L10n.GeneralTextProfile.string
        navigationController?.tabBarItem.image = UIImage(asset: .Menu_Profile)
        navigationController?.tabBarItem.selectedImage = UIImage(asset: .Menu_Profile_Selected)
        navigationController?.navigationBar.tintColor = UIColor.white
        reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        localize()
        reload()
        UserService.shared.update { [weak self]_ in
            self?.reload()
        }
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    fileprivate func reload() {
        avatarView.url = UserService.shared.user?.photo
        var name = ""
        if let value = UserService.shared.user?.name {
            name = value + " "
        }
        if let value = UserService.shared.user?.lastName {
            name += value
        }
        nameLbl.text = name
        pointsLbl.text = Int(UserService.shared.user?.points ?? 0).priceString()
        service.fetch {[weak self] _ in
            self?.tableView.reloadData()
        }
    }
    
    @IBAction func onAvatar(_ : AnyObject?) {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: L10n.ProfileAvatarPhoto.string) { [weak self]_ in
            let ctrl = UIImagePickerController()
            ctrl.sourceType = .camera
            ctrl.delegate = self
            self?.present(ctrl, animated: true, completion: nil)
        })
        sheet.addAction(UIAlertAction(title: L10n.ProfileAvatarGallery.string) { [weak self]_ in
            let ctrl = UIImagePickerController()
            ctrl.sourceType = .photoLibrary
            ctrl.delegate = self
            self?.present(ctrl, animated: true, completion: nil)
        })
        sheet.addAction(UIAlertAction(title: L10n.GeneralTextCancel.string, style: .cancel, handler: nil))
        present(sheet, animated: true, completion: nil)
    }
    
    @IBAction func onSegmentChanged(_ : AnyObject?) {
        tableView.reloadData()
    }
    
    @IBAction func onCardRemove(btn : UIButton) {
        let alert = UIAlertController(title: "pay.card.remove.confirm".localized, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: L10n.GeneralTextCancel.string, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: L10n.GeneralTextYes.string, handler: { [weak self]_ in
            guard let service = self?.service else {return}
            self?.showLoading()
            service.removeCard(card: service.items[btn.tag]) { [weak self](error) in
                self?.hideLoading()
                ShowAlert(error: error)
                self?.tableView.reloadData()
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let ctrl = segue.destination as? NewCardViewController {
            ctrl.service = service
        }
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch segCtrl.selectedIndex {
        case 0:     return 1
        case 1:     return 2
        default:    return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segCtrl.selectedIndex {
        case 0: return service.items.count + 1
        case 1:
            switch section {
            case 0:     return 3
            case 1:     return 1
            default:    return 0
            }
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch segCtrl.selectedIndex {
        case 0:
            if indexPath.row < service.items.count {
                let cell: ProfilePaymentCardTableViewCell = tableView.dequeueReusableCell(identifier: "card", indexPath: indexPath)
                cell.adjustTo(card: service.items[indexPath.row], index: indexPath.row)
                return cell
            } else {
                return tableView.dequeueReusableCell(withIdentifier: "card.new", for: indexPath)
            }
        case 1:
            switch indexPath.section {
            case 0:
                switch indexPath.row {
                case 0:     return tableView.dequeueReusableCell(withIdentifier: "account", for: indexPath)
                case 1:     return tableView.dequeueReusableCell(withIdentifier: "language", for: indexPath)
                case 2:     return tableView.dequeueReusableCell(withIdentifier: "about", for: indexPath)
                default:    return UITableViewCell()
                }
            case 1:     return tableView.dequeueReusableCell(withIdentifier: "logout", for: indexPath)
            default:    return UITableViewCell()
            }
        default: return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:     return segCtrl.selectedIndex == 0 ? 20 : 0
        case 1:     return segCtrl.selectedIndex == 1 ? 20 : 0
        default:    return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let reuseIdentifier = tableView.cellForRow(at: indexPath)?.reuseIdentifier else {return}
        switch reuseIdentifier {
        case "logout":
            showLoading()
            UserService.shared.logout { [weak self] in
                self?.hideLoading()
            }
        default: break
        }
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            showLoading()
            ProfileUpdate().update(photo: image, { [weak self](error) in
                self?.hideLoading()
                ShowAlert(error: error)
                self?.avatarView.url = UserService.shared.user?.photo
            })
        }
    }

}
