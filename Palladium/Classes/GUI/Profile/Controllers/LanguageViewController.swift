//
//  LanguageTableViewController.swift
//  Palladium
//
//  Created by Igor Danich on 9/15/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class LanguageViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        makeBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - UITableViewCellDelegate, UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Localization.availableLocalizations().count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let value = Localization.availableLocalizations()[indexPath.row]
        cell.textLabel?.text = value.name
        cell.accessoryType = value.identifier == Localization.current.identifier ? .checkmark : .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
        Localization.setCurrent(localization: Localization.availableLocalizations()[indexPath.row])
    }

}
