//
//  ProfileViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/23/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class ProfileUpdateViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var pointsLbl: UILabel!
    @IBOutlet weak var nameTxtFld: UITextField!
    @IBOutlet weak var surnameTxtFld: UITextField!
    @IBOutlet weak var birthTxtFld: UITextField!
    @IBOutlet weak var phoneCountryImgView: UIImageView!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var mailTxtFld: UITextField!
    
    fileprivate let profile = ProfileUpdate()

    override func viewDidLoad() {
        super.viewDidLoad()
        makeBackButton()
        view.localize()
        reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    fileprivate func reload() {
        pointsLbl.text = profile.points.priceString()
        nameTxtFld.text = profile.name
        surnameTxtFld.text = profile.surname
        birthTxtFld.text = profile.birth
        phoneCountryImgView.image = profile.country
        phoneNumberLbl.text = profile.phone
        mailTxtFld.text = profile.mail
    }
    
    @IBAction func onSave(_ : AnyObject?) {
        view.endEditing(true)
        profile.name = nameTxtFld.text
        profile.surname = surnameTxtFld.text
        profile.mail = mailTxtFld.text
        showLoading()
        profile.save { [weak self](error) in
            self?.hideLoading()
            ShowAlert(error: error)
            self?.reload()
        }
    }
        
    fileprivate func showDatePicker() {
        view.endEditing(true)
        let picker = DatePickerView()
        var components = Calendar.current.dateComponents([.year, .month, .day], from: Date())
        if let year = components.year {
            components.year = year - 15
        }
        picker.maximumDate = Calendar.current.date(from: components)
        picker.date = profile.birthDate ?? Calendar.current.date(from: components) ?? Date()
        picker.onSelect = { [weak self]date in
            self?.profile.birthDate = date
            self?.birthTxtFld.text = self?.profile.birth
        }
        picker.show()
    }
    
    // MARK: - TableView
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch tableView.cellForRow(at: indexPath)?.reuseIdentifier ?? "" {
        case "birth":   showDatePicker()
        default:        break
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }

}
