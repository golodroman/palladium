//
//  NewCardViewController.swift
//  Palladium
//
//  Created by Igor Danich on 9/15/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class NewCardViewController: UIViewController {
    
    @IBOutlet weak var cardImgView: UIImageView!
    @IBOutlet weak var holderTxtFld: UITextField!
    @IBOutlet weak var cardTxtFld: UITextField!
    @IBOutlet weak var expireTxtFld: UITextField!
    @IBOutlet weak var cvvTxtFld: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    fileprivate let card = PaymentCardInfo()
    var service: PaymentCardsService!
    
    fileprivate var activeField: UITextField? {
        didSet {
            view.subviews.forEach { (view) in
                if view.view(identifier: "textField") == self.activeField {
                    view.view(identifier: "line")?.backgroundColor = "#DEC68D".toUIColor()
                } else {
                    view.view(identifier: "line")?.backgroundColor = "#E3E3E3".toUIColor()
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        submitBtn.setBackgroundImage(UIImage.orangeGradient(size: submitBtn.size), for: .normal)
        makeBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func onCamera(_ : AnyObject?) {
        let ctrl = CardIOPaymentViewController(paymentDelegate: self)!
        ctrl.suppressScanConfirmation = true
        ctrl.suppressScannedCardImage = true
        ctrl.disableManualEntryButtons = true
        present(ctrl, animated: true, completion: nil)
    }
    
    fileprivate func reload() {
        cardImgView.image = UIImage(cardType: card.type)
//        holderTxtFld.text = card.holder
        cardTxtFld.text = card.number
        expireTxtFld.text = card.expirationDateString
        cvvTxtFld.text = card.cvc
    }
    
    @IBAction func onSubmit(_ : AnyObject?) {
        view.endEditing(true)
        card.holder = holderTxtFld.text
        card.cvc = cvvTxtFld.text
        guard let value = card.paymentCard else {
            ShowAlert(text: L10n.FillInAllTheFields.string)
            return
        }
        showLoading()
        service.addCard(card: value) { [weak self](error) in
            self?.hideLoading()
            if error == nil || error?.code == 200 {
                self?.navigationController?.popViewController(animated: true)
            }
            ShowAlert(error: error, inViewController: self)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

// MARK: - UITextFieldDelegate
extension NewCardViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        activeField = nil
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == expireTxtFld {
            view.endEditing(true)
            activeField = textField
            let picker = DatePickerView()
            picker.date = card.expirationDate ?? Date()
            picker.mode = .date
            picker.show()
            picker.onSelect = { [weak self]date in
                self?.card.expirationDate = date
                self?.expireTxtFld.text = self?.card.expirationDateString
            }
            picker.onFinish = { [weak self] in
                self?.activeField = nil
            }
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeField = nil
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == cardTxtFld {
            var range = range
            if string == "" && (textField.text! as NSString).substring(with: range) == " " {
                range.location -= 1
            }
            card.number = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            textField.text = card.number
            cardImgView.image = UIImage(cardType: card.type)
            return false
        }
        return true
    }
}

// MARK: - CardIOPaymentViewControllerDelegate
extension NewCardViewController: CardIOPaymentViewControllerDelegate {

    func userDidProvide(_ cardInfo: CardIOCreditCardInfo!, in paymentViewController: CardIOPaymentViewController!) {
        paymentViewController.dismiss(animated: true, completion: nil)
        card.adjustTo(card: cardInfo)
        reload()
    }
    
    func userDidCancel(_ paymentViewController: CardIOPaymentViewController!) {
        paymentViewController.dismiss(animated: true, completion: nil)
    }

}
