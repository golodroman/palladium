//
//  PaymentCardInfo.swift
//  Palladium
//
//  Created by Igor Danich on 9/18/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation

class PaymentCardInfo {
    
    fileprivate var cardNumber  = ""
    
    var type: PaymentCardType {
        if number.hasPrefix("4") {
            return .visa
        } else if number.hasPrefix("5") {
            return .mastercard
        } else {
            return .unknown
        }
    }
    
    var number              : String {
        get {return cardNumber}
        set {cardNumber = newValue.formattedPaymentCardNumber}
    }
    
    var expirationDate  : Date?
    
    var expirationDateString: String? {
        return expirationDate?.string(format: "MM/yy")
    }
    
    var cvc             : String?
    var holder          : String?
    
    var paymentCard: PaymentCard? {
        guard   let month = expirationDate?.month,
                let year = expirationDate?.year,
                let cvc = cvc,
                let holder = holder else {return nil}
        return PaymentCard(number: number, expirationMonth: month, expirationYear: year, cvc: cvc, holder: holder)
    }
    
    init() {
    }
    
    func adjustTo(card: CardIOCreditCardInfo) {
        number = card.cardNumber
        holder = card.cardholderName
        expirationDate = Date(day: 0, month: Int(card.expiryMonth), year: Int(card.expiryYear))
    }
    
}
