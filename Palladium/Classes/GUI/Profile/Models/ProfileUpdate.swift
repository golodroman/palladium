//
//  ProfileUpdate.swift
//  Palladium
//
//  Created by Igor Danich on 8/24/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI
import PhoneNumberKit

class ProfileUpdate: NSObject {
    
    var points      = 0
    var name        : String?
    var surname     : String?
    var birth       : String? {
        get {return birthDate?.string(format: "yyyy-MM-dd")}
        set {birthDate = newValue?.date(format: "yyyy-MM-dd hh:mm:ss")}
    }
    var birthDate   : Date?
    var country     : UIImage?
    var phone       : String?
    var mail        : String?
    
    override init() {
        super.init()
        points = UserService.shared.user?.points ?? 0
        name = UserService.shared.user?.name
        surname = UserService.shared.user?.lastName
        birth = UserService.shared.user?.dob
        mail = UserService.shared.user?.email
        if let value = UserService.shared.user?.phones {
            phone = "+" + value
            if let info = try? PhoneNumberKit().parse(phone ?? "") {
                country = UIImage(named: CountryService().countryBy(phone: "\(info.countryCode)")?.flag ?? "")
            }
        }
    }
    
    func update(photo: UIImage, _ handler: ServiceHandler) {
        guard let photo = photo.imageScaledToMaxSize(CGSize(width: 100, height: 100)) else {return}
        guard let data = UIImageJPEGRepresentation(photo, 1) else {return}
        UserService.shared.update(photo: data, type: "image/jpeg", handler)
    }
    
    func save(handler: ServiceHandler) {
        UserService.shared.update(profile: User(name: name, lastName: surname, email: mail, dob: birthDate?.string(format: "yyyy-MM-dd hh:mm:ss")), handler)
    }

}
