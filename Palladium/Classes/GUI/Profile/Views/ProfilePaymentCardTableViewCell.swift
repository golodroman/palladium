//
//  PaymentCardTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 9/15/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class ProfilePaymentCardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var typeImgView: UIImageView!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var removeBtn: UIButton!

    func adjustTo(card: PaymentCard, index: Int) {
        typeImgView.image = UIImage(cardType: card.type)
        numberLbl.text = card.number
        removeBtn.tag = index
    }

}
