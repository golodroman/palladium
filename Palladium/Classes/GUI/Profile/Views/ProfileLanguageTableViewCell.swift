//
//  LanguageTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 9/15/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class ProfileLanguageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        titleLbl.text = Localization.current.localizedName
    }
    
}
