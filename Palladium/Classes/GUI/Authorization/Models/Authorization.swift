//
//  Authorization.swift
//  Palladium
//
//  Created by Igor Danich on 8/16/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class Authorization: NSObject {

    fileprivate let countriesService = CountryService()
    
    fileprivate var selectedCountry: Country?
    
    var country: Country {
        get {return selectedCountry ?? countriesService.defaultCountry}
        set {selectedCountry = newValue}
    }
    
    var privacyURL: URL {
        return URL(string: "https://addzer.com/\(LanguageService.shared.language.shortCode)/page/privacy-policy")!
    }
    
    var phone: String?
    
    var phoneNumber: String {
        return "+" + country.dialCode + (phone ?? "")
    }
    
    var countries: [Country] {
        return countriesService.countries
    }
    
    var licenseText: NSAttributedString {
        let font = UIFont(name: "AvenirNext-Regular", size: 12)!
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        let text = NSMutableAttributedString(string: L10n.RegistrationPrivacyLicense.string + " ", attributes: [
            NSAttributedStringKey.font             : font,
            NSAttributedStringKey.foregroundColor  : UIColor.gray,
            NSAttributedStringKey.paragraphStyle   : style
        ])
        text.append(NSAttributedString(string: L10n.TitleTerms.string, attributes: [
            NSAttributedStringKey.font             : font,
            NSAttributedStringKey.foregroundColor  : "D8CEB8".toUIColor(),
            NSAttributedStringKey.paragraphStyle   : style
        ]))
        return text
    }
    
    func preload(handler: ServiceHandler) {
        countriesService.fetchDefault { error in
            handler?(error)
        }
    }
    
    func login(_ handler: ServiceHandler) {
        if UseAddzer {
            Timer.scheduled(1) { (_) in
                handler?(nil)
            }
        } else {
            UserService.shared.login(phone: phoneNumber, code: nil, handler: handler)
        }
    }
    
    func login(code: String, _ handler: ServiceHandler) {
        if UseAddzer {
            UserService.shared.login(handler: handler)
        } else {
            UserService.shared.login(phone: phoneNumber, code: code, handler: handler)
        }
    }
    
}
