//
//  WelcomeViewController.swift
//  Addzer
//
//  Created by Eugene Donov on 2/25/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class TutorialViewController: UIViewController {
    
    @IBOutlet var imageView     : UIImageView!
    @IBOutlet var label0        : UILabel!
    @IBOutlet var label1        : UILabel!
    @IBOutlet var pageControl   : PageControl!
    @IBOutlet var buttonAuth    : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonAuth.backgroundColor = UIColor(red:0.82, green:0.68, blue:0.45, alpha:1)
        buttonAuth.setTitle("general.text.login".localized, for: .normal)
        pageControl.numberOfPages = self.source.count
        imageView.image = UIImage(named: self.source[0].0)
        label0.text = self.source[0].1.localized
        label1.text = self.source[0].2.localized
        pageControl.color = "D1AD73".toUIColor()
        makeBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
        navigationController?.navigationBar.clear()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    // MARK: - Actions
    
    @IBAction func onSwipeLeft(sender: UISwipeGestureRecognizer) {
        if pageControl.currentPage + 1 < pageControl.numberOfPages {
            pageControl.currentPage += 1
        } else {
            pageControl.currentPage = 0
        }
        adjustSource()
    }

    @IBAction func onSwipeRight(sender: UISwipeGestureRecognizer) {
        if pageControl.currentPage - 1 < 0 {
            pageControl.currentPage = self.pageControl.numberOfPages - 1
        } else {
            pageControl.currentPage -= 1
        }
        adjustSource()
    }
    
    @IBAction func onLogin(_ : AnyObject?) {
        let auth = Authorization()
        showLoading()
        auth.preload { [weak self]_ in
            self?.hideLoading()
            let ctrl = StoryboardScene.Authorization.instantiateLogin()
            ctrl.authorization = auth
            self?.navigationController?.pushViewController(ctrl, animated: true)
        }
    }
        
    // MARK: - Private
    
    fileprivate let source: [(String, String, String)] = [
        ("tutorial-0", "launcher.1.title", "launcher.1.text"),
        ("tutorial-1", "launcher.2.title", "launcher.2.text"),
        ("tutorial-2", "launcher.3.title", "launcher.3.text"),
    ]
    
    fileprivate func adjustSource() {
        self.fadeOut() {
            self.imageView.image = UIImage(named: self.source[self.pageControl.currentPage].0)
            self.label0.text = self.source[self.pageControl.currentPage].1.localized
            self.label1.text = self.source[self.pageControl.currentPage].2.localized
            self.fadeIn(completion: nil)
        }
    }
    
    fileprivate func fadeIn(completion: (() -> Void)?) {
        UIView.animate(withDuration: 0.4, animations: {
            self.imageView.alpha = 1
            self.label0.alpha = 1
            self.label1.alpha = 1
        }, completion: { (finished) in
            completion?()
        })
    }

    fileprivate func fadeOut(completion: (() -> Void)?) {
        UIView.animate(withDuration: 0.00, animations: {
            self.imageView.alpha = 0
            self.label0.alpha = 0
            self.label1.alpha = 0
        }, completion: { (finished) in
            completion?()
        })
    }
    
}
