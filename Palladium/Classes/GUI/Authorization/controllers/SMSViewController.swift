//
//  SMSViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/16/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class SMSViewController: UIViewController, UITextFieldDelegate {
    
    fileprivate let count = 4
    fileprivate let labelOffset = 100
    fileprivate let underlineOffset = 200
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var phoneLbl: UILabel!
    
    var authorization: Authorization!

    override func viewDidLoad() {
        super.viewDidLoad()
        phoneLbl.text = authorization.phoneNumber
        makeBackButton()
        updateSelection()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textField.becomeFirstResponder()
    }
    
    fileprivate func onSubmit() {
        view.endEditing(true)
        showLoading()
        authorization.login(code: textField.text ?? "") { [weak self]error in
            self?.hideLoading()
            ShowAlert(error: error)
            self?.textField.text = nil
            self?.updateSelection()
            if error != nil {
                self?.textField.becomeFirstResponder()
            }
        }
    }
    
    @IBAction func onResend(_ : AnyObject?) {
        showLoading()
        authorization.login { [weak self]error in
            self?.hideLoading()
            ShowAlert(error: error)
            self?.textField.text = nil
            self?.updateSelection()
        }
    }
    
    @IBAction func onKB(_: AnyObject?) {
        textField.becomeFirstResponder()
    }
    
    fileprivate func updateSelection() {
        for i in 0 ..< count {
            if let lbl = view.viewWithTag(labelOffset + i) as? UILabel {
                if textField.text?.length ?? 0 > i {
                    lbl.text = (textField.text as NSString?)?.substring(with: NSMakeRange(i, 1))
                } else {
                    lbl.text = nil
                }
            }
            if let underline = view.viewWithTag(underlineOffset + i) {
                underline.backgroundColor = (textField.text?.length ?? 0 > i) ? "AD9761".toUIColor() : UIColor.gray
                underline.height = (textField.text?.length ?? 0 > i) ? 2 : 1
                underline.originY = underline.superview!.height - underline.height
            }
        }
        navigationItem.rightBarButtonItem?.isEnabled = (textField.text?.length ?? 0 == count)
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else {return false}
        if text.length <= 4 {
            textField.text = text
        }
        if text.length == 4 {
            onSubmit()
        }
        updateSelection()
        return false
    }

}
