//
//  LoginViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/11/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var phoneTxtFld: UITextField!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var flagImgView: UIImageView!
    @IBOutlet weak var licenseBtn: UIButton!
    
    var authorization: Authorization!

    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        updateCountry()
        licenseBtn.titleLabel?.numberOfLines = 2
        licenseBtn.setAttributedTitle(authorization.licenseText, for: .normal)
        makeBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        phoneTxtFld.becomeFirstResponder()
    }
    
    fileprivate func updateCountry() {
        countryLbl.text = "+" + authorization.country.dialCode
        flagImgView.image = UIImage(named: authorization.country.flag)
    }
    
    @IBAction func onCountryChange(_ : AnyObject?) {
        view.endEditing(true)
        let picker = PickerView()
        picker.onSelect = { [weak self]index in
            if let country = self?.authorization.countries[index] {
                self?.authorization.country = country
            }
            self?.updateCountry()
        }
        picker.items = authorization.countries.makeArray { (_, country) in
            return country.name
        }
        picker.showIn(view: view)
    }
    
    @IBAction func onLogin(_ : AnyObject?) {
        view.endEditing(true)
        showLoading()
        authorization.login { [weak self] error in
            self?.hideLoading()
            ShowAlert(error: error)
            if error == nil || error?.code == 10 {
                let ctrl = StoryboardScene.Authorization.instantiateSms()
                ctrl.authorization = self?.authorization
                self?.navigationController?.pushViewController(ctrl, animated: true)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "license" else {return}
        guard let ctrl = (segue.destination as? UINavigationController)?.viewControllers.first as? WebViewController else {return}
        ctrl.url = authorization.privacyURL
        ctrl.title = L10n.AgreeWithLicenseTerm.string
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        onLogin(nil)
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        authorization.phone = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        return true
    }
    
}
