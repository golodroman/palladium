//
//  GalleryViewController.swift
//  Addzer
//
//  Created by Igor Danich on 5/31/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    fileprivate var collectionView: UICollectionView!
    var items = [String]()
    fileprivate var statusBarHidden = false
    fileprivate var toolbar: UIToolbar!
    fileprivate var rotating = false
    
    fileprivate var selectedIndex: Int = 0
    var currentIndex: Int {
        get {return selectedIndex}
        set {
            selectedIndex = newValue
            collectionView?.contentOffset = CGPoint(x: CGFloat(selectedIndex)*view.width, y: 0)
            updateTitle()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(GalleryViewController.onDone))
        navigationItem.leftBarButtonItem?.setTitleTextAttributes([
            NSAttributedStringKey.font             : UIFont(name: "HelveticaNeue-Thin", size: 15) as Any,
            NSAttributedStringKey.foregroundColor  : UIColor.black
            ], for: .normal)
        
        view.backgroundColor = UIColor.black
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets.zero
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height), collectionViewLayout: layout)
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.translatesAutoresizingMaskIntoConstraints = true
        collectionView.isPagingEnabled = true
        collectionView.backgroundView = nil
        collectionView.backgroundColor = UIColor.clear
        collectionView.delegate = self
        collectionView.dataSource = self
        view.addSubview(collectionView)
        collectionView.register(GalleryCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cell")
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(GalleryViewController.onTap)))
        
        toolbar = UIToolbar(frame: CGRect(x: 0, y: view.frame.size.height - 44.0, width: view.frame.size.width, height: 44))
        toolbar.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        toolbar.translatesAutoresizingMaskIntoConstraints = true
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(GalleryViewController.onShare))]
        toolbar.tintColor = UIColor.black
        view.addSubview(toolbar)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView.reloadData()
        if selectedIndex < items.count {
            collectionView.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: .top, animated: false)
        }
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.font             : UIFont(name: "HelveticaNeue-Thin", size: 17) as Any,
            NSAttributedStringKey.foregroundColor  : UIColor.black
        ]
        updateTitle()
        (UIApplication.shared.delegate as? AppDelegate)?.restrictRotation = false
    }
    
    @objc func onShare() {
        if let image = ImageCache()[items[currentIndex]] {
            present(UIActivityViewController(activityItems: [image], applicationActivities: nil), animated: true, completion: nil)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return statusBarHidden
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .none
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    @objc func onDone() {
        (UIApplication.shared.delegate as? AppDelegate)?.restrictRotation = true
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func onTap() {
        statusBarHidden = (navigationController?.navigationBar.alpha ?? 0) == 1
        let alpha: CGFloat = statusBarHidden ? 0 : 1
        UIView.animate(withDuration: 0.25) {
            self.setNeedsStatusBarAppearanceUpdate()
            self.navigationController?.navigationBar.alpha = alpha
            self.toolbar.alpha = alpha
        }
    }
    
    fileprivate func updateTitle() {
        title = "\(currentIndex + 1) of \(items.count)"
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .allButUpsideDown
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        rotating = true
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        collectionView.reloadData()
        currentIndex = selectedIndex
        rotating = false
    }
    
    // MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! GalleryCollectionViewCell
        cell.resource = items[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard !rotating else {return}
        selectedIndex = Int(CGFloat(items.count)*(collectionView.contentOffset.x + collectionView.frame.size.width/2)/collectionView.contentSize.width)
        updateTitle()
    }

}
