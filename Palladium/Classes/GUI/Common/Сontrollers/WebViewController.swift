//
//  WebViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/16/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    var url: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = url {
            webView.loadRequest(URLRequest(url: url))
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: L10n.GeneralTextClose.string) { [weak self]_ in
            self?.navigationController?.dismiss(animated: true, completion: nil)
        }
    }

}
