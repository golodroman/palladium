//
//  UIViewController+Extensions.swift
//  Palladium
//
//  Created by Igor Danich on 8/21/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

fileprivate let loading = LoadingView()

extension UIViewController {
    
    func showLoading() {
        loading.show()
    }
    
    func hideLoading() {
        loading.hide()
    }
    
}
