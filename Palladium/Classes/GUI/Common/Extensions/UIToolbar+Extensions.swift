//
//  UIToolbar+Extensions.swift
//  Addzer
//
//  Created by Maria Nashchanskaia on 26.02.17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

extension UIToolbar {
    func prepareForPickerView(target: AnyObject?,donePicker:Selector, cancelPicker:Selector) {
        barStyle = .default
        sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "menu.done".localized, style: UIBarButtonItemStyle.plain, target: target, action:donePicker)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "general.text.cancel".localized, style: UIBarButtonItemStyle.plain, target: target, action: cancelPicker)
        
        setItems([cancelButton, spaceButton, doneButton], animated: false)
        isUserInteractionEnabled = true
    }
}
