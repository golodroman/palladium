//
//  UIImage+Extensions.swift
//  NoHalftime
//
//  Created by Eugene Donov on 8/27/16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import UIKit

extension UIImage {
    
    func imageWithTintColor(_ color: UIColor?) -> UIImage? {
        if let color = color {
            var image = self.withRenderingMode(.alwaysTemplate)
            UIGraphicsBeginImageContextWithOptions(self.size, false, image.scale);
            color.set()
            image.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: image.size.height))
            image = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return image
        }else {
            return self.copy() as? UIImage
        }
    }
    
}
