//
//  LoadingView.swift
//  Palladium
//
//  Created by Igor Danich on 8/21/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    
    fileprivate var indicator: UIActivityIndicatorView!
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        backgroundColor = UIColor.black.withAlphaComponent(0.4)
        indicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        indicator.center = center
        addSubview(indicator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show() {
        var window: UIWindow!
        for item in UIApplication.shared.windows {
            if NSStringFromClass(item.classForCoder) == "UIWindow" {
                window = item
            }
        }
        guard window != nil else {return}
        window.addSubview(self)
        alpha = 0
        indicator.startAnimating()
        UIView.animate(withDuration: 0.2) { 
            self.alpha = 1
        }
    }
    
    func hide() {
        UIView.animate(withDuration: 0.2, animations: { 
            self.alpha = 0
        }) { _ in
            self.removeFromSuperview()
            self.indicator.stopAnimating()
        }
    }

}
