//
//  GalleryCollectionViewCell.swift
//  Gallery
//
//  Created by Igor Danich on 5/31/17.
//  Copyright © 2017 Igor Danich. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    fileprivate var scrollView: GalleryScrollView?
    
    var resource: String? {
        didSet {
            scrollView?.removeFromSuperview()
            scrollView = nil
            if let resource = resource {
                scrollView = GalleryScrollView(frame: CGRect(origin: CGPoint.zero, size: contentView.frame.size))
                scrollView?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                scrollView?.translatesAutoresizingMaskIntoConstraints = true
                scrollView?.adjustTo(resource: resource)
                contentView.addSubview(scrollView!)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundView = nil
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView?.reset()
    }
    
}
