//
//  PickerView.swift
//  Palladium
//
//  Created by Igor Danich on 8/16/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class DatePickerView: UIView {
    
    fileprivate var picker = UIDatePicker()
    fileprivate var alphaView = UIView()
    
    var onSelect: ((_ date: Date) -> Void)?
    var onFinish: (() -> Void)?
    var date: Date {
        get {return picker.date}
        set {picker.date = newValue}
    }
    var maximumDate: Date? {
        get {return picker.maximumDate}
        set {picker.maximumDate = newValue}
    }
    
    var minimumDate: Date? {
        get {return picker.minimumDate}
        set {picker.minimumDate = newValue}
    }
    
    var mode: UIDatePickerMode {
        get {return picker.datePickerMode}
        set {picker.datePickerMode = newValue}
    }
    
    var selectedIndex: Int = 0
    
    init() {
        super.init(frame: CGRect.zero)
        
        backgroundColor = UIColor.clear
        
        alphaView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        alphaView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PickerView.hide)))
        addSubview(alphaView)
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(DatePickerView.onChanged), for: .valueChanged)
        picker.backgroundColor = UIColor.white
        addSubview(picker)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show() {
        if let view = UIApplication.shared.windows.first {
            showIn(view: view)
        }
    }
    
    func showIn(view: UIView) {
        frame = CGRect(origin: CGPoint.zero, size: view.size)
        alphaView.frame = frame
        alphaView.alpha = 0
        view.addSubview(self)
        
        picker.frame = CGRect(x: 0, y: view.height, width: view.width, height: 200)
        
        UIView.animate(withDuration: 0.2) {
            self.alphaView.alpha = 1
            self.picker.originY = view.height - self.picker.height
        }
    }
    
    @objc func hide() {
        onSelect?(picker.date)
        onFinish?()
        UIView.animate(withDuration: 0.2) {
            self.alpha = 0
            self.picker.originY += self.picker.height
        }
    }

    @objc func onChanged() {
        onSelect?(picker.date)
    }
    
}
