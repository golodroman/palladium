//
//  PageControl.swift
//  Addzer
//
//  Created by Eugene Donov on 2/25/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

enum PageControlStyle: Int {
    case style0 = 0
    case style1
}

class PageControl: UIControl {
    
    fileprivate var pointIndent: CGFloat = 16
    var style: PageControlStyle = .style0 {
        didSet {
            switch style {
            case .style0:   pointIndent = 16
            case .style1:   pointIndent = 17
            }
            setNeedsDisplay()
        }
    }
    
    var currentPage: Int = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var numberOfPages: Int = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var color: UIColor = "#4296C7".toUIColor()
    
    override func draw(_ rect: CGRect) {
        let centerPoint = CGPoint(x: rect.midX, y: rect.midY)
        let xIndent: CGFloat = (CGFloat)(numberOfPages - 1) / 2.0 * pointIndent
        for i in 0..<self.numberOfPages {
            let isCurrent = currentPage == i
            let indent = centerPoint.x - xIndent + pointIndent * CGFloat(i)
            let currentPoint = CGPoint(x: indent, y: centerPoint.y)
            let path = bezierPathAtPoint(currentPoint, isCurrent: isCurrent, style: style)
            if isCurrent {
                color.setFill()
            } else {
                UIColor.lightGray.setFill()
            }
            path.fill()
            path.stroke()
        }
    }
    
    // MARK: - Private
    
    fileprivate func bezierPathAtPoint(_ point: CGPoint, isCurrent: Bool, style: PageControlStyle) -> UIBezierPath {
        let path = UIBezierPath()
        if .style1 == style {
            var bubbleHeight: CGFloat = 2.0
            var bubbleWidth: CGFloat = 7.0
            if isCurrent {
                bubbleWidth = 13.0
                bubbleHeight = 4.0
            }
            path.move(to: CGPoint(x: point.x - (bubbleWidth - bubbleHeight) / 2, y: point.y - bubbleHeight / 2))
            path.addLine(to: CGPoint(x: point.x + (bubbleWidth - bubbleHeight) / 2, y: point.y - bubbleHeight / 2))
            path.addArc(
                withCenter: CGPoint(x: point.x + (bubbleWidth - bubbleHeight) / 2, y: point.y),
                radius: bubbleHeight / 2,
                startAngle: CGFloat(-1 * Double.pi/2),
                endAngle: CGFloat(Double.pi/2),
                clockwise: true
            )
            path.addLine(to: CGPoint(x: point.x - (bubbleWidth - bubbleHeight) / 2, y: point.y + bubbleHeight / 2))
            path.addArc(
                withCenter: CGPoint(x: point.x - (bubbleWidth - bubbleHeight) / 2, y: point.y),
                radius: bubbleHeight / 2,
                startAngle: CGFloat(Double.pi/2),
                endAngle: CGFloat(3 * Double.pi/2),
                clockwise: true
            )
        } else {
            path.addArc(
                withCenter: point,
                radius: 3.5,
                startAngle: 0,
                endAngle: CGFloat(2 * Double.pi),
                clockwise: true
            )
        }
        path.lineWidth = 0
        path.close()
        return path
    }
    
}
