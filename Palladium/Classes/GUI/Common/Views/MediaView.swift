//
//  MediaView.swift
//
//  Created by Igor Danich on 27.06.16.
//  Copyright © 2016 Mediapark. All rights reserved.
//

import DCUI

@IBDesignable
class MediaView: UIControl {
    
    fileprivate(set) var imageView: UIImageView!
    fileprivate var indicator: UIActivityIndicatorView?
    fileprivate var task: CacheTask?
    var loading = false {
        didSet {
            updateImage()
        }
    }
    @IBInspectable var largeIndicator: Bool = false {
        didSet {
            indicator?.removeFromSuperview()
            indicator = UIActivityIndicatorView(activityIndicatorStyle: largeIndicator ? .whiteLarge : .white)
            indicator?.hidesWhenStopped = true
            indicator?.color = indicatorColor
            addSubview(indicator!)
        }
    }
    
    @IBInspectable var isIndicatorEnabled: Bool = true {
        didSet {
            if !isIndicatorEnabled {indicator?.stopAnimating()}
            updateImage()
        }
    }
    
    @IBInspectable var indicatorColor: UIColor? = UIColor.black {
        didSet {
            indicator?.color = indicatorColor
        }
    }
    
    @IBInspectable var defaultImage: UIImage? {
        didSet {
            updateImage()
        }
    }
    
    @IBInspectable var image: UIImage? {
        didSet {
            updateImage()
        }
    }
    
    var cache = ImageCache()
    
    var onImageUpdated: ((UIImage?) -> Void)?
    
    func cacheImage(url: String) -> UIImage! {
        return cache[url] as? UIImage
    }
    
    deinit {
        task?.cancel()
        task = nil
    }
    
    var url: String? {
        didSet {
//            if oldValue == url && image != nil {return}
            image = nil
            task?.cancel()
            task = nil
            if cache.contains(key: url) {
                loading = false
                image = cache[url] as? UIImage
            } else if let task = cache.perform(key: url) {
                loading = true
                self.task = task
                task.onComplete = { [weak self](object,error) in
                    self?.loading = false
                    self?.task = nil
                    self?.image = object as? UIImage
                    self?.onImageUpdated?(object as? UIImage)
                }
            } else {
                loading = false
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepare()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepare()
    }
    
    fileprivate func prepare() {
        clipsToBounds = true
        imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: size))
        imageView.contentMode = contentMode
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.translatesAutoresizingMaskIntoConstraints = true
        addSubview(imageView)
        indicator = UIActivityIndicatorView(activityIndicatorStyle: largeIndicator ? .whiteLarge : .white)
        indicator?.hidesWhenStopped = true
        addSubview(indicator!)
    }
    
    override var contentMode: UIViewContentMode {
        didSet {
            imageView?.contentMode = contentMode
        }
    }
    
    fileprivate func updateImage() {
        if loading {
            if isIndicatorEnabled {indicator?.startAnimating()}
            imageView.image = nil
        } else {
            indicator?.stopAnimating()
            if let image = image {
                imageView.image = image
            } else {
                imageView.image = defaultImage
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        indicator?.center = CGPoint(x: width/2, y: height/2)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        sendActions(for: .touchUpInside)
    }
    
    override func prepareForInterfaceBuilder() {
        layer.contents = (UIImage.draw(size: size) { (size, context) in
            let imgView = UIImageView()
            imgView.contentMode = self.contentMode
            imgView.image = self.defaultImage
            imgView.layer.render(in: context)
        }).cgImage
    }
    
}
