//
//  PickerView.swift
//  Palladium
//
//  Created by Igor Danich on 8/16/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class PickerView: UIView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    fileprivate var picker = UIPickerView()
    fileprivate var alphaView = UIView()
    
    var onSelect: ((_ index: Int) -> Void)?
    
    var items = [String]() {
        didSet {
            picker.reloadAllComponents()
        }
    }
    
    var selectedIndex: Int = 0
    
    init() {
        super.init(frame: CGRect.zero)
        
        backgroundColor = UIColor.clear
        
        alphaView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        alphaView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PickerView.hide)))
        addSubview(alphaView)
        
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = UIColor.white
        addSubview(picker)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showIn(view: UIView) {
        frame = CGRect(origin: CGPoint.zero, size: view.size)
        alphaView.frame = frame
        alphaView.alpha = 0
        view.addSubview(self)
        
        picker.frame = CGRect(x: 0, y: view.height, width: view.width, height: 200)
        
        UIView.animate(withDuration: 0.2) {
            self.alphaView.alpha = 1
            self.picker.originY = view.height - self.picker.height
        }
    }
    
    @objc func hide() {
        onSelect?(selectedIndex)
        UIView.animate(withDuration: 0.2) {
            self.alpha = 0
            self.picker.originY += self.picker.height
        }
    }
    
    // MARK: - UIPickerViewDelegate, UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return items[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedIndex = row
        onSelect?(selectedIndex)
    }
    
}
