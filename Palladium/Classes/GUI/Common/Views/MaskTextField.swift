//
//  MaskTextField.swift
//  Palladium
//
//  Created by Igor Danich on 9/13/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

fileprivate class MaskTextFieldPrivate: UITextField {
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect.zero
    }
    
}

@IBDesignable
class MaskTextField: UIControl, UITextFieldDelegate {
    
    fileprivate let textField = MaskTextFieldPrivate()
    fileprivate let backgroundView = UIView()
    fileprivate let label = UILabel()
    
    @IBInspectable var maskText    : String!
    @IBInspectable var staticText  : String?
    
    fileprivate var value: String? {
        didSet {
            label.attributedText = attributedText
        }
    }
    
    var text: String {
        get {
            guard let value = value else {return maskText ?? ""}
            var string: String = maskText
            var offset = 0
            for range in maskRanges {
                if offset + range.length < value.length {
                    string = string.replacing(range: range, string: value.substring(range: NSMakeRange(offset, range.length)))
                } else {
                    string = string.replacing(range: NSMakeRange(range.location, value.length - offset), string: value.substring(range: NSMakeRange(offset, value.length - offset)))
                    break
                }
                offset += range.length
            }
            return string
        }
        set {
            value = newValue.replacingOccurrences(of: staticText ?? "", with: "")
            textField.text = value
        }
    }
    
    var attributedText: NSAttributedString {
        let string = NSMutableAttributedString(string: maskText, attributes: [
            .font               : font,
            .foregroundColor    : placeholderColor ?? UIColor.lightGray
        ])
        if let value = value {
            let attrs: [NSAttributedStringKey:Any] = [
                .font               : font,
                .foregroundColor    : textColor ?? UIColor.black
            ]
            var offset = 0
            for range in maskRanges {
                if offset + range.length < value.length {
                    string.replaceCharacters(
                        in: range,
                        with: NSAttributedString(string: value.substring(range: NSMakeRange(offset, range.length)), attributes: attrs)
                    )
                } else {
                    string.replaceCharacters(
                        in: NSMakeRange(range.location, value.length - offset),
                        with: NSAttributedString(string: value.substring(range: NSMakeRange(offset, value.length - offset)), attributes: attrs)
                    )
                    break
                }
                offset += range.length
            }
        }
        return string
    }
    
    override var backgroundColor: UIColor? {
        didSet {
            backgroundView.backgroundColor = backgroundColor
        }
    }
    
    var textAlignment: NSTextAlignment {
        set {label.textAlignment = newValue}
        get {return label.textAlignment}
    }
    
    var font: UIFont {
        set {label.font = font}
        get {return label.font}
    }
    
    @IBInspectable var textColor: UIColor?
    @IBInspectable var placeholderColor: UIColor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.frame = CGRect(origin: CGPoint.zero, size: size)
        textField.borderStyle = .none
        textField.keyboardType = .numberPad
        textField.delegate = self
        addSubview(textField)
        backgroundView.frame = textField.frame
        addSubview(backgroundView)
        label.frame = CGRect(origin: CGPoint.zero, size: size)
        addSubview(label)
        value = nil
    }
    
    override var isFirstResponder: Bool {
        return textField.isFirstResponder
    }
    
    @discardableResult override func becomeFirstResponder() -> Bool {
        sendActions(for: .editingDidBegin)
        return textField.becomeFirstResponder()
    }
    
    @discardableResult override func resignFirstResponder() -> Bool {
        let result = textField.resignFirstResponder()
        if result {sendActions(for: .editingDidEnd)}
        return result
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        textField.becomeFirstResponder()
        sendActions(for: .editingDidBegin)
    }
    
    fileprivate var maskRanges: [NSRange] {
        guard let staticText = staticText else {return [NSMakeRange(0, maskText.length)]}
        guard staticText.length > 0 else {return [NSMakeRange(0, maskText.length)]}
        var ranges = [NSRange]()
        var offset = 0
        for var i in 0 ..< maskText.length {
            let sub = maskText.substring(range: NSMakeRange(i, staticText.length))
            guard sub == staticText else {continue}
            ranges << NSMakeRange(offset, i - offset)
            offset = i + staticText.length
            i += staticText.length
        }
        if maskText.length - offset > 0 {
            ranges << NSMakeRange(offset, maskText.length - offset)
        }
        return ranges
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let string = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? ""
        let length = maskText.replacingOccurrences(of: staticText ?? "", with: "").length
        if string.length < length {
            value = string
            sendActions(for: .editingChanged)
            return true
        } else if string.length == length {
            value = string
            sendActions(for: .editingDidEnd)
            return true
        } else {
            return false
        }
    }
    
    // MARK: - Designable
    
    override func prepareForInterfaceBuilder() {
        layer.contents = (UIImage.draw(size: size) { (size, _) in
            let label = UILabel(frame: CGRect(origin: CGPoint.zero, size: size))
            label.attributedText = self.attributedText
            label.drawText(in: label.frame)
        }).cgImage
    }

}
