//
//  GalleryScrollView.swift
//  Gallery
//
//  Created by Igor Danich on 5/31/17.
//  Copyright © 2017 Igor Danich. All rights reserved.
//

import UIKit

class GalleryScrollView: UIScrollView, UIScrollViewDelegate {
    
    fileprivate var imageView: MediaView!
    fileprivate var isZoomEnabled = false
    fileprivate var resource: String?

    override init(frame: CGRect) {
        super.init(frame: frame)
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        decelerationRate = UIScrollViewDecelerationRateFast
        delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func adjustTo(resource: String?) {
        self.resource = resource
        reset()
    }
    
    func reset() {
        imageView?.removeFromSuperview()
        imageView = MediaView()
        imageView.backgroundColor = UIColor.clear
        imageView.largeIndicator = true
        imageView.indicatorColor = UIColor.white
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        isZoomEnabled = false
        imageView.onImageUpdated = { [weak self] image in
            if image != nil {
                self?.isZoomEnabled = true
                self?.resetImageFrame()
            }
        }
        imageView.url = resource
        if imageView.image != nil {
            isZoomEnabled = true
        }
        resetImageFrame()
    }
    
    fileprivate func resetImageFrame() {
        var imageSize = frame.size
        if let size = imageView?.image?.size {
            imageSize = size
        }
        imageView.frame = CGRect(origin: CGPoint.zero, size: imageSize)
        imageView.transform = CGAffineTransform.identity
        contentSize = imageSize
        
        let xScale = bounds.width / imageSize.width
        let yScale = bounds.height / imageSize.height
        let imagePortrait = imageSize.height > imageSize.width
        let phonePortrait = bounds.height >= bounds.width
        var minScale = (imagePortrait == phonePortrait) ? xScale : min(xScale, yScale)
        let maxScale = 5.0*minScale
        if minScale > maxScale {
            minScale = maxScale
        }
        maximumZoomScale = maxScale
        minimumZoomScale = minScale * 0.999
        
        zoomScale = minimumZoomScale
        contentOffset = CGPoint.zero
    }
    
    fileprivate func adjustFrameToCenter() {
        var frameToCenter = imageView.frame
        
        if frameToCenter.size.width < bounds.width {
            frameToCenter.origin.x = (bounds.width - frameToCenter.size.width) / 2
        } else {
            frameToCenter.origin.x = 0
        }
        if frameToCenter.size.height < bounds.height {
            frameToCenter.origin.y = (bounds.height - frameToCenter.size.height) / 2
        } else {
            frameToCenter.origin.y = 0
        }
        imageView.frame = frameToCenter
    }
    
    // MARK: - UIScrollViewDelegate
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return isZoomEnabled ? imageView : nil
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        adjustFrameToCenter()
    }

}
