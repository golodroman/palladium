//
//  SegmentedControl.swift
//  Palladium
//
//  Created by Igor Danich on 9/15/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class SegmentedControl: UIControl {
    
    @IBInspectable var itemsString: String? {
        didSet {
            items = (itemsString ?? "").components(separatedBy: ";")
        }
    }
    
    @IBInspectable var textColor: UIColor? {
        didSet {
            for btn in buttons {
                btn.setTitleColor(textColor, for: .normal)
            }
        }
    }
    @IBInspectable var selectedTextColor: UIColor? {
        didSet {
            for btn in buttons {
                btn.setTitleColor(selectedTextColor, for: .selected)
            }
        }
    }
    
    var font: UIFont? {
        didSet {
            for btn in buttons {
                btn.titleLabel?.font = font
            }
        }
    }
    
    var items = [String]() {
        didSet {
            for btn in buttons {
                btn.removeFromSuperview()
            }
            buttons = []
            let width = self.width/CGFloat(items.count)
            for (idx,item) in items.enumerated() {
                let btn = UIButton(frame: CGRect(x: CGFloat(idx)*width, y: 0, width: width, height: height))
                btn.setTitleColor(textColor, for: .normal)
                btn.setTitleColor(selectedTextColor, for: .selected)
                btn.setTitle(item.localized, for: .normal)
                btn.addTarget(self, action: #selector(SegmentedControl.onAction(btn:)), for: .touchUpInside)
                btn.tag = idx
                buttons << btn
                addSubview(btn)
            }
            updateSelection(animated: false)
        }
    }
    
    fileprivate var _selectedIndex = 0
    var selectedIndex: Int {
        get {return _selectedIndex}
        set {
            _selectedIndex = newValue
            updateSelection(animated: false)
        }
    }
    
    fileprivate var buttons     = [UIButton]()
    fileprivate let lineView    = UIView()
    
    deinit {
        NotificationRemove(observer: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lineView.backgroundColor = UIColor(red:0.82, green:0.68, blue:0.45, alpha:1)
        addSubview(lineView)
        NotificationAdd(observer: self, name: kNotificationLocalizationChanged) { [weak self]_ in
            for btn in self?.buttons ?? [] {
                btn.setTitle(self?.items[safe: btn.tag]?.localized, for: .normal)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let width = self.width/CGFloat(items.count)
        for (idx,btn) in buttons.enumerated() {
            btn.frame = CGRect(x: CGFloat(idx)*width, y: 0, width: width, height: height)
        }
        if let selected = buttons[safe: _selectedIndex] {
            lineView.frame = CGRect(x: selected.originX, y: height - 3, width: selected.width, height: 3)
        }
    }
    
    fileprivate func updateSelection(animated: Bool) {
        guard _selectedIndex < buttons.count else {return}
        for btn in buttons {
            btn.isSelected = btn.tag == _selectedIndex
        }
        let selected = buttons[_selectedIndex]
        UIView.animate(withDuration: animated ? 0.25 : 0) {
            self.lineView.frame = CGRect(x: selected.originX, y: self.height - 3, width: selected.width, height: 3)
        }
    }
    
    @objc func onAction(btn: UIButton) {
        _selectedIndex = btn.tag
        updateSelection(animated: true)
        sendActions(for: .valueChanged)
    }
    
}
