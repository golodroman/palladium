// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import Foundation

enum L10n {
  /// Accept invitation
  case AcceptInvitation
  /// Exit
  case ActionExit
  /// Activate twitter account in your phone settings
  case ActivateTwitterAccountInPhoneSettings
  /// Accept license agreement
  case AgreeWithLicenseTerm
  /// All fields must be completed
  case AllFieldsAreRequired
  /// Invalid amount format
  case AmountErrorInvalid
  /// Amount must be greater than zero
  case AmountErrorZero
  /// App Permission Denied
  case AppPermissionDenied
  /// Mobile
  case AuthorizationCellNumber
  /// Create account
  case AuthorizationCreateAccount
  /// Reset password
  case AuthorizationResetPass
  /// The balance of the client
  case BalanceOfClient
  /// bonus points
  case BonusPoints
  /// Maximum amount of cash points is
  case BonusPointsMax
  /// Активировать бонусную карту
  case BonuscardButtonActivateTitle
  /// Привязать бонусную карту
  case BonuscardButtonAttachTitle
  /// *by clicking Cancel you will reject recent transaction
  case ByClickCancel
  /// Call is not possible
  case CallUnavailable
  /// Allow access to the camera
  case CameraPermissionsRequest
  /// cash for payment
  case CashForPayment
  /// cash paid
  case CashPaid
  /// Cash points
  case CashPoints
  /// With discount
  case CashierAllPercent
  /// Total:
  case CashierAllSumm
  /// Cancel *
  case CashierCancel
  /// cash
  case CashierCash
  /// Confirm
  case CashierFinish
  /// Pay
  case CashierPay
  /// Price
  case CashierPrice
  /// Bonus points:
  case CashierResBalls
  /// Total due:
  case CashierResSumm
  /// Spend bonus points
  case CashierUseBalls
  /// Customer balance:
  case CashierUserBalls
  /// Payment with bonuses:
  case CashierUserPayBalls
  /// New version
  case CashierVersionNew
  /// Chain
  case ChainName
  /// place
  case ChainNumber1
  /// places
  case ChainNumber234
  /// places
  case ChainNumber5
  /// Choose role
  case ChooseRole
  /// Choose an Email client:
  case ChooseRoleEmail
  /// Other
  case CityOther
  /// Client cancelled transaction
  case ClientCancelledTransaction
  /// Client not found
  case ClientNotFound
  /// Completion of the transaction
  case CompletionOfTransaction
  /// Enter confirmation code
  case ConfirmationText1
  /// that was sent to
  case ConfirmationText2
  /// Connect via Facebook
  case ConnectToFb
  /// Connect via Vkontakte
  case ConnectToVk
  /// Connection error,  please try again
  case ConnectionErrorPleaseTryAgain
  ///  Internet connection is interrupted.
  case ConnectionInterrupted
  /// Connection to socket is lost
  case ConnectionLostMsg
  /// Connection lost
  case ConnectionLostTitle
  /// Customer disconnected
  case CustomerDisconnected
  /// Detailed information to connect
  case DetailedInfoToConnect
  /// No phone module.
  case DialogPhoneNoModule
  /// Phones
  case DialogPhoneTitle
  /// Please confirm transaction rejection.
  case DialogTransaction
  /// discount by QR-code
  case DiscountByQr
  /// don’t have an account?
  case DontHaveAccount
  /// Email cancelled
  case EmailCancelled
  /// Email is already registered in the system
  case EmailIsAlreadyRegisteredInTheSystem
  /// Email is invalid
  case EmailIsInvalid
  /// Login error
  case EmailLoginError
  /// Email not sent
  case EmailNotSent
  /// Email notifications
  case EmailNotificationSettings
  /// Email saved
  case EmailSaved
  /// Email sent
  case EmailSent
  /// Registration error
  case EmailSignInError
  /// Hello,I’ve got problem on
  case EmailTextBody
  /// Email will be sent by
  case EmailWillBeSent
  /// Active promotions for selected places are not found
  case EmptyPromotions
  /// or enter code
  case EnterCode
  /// enter code
  case EnterCodeLogin
  /// Enter e-mail address
  case EnterEmail
  /// Enter sms code
  case EnterSmsCode
  /// Enter the amount
  case EnterTheAmount
  /// Enter via Facebook
  case EnterViaFb
  /// Enter via Vkontakte
  case EnterViaVk
  /// Please make a payment in at least one place of a chain to invite friends to that chain
  case ErrorChainText
  /// Error: empty code
  case ErrorEmptyCode
  /// Found an error
  case ErrorFounded
  /// Invalid password
  case ErrorInvPass
  /// You are not registered in this place to invite other users
  case ErrorPlaceText
  /// Exit the application
  case ExitApplication
  /// Fail authorization
  case FailAuthorizationMessage
  /// Connection terminated. Check your Internet connection and try again
  case FailNetworkConnectionMessage
  /// Wrong place code
  case FailPlaceId
  /// Scan QR-code error
  case FailQrCode
  /// Server error
  case FailServerMessage
  /// Fail when participate to task
  case FailToParticipate
  /// Fail to load task list
  case FailsToLoadTaskList
  /// Благодарим за отзыв!
  case FaqDetailLabelThankforfeedback
  /// Эта информация была полезна?
  case FaqDetailLabelUseful
  /// Узнать подробнее
  case FaqDetailViewTitle
  /// Sorry, the section is under development
  case FaqEmptyMessage
  /// Необходимо заполнить хотя бы одно поле.
  case FaqFeedbackInvalid
  /// Эта информация была полезна?
  case FaqRatingTitle
  /// Ваш email
  case FaqSupportFieldMail
  /// Сообщение
  case FaqSupportFieldMessage
  /// Имя и Фамилия
  case FaqSupportFieldName
  /// Ваш номер телефона
  case FaqSupportFieldPhone
  /// Получить консультацию
  case FaqSupportTitle
  /// Help
  case FaqTitle
  /// Здесь приведены ответы на самые часто встречающиеся вопросы о Palladium. Если вы не нашли ответ на интересующий вас вопрос, вы можете задать его нашей
  case FaqTitleDescription
  /// Как пользоваться приложением Palladium?
  case FaqTitleHowto
  /// службе поддержки
  case FaqTitleSupport
  /// Favorite
  case FavoritePlacesTitle
  /// Fill in all fields
  case FillInAllTheFields
  /// Fill
  case FillProfFill
  /// Later
  case FillProfLater
  /// Please fill profile for Addzer correct work
  case FillProfMessage
  /// Fill your profile
  case FillProfTitle
  /// Reset filters
  case FilterReset
  /// Forgot your password?
  case ForgotPassword
  /// Error is found
  case FoundTheError
  /// Authenticated
  case GeneralAuthenticated
  /// OK
  case GeneralButtonOk
  /// SMS Code
  case GeneralCode
  /// Продолжить
  case GeneralContinue
  /// Guest
  case GeneralGuest
  /// GPS coordinates not available
  case GeneralMessageLocationOff
  /// Scanning not available
  case GeneralMessageQrscanOff
  /// Join
  case GeneralParticipate
  /// About
  case GeneralTextAbout
  /// Принять
  case GeneralTextAccept
  /// Palladium
  case GeneralTextAddzer
  /// ago
  case GeneralTextAgo
  /// Apply
  case GeneralTextApply
  /// Palladium
  case GeneralTextAppname
  /// Attention
  case GeneralTextAttention
  /// Authorization
  case GeneralTextAuthorization
  /// Back
  case GeneralTextBack
  /// Cancel
  case GeneralTextCancel
  /// Category filter
  case GeneralTextCategoryFilter
  /// Change password
  case GeneralTextChangePassword
  /// Close
  case GeneralTextClose
  /// Confirm
  case GeneralTextConfirm
  /// Connected
  case GeneralTextConnected
  /// day
  case GeneralTextDay
  /// days
  case GeneralTextDayS
  /// days
  case GeneralTextDays
  /// Отклонить
  case GeneralTextDecline
  /// Email
  case GeneralTextEmail
  /// Error
  case GeneralTextError
  /// From
  case GeneralTextFrom
  /// Get points
  case GeneralTextGetPoints
  /// Hide
  case GeneralTextHide
  /// hour
  case GeneralTextHour
  /// hours(s)
  case GeneralTextHourS
  /// hours
  case GeneralTextHours
  /// Invite
  case GeneralTextInvite
  /// km
  case GeneralTextKm
  /// Language
  case GeneralTextLanguage
  /// Please wait..
  case GeneralTextLoader
  /// Login
  case GeneralTextLogin
  /// Logout
  case GeneralTextLogout
  /// m
  case GeneralTextM
  /// Map
  case GeneralTextMap
  /// minute
  case GeneralTextMinute
  /// minutes
  case GeneralTextMinuteS
  /// minutes
  case GeneralTextMinutes
  /// month
  case GeneralTextMonth
  /// month(s)
  case GeneralTextMonthS
  /// months
  case GeneralTextMonths
  /// Name
  case GeneralTextName
  /// Next
  case GeneralTextNext
  /// or
  case GeneralTextOr
  /// Password
  case GeneralTextPassword
  /// Confirm password
  case GeneralTextPasswordConfirm
  /// Payment
  case GeneralTextPayment
  /// Profile
  case GeneralTextProfile
  /// Promo codes
  case GeneralTextPromoCodes
  /// Register
  case GeneralTextRegister
  /// Reload
  case GeneralTextReload
  /// Save
  case GeneralTextSave
  /// Scanner
  case GeneralTextScanner
  /// Send
  case GeneralTextSend
  /// Settings
  case GeneralTextSettings
  /// Successful
  case GeneralTextSuccessful
  /// Tasks
  case GeneralTextTasks
  /// Till
  case GeneralTextTill
  /// Try without sign in
  case GeneralTextTry
  /// Updating...
  case GeneralTextUpdating
  /// week
  case GeneralTextWeek
  /// weeks
  case GeneralTextWeeks
  /// Working hours
  case GeneralTextWorkingHours
  /// year
  case GeneralTextYear
  /// years
  case GeneralTextYearS
  /// years
  case GeneralTextYears
  /// Yes
  case GeneralTextYes
  /// Push here to sign in
  case GeneralTouchToRegister
  /// Get Code
  case GetCode
  /// Введите сумму платежа вручную или вернитесь и введите другой номер столика
  case GuscomErrorMessage
  /// Нет данных по столику
  case GuscomErrorTitle
  /// Номер столика
  case GuscomTableNumber
  /// Номер места
  case GuscomTablePlace
  /// Столик и место
  case GuscomTableTitle
  /// Enter email
  case HintInputEmail
  /// Пригласить друга
  case IncludeUserInfoInviteFriend
  /// Incorrect code
  case IncorrectCode
  /// Incorrect QR-code
  case IncorrectQrCode
  /// Incorrect username or password
  case IncorrectUsernameOrPassword
  /// information
  case InformationMenuTitle
  /// Input a code to text field
  case InputCodeToTextfield
  /// enter phone number
  case InputPhone
  /// Invalid code
  case InvalidCode
  /// Invalid data
  case InvalidData
  /// Invalid phone number
  case InvalidPhoneNumber
  /// To invite your friend to this chain
  case InviteCommonNetworkText1
  /// , let your friend scan the QR code below with his Palladium scanner
  case InviteCommonNetworkText2
  /// or invite to..
  case InviteTo
  /// or invite with
  case InviteWith
  /// Notifications
  case ItemActivitySettingsNotifications
  /// Social networks
  case ItemActivitySettingsSocial
  /// English
  case LangSettingsEn
  /// Estonian
  case LangSettingsEt
  /// Русский
  case LangSettingsRu
  /// Bonus`s system and promotions in cafes and restaurants
  case Launcher1Text
  /// Get a discount up to 100%, use bonuses
  case Launcher1Title
  /// Forget about loyalty cards. Show the QR code using Palladium application to the casier, get bonuses and pay the check with them.
  case Launcher2Text
  /// Always with you
  case Launcher2Title
  /// Place catalogue expands constantly.
  case Launcher3Text
  /// A ton of places already with us
  case Launcher3Title
  /// Invite your friends to the best restaurants and get bonuses for them. More friends, more benefits.
  case Launcher4Text
  /// More benefits with friends
  case Launcher4Title
  /// Know more how to get points.
  case LearnHowToGetPoints
  /// less minute ago
  case LessMinuteAgo
  /// Load QR image error
  case LoadQrError
  /// Location not available
  case LocationAvailibleMsg
  /// Please check location enabling in settings
  case LocationMsg
  /// Location unavailable
  case LocationUnavailable
  /// Login as
  case LoginAs
  /// Try without sign in
  case LoginDemo
  /// For the first time in Palladium?
  case LoginRegister
  /// or log in with
  case LoginWith
  /// Do you want to quit?
  case LogoutMessage
  /// Logout confirmation
  case LogoutTitle
  /// Done
  case MenuDone
  /// Filter
  case MenuPlaceListFilterTitle
  /// Search
  case MenuPlaceListSearchTitle
  /// place qr code
  case MenuPlaceQrTitle
  /// Support
  case MenuTitleSupport
  /// Confirmation Code Has Been Sent To Provided Phone Number
  case MessageConfirmCodeSent
  /// Phone number not confirmed
  case MessagePhoneNotConfirm
  /// Please enter your name
  case MessageProfileNotFilled
  /// Your account is inactive
  case MessageUserNotActivated
  /// User deactivated
  case MessageUserNotFound
  /// Confirmation code is invalid
  case MessageWrongConfirmCode
  /// Wrong credentials
  case MessageWrongCreds
  /// You must confirm the payment
  case MustConfirmNotifMsg
  /// Please confirm the payment
  case MustConfirmNotifTitle
  /// Выбрать заведение
  case MyInviteEmptyButton
  /// Пригласи новых друзей в понравившиеся заведения, и получай баллы за каждый их заказ. Все приглашенные ими также принесут вам дополнительные баллы!
  case MyInviteEmptyMessage
  /// Приглашайте друзей и получайте бонусные баллы!
  case MyInviteEmptyTitle
  /// Все заведения
  case MyPlacesEmptyAction
  /// Using Addzer is very easy - just choose a place nearby, then when visiting show your QR code to the waiter and start saving in your favorite places!
  case MyPlacesEmptyMessage
  /// There are currently no places you are connected to.
  case MyPlacesEmptyTitle
  /// My places
  case MyPlacesTitle
  /// My QR-code
  case MyQr
  /// Close navigation panel
  case NavigationDrawerClose
  /// Open navigation panel
  case NavigationDrawerOpen
  /// Network error
  case NetworkFailError
  /// New notification
  case NewNotification
  /// New version
  case NewVersion
  /// There is no places found around you!Refine your search.
  case NoAroundPlaces
  /// No internet connection
  case NoInternetConnection
  /// Scanner is not available.  Please enter code manually.
  case NoPermissionCamera
  /// No results
  case NoResults
  /// No places
  case NoResultsForPlaces
  /// You have no transactions.
  case NoResultsForTransactionsCashier
  /// There are no tasks yet.
  case NoTasks
  /// No, Thanks
  case NoThanks
  /// Don’t close QR-code screen before transaction confirmation. 
  case NotClose
  /// Promotion changes
  case NotificationSettingsAbout
  /// New bonus points
  case NotificationSettingsAwarding
  /// Accepted invitations
  case NotificationSettingsInvitation
  /// (en)Разрешить пуш уведомления
  case NotificationSettingsNoPush
  /// Visited places
  case NotificationSettingsOpportunities
  /// Usage statistics
  case NotificationSettingsSysStatistics
  /// or enter QR-code
  case OrEnterCodeManually
  /// Partners
  case OwnerTitle
  /// https://addzer.com/en/site/business
  case PartnersLink
  /// Password must be min 6 characters
  case PasswordMinCharacters
  /// Password must be the same
  case PasswordMustBeSame
  /// Password successfully updated
  case PasswordUpdated
  /// pay cash
  case PayCash
  /// Service
  case PayDetailsService
  /// 
  case PayDoneFail
  /// 
  case PayDoneSuccessMessage
  /// pay points
  case PayPoints
  /// Payment amount
  case PaymentAmount
  /// Баллов получено
  case PaymentPointsEarned
  /// Баллов списано
  case PaymentPointsSpend
  /// Баллов накоплено
  case PaymentPointsTotal
  /// Сумма счета
  case PaymentTotal
  /// If the scanner didn’t work, name the code to receive bonus points:
  case PersonalCode
  /// Phone number is already registered in the system
  case PhoneNumberIsAlreadyRegisteredInTheSystem
  /// Incorrect phone number!
  case PhoneValidateNumber
  /// Place is removed
  case PlaceDeleted
  /// Chain places
  case PlaceDetailsChain
  /// Place ID not found
  case PlaceIdNotFound
  /// fb://profile/714053841970568
  case PlaceMenuFacebookLinkId
  /// Error when load fullscreen photo
  case PlacePhotoFailedToLoad
  /// All places
  case PlacesAroundTitle
  /// All chain places
  case PlacesChainTitle
  /// i get
  case PlacesDetailsScore1
  /// from friends
  case PlacesDetailsScore2
  /// from others
  case PlacesDetailsScore3
  /// places
  case PlacesMenuTitle
  /// points accumulation
  case PlacesReferralScore
  /// RESTAURANTS
  case PlacesRestaurants
  /// points earned
  case PointsEarned
  /// points for payment
  case PointsForPayment
  /// points in this place
  case PointsInThisPlace
  /// points paid
  case PointsPaid
  /// Необходимо закрыть столик
  case PosTableClose
  /// price with discount
  case PriceWithDiscount
  /// I have read and accept
  case PrivacyTerms1
  /// the licence term
  case PrivacyTerms2
  /// and
  case PrivacyTerms3
  /// privacy policy
  case PrivacyTerms4
  /// age
  case ProfileAge
  /// Select from gallery
  case ProfileAvatarGallery
  /// Take a photo
  case ProfileAvatarPhoto
  /// Confirmation code not match
  case ProfileConfCodeNotMatch
  /// Confirmation sent to 
  case ProfileConfirmSent
  /// Send conformation to email error
  case ProfileConfirmSentError
  /// password confirmation
  case ProfileConfpass
  /// enter code
  case ProfileEnterCode
  /// Empty field
  case ProfileErrorEmptyField
  /// Invalid email
  case ProfileErrorInvEmail
  /// Invalid phone
  case ProfileErrorInvPhone
  /// Confirm password not match
  case ProfileErrorPassNotMatch
  /// Profile fill
  case ProfileFillrate
  /// Forgot password?
  case ProfileForgotPassword
  /// In order to get birthday gifts
  case ProfileFormBirthdayHint
  /// Enter your birthday
  case ProfileFormBirthdayPlaceholder
  /// Birthday
  case ProfileFormBirthdayTitle
  /// In order to restore access
  case ProfileFormEmailHint
  /// Enter your email
  case ProfileFormEmailPlaceholder
  /// Email
  case ProfileFormEmailTitle
  /// Your second name
  case ProfileFormLastNamePlaceholder
  /// Second name
  case ProfileFormLastNameTitle
  /// Добавить карту Palladium
  case ProfileFormLyalityCardTitle
  /// Your name
  case ProfileFormNamePlaceholder
  /// Name
  case ProfileFormNameTitle
  /// In order to log in using email address
  case ProfileFormPasswordHint
  /// Set up password
  case ProfileFormPasswordTitle
  /// Current password
  case ProfileFormPasswordChangeCurrentTitle
  /// For better security
  case ProfileFormPhoneHint
  /// Enter your phone
  case ProfileFormPhonePlaceholder
  /// Phone number
  case ProfileFormPhoneTitle
  /// new password
  case ProfileNewPassword
  /// phone
  case ProfilePhone
  /// Region
  case ProfileRegion
  /// All regions
  case ProfileRegionAll
  /// Reset email
  case ProfileResetEmail
  /// Profile save error
  case ProfileSaveError
  /// Send confirmation again
  case ProfileSendConfAgain
  /// Please, set age
  case ProfileSetAge
  /// Please, set sex
  case ProfileSetSex
  /// sex
  case ProfileSex
  /// Successful
  case ProfileSuccessful
  /// Profile successfully updated
  case ProfileSuccessfullyUpdated
  /// Profile update error
  case ProfileUpdateError
  /// If you have a promotional code Addzer, enter it and save to connect to a place
  case PromoCodesDescription
  /// Promo code must be min 6 characters
  case PromoCodesInputLengthError
  /// Введите промокод
  case PromocodesLabel
  /// К сожалению, в вашем регионе сейчас нет действующих акций от заведений
  case PromotionsEmptyMessage
  /// Нет акций
  case PromotionsEmptyTitle
  /// Promotions
  case PromotionsTitle
  /// Push notifications
  case PushNotificationSettings
  /// QR code in place
  case QrCodeInPlace
  /// QR-code reader
  case QrCodeReader
  /// If you enjoy using Palladium, would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!
  case RatingMessage
  /// Rate It Now
  case RatingNow
  /// Rating Palladium
  case RatingTitle
  /// Complete
  case RedeemCompleteTitle
  /// Confirmation
  case RedeemConfirmation
  /// Registration successful
  case RegisteredSuccessful
  /// Already with us?
  case RegistrationLogin
  /// Registration not possible
  case RegistrationNotPossible
  /// Проверьте код страны и введите Ваш номер телефона
  case RegistrationPhoneMessage
  /// By entering, you will be deemed to have read and understood these
  case RegistrationPrivacyLicense
  /// We do not make any publications on your behalf
  case RegistrationPrivacySocial
  /// Remind Me Later
  case RemindLater
  /// Request timeout
  case RequestTimeout
  /// Resend SMS
  case RetryConfirmCode
  /// Cashier
  case RoleCashier
  /// Client
  case RoleClient
  /// Rollback successful
  case RollbackSuccessful
  /// scan qr-code
  case ScanQrCode
  /// select social
  case SelectNetwork
  /// Sent
  case SentMail
  /// Server error occurred
  case ServerErrorOccurred
  /// Please login again.
  case SessionIsOver
  /// Your email client has not been set up. Set it up and repeat email sending.
  case SetUpEmailClientAndRepeat
  /// Error while sending: 
  case ShareError
  /// Sent
  case ShareLinkSent
  /// Код
  case SharePlaceCode
  /// Not send. May be this twit already posted
  case ShareTwitError
  /// Show QR-code to the waiter
  case ShowQrCode
  /// To invite friend
  case ShowQrCodeInOrderToInvite1
  /// , your friend should scan QR code with Palladium
  case ShowQrCodeInOrderToInvite2
  /// Пригласи новых друзей и получай
  case ShowQrCodeLabel1
  /// бонусных баллов за каждый их заказ. Каждый приглашенный ими принесет Вам
  case ShowQrCodeLabel2
  /// Show this code to your friends
  case ShowQrCodeToFriends
  /// Sign In
  case SignIn
  /// Sign Up
  case SignUp
  /// Facebook
  case SocialSettingsFb
  /// Google+
  case SocialSettingsGp
  /// Odnoklassniki
  case SocialSettingsOk
  /// Twitter
  case SocialSettingsTw
  /// VKontakte
  case SocialSettingsVk
  /// The operation could not be completed
  case SocketClosed
  /// Statistic info
  case StatDialogTitle
  /// Successful
  case SuccessfulSettings
  /// support@addzer.com
  case SupportEmail
  /// Tap for continue scanning
  case TapForContinueScanning
  /// Canceled
  case TaskCanceled
  /// Completed
  case TaskComplete
  /// Confirmed
  case TaskConfirmed
  /// Expired
  case TaskExpired
  /// К сожалению, сейчас нет активных заданий от заведений, к которым вы подключены
  case TaskListEmptyMessage
  /// Нет заданий
  case TaskListEmptyTitle
  /// There is no user with such email.
  case ThereNoUserWithSuchEmail
  /// Complete transaction
  case TitleActivityCashierComplete
  /// Payment info
  case TitleActivityCashierPaymentInfo
  /// Login via Email
  case TitleActivityEmailLogin
  /// Email registration
  case TitleActivityEmailSignIn
  /// GalleryActivity
  case TitleActivityGallery
  /// LoginActivity
  case TitleActivityLogin
  /// New discounts
  case TitleActivityNewPromotion
  /// Description
  case TitleActivityPlaceDetails
  /// QR Places
  case TitleActivityPlacesQr
  /// Discount details
  case TitleActivityPromotionDetails
  /// QR-code
  case TitleActivityQrCode
  /// QR-code Scanner
  case TitleActivityQrScanner
  /// SettingActivity
  case TitleActivitySetting
  /// Share link
  case TitleActivityShareLink
  /// FAQ
  case TitleFaq
  /// Privacy policy
  case TitlePrivacy
  /// Language settings
  case TitleSettingsLanguage
  /// Notification settings
  case TitleSettingsNotification
  /// Social network settings
  case TitleSettingsSocialNetwork
  /// Share
  case TitleSocialShareLink
  /// Palladium support request 
  case TitleSupport
  /// Licence term
  case TitleTerms
  /// Transactions
  case TitleTransactions
  /// History
  case TitleTransactionsHistory
  /// History
  case TitleTransactionsTabHistory
  /// To re-enable, please go to Settings and turn on Location Service for this app.
  case ToReenablePermission
  /// Transaction cancelled
  case TransactionCancelled
  /// Transaction successfully complete
  case TransactionComplete
  /// Transaction error
  case TransactionError
  /// Transaction not apply
  case TransactionNotApply
  /// Ужасно
  case TransactionRate1
  /// Плохо
  case TransactionRate2
  /// Удовлетворительно
  case TransactionRate3
  /// Хорошо
  case TransactionRate4
  /// Отлично!
  case TransactionRate5
  /// Ваша оценка:
  case TransactionRateDone
  /// Оцените посещение
  case TransactionRateMessage
  /// Подтверждена
  case TransactionStatusApproved
  /// Не подтверждена
  case TransactionStatusProcessing
  /// Отменена
  case TransactionStatusRejected
  /// Friends history
  case TransactionsFriendsHistory
  /// income
  case TransactionsIncome
  /// income
  case TransactionsIncomeCashier
  /// У Вас пока нет оплат и бонусных баллов
  case TransactionsListEmptyMessage
  /// money
  case TransactionsMoneyCashier
  /// You have no transactions. No points for payment.
  case TransactionsNotFound
  /// outcome
  case TransactionsOutcome
  /// outcome
  case TransactionsOutcomeCashier
  /// points
  case TransactionsPointsCashier
  /// Redeem points
  case TransactionsRedeemPoints
  /// Try again
  case TryAgain
  /// Try again later
  case TryAgainLater
  /// Info about the place
  case Tut11
  /// Push to open the additional info about the place
  case Tut12
  /// Invite a friend
  case Tut13
  /// Push to invite a friend. Select the place from the list and show its QR-code to a friend
  case Tut14
  /// Menu sections
  case Tut21
  /// Select the menu section to look through promotions, transactions history and other info.
  case Tut22
  /// Invite
  case Tut31
  /// Invite friends to get more bonus points
  case Tut32
  /// My QR-code
  case Tut33
  /// Show QR-code to get bonus points
  case Tut34
  /// Scan
  case Tut35
  /// Scan QR-code to join the place
  case Tut36
  /// My orders
  case Tut41
  /// Friends’ orders
  case Tut42
  /// Friends of friends’orders
  case Tut43
  /// Get bonus points from
  case Tut44
  /// Previous
  case TutorialBtnPrev
  /// Tutorial
  case TutorialTitle
  /// Unknown error
  case UnknownError
  /// https://addzer.com/en/page/privacy-policy-rules
  case UrlPrivacy
  /// https://addzer.com/en/page/license-agreement
  case UrlTerms
  /// Use points
  case UsePoints
  /// License agreements
  case UserAgreements
  /// User is not registered to pay with points
  case UserNotRegistered
  /// Mandatory update is available for downloading. It is highly recommended to update Addzer for correct application work.
  case VersionMessage
  /// Are you sure you want to cancel the registration?
  case WantToCancelRegistrationText
  /// Cheers!
  case WelcomeButtonText
  /// Brands
  case WelcomeSubjectTitleText
  /// Brands can be viewed in the place card
  case WelcomeSubtitleText
  /// The long-awaited update!
  case WelcomeTitleText
  /// Hello, I would like to invite you to
  case WouldLikeToInviteYou
  /// wrong code
  case WrongCode
  /// You are not registered anywhere. Scan QR-code of the place for registration.
  case YouDontHaveMyPlaces
  /// You must setup email client
  case YouMustSetupEmailClient
}

extension L10n: CustomStringConvertible {
  var description: String { return self.string }

  var string: String {
    switch self {
      case .AcceptInvitation:
        return L10n.tr("accept.invitation")
      case .ActionExit:
        return L10n.tr("action.exit")
      case .ActivateTwitterAccountInPhoneSettings:
        return L10n.tr("activate.twitter.account.in.phone.settings")
      case .AgreeWithLicenseTerm:
        return L10n.tr("agree.with.license.term")
      case .AllFieldsAreRequired:
        return L10n.tr("all.fields.are.required")
      case .AmountErrorInvalid:
        return L10n.tr("amount.error.invalid")
      case .AmountErrorZero:
        return L10n.tr("amount.error.zero")
      case .AppPermissionDenied:
        return L10n.tr("app.permission.denied")
      case .AuthorizationCellNumber:
        return L10n.tr("authorization.cell.number")
      case .AuthorizationCreateAccount:
        return L10n.tr("authorization.create.account")
      case .AuthorizationResetPass:
        return L10n.tr("authorization.reset.pass")
      case .BalanceOfClient:
        return L10n.tr("balance.of.client")
      case .BonusPoints:
        return L10n.tr("bonus.points")
      case .BonusPointsMax:
        return L10n.tr("bonus.points.max")
      case .BonuscardButtonActivateTitle:
        return L10n.tr("bonuscard.button.activate.title")
      case .BonuscardButtonAttachTitle:
        return L10n.tr("bonuscard.button.attach.title")
      case .ByClickCancel:
        return L10n.tr("by.click.cancel")
      case .CallUnavailable:
        return L10n.tr("call.unavailable")
      case .CameraPermissionsRequest:
        return L10n.tr("camera.permissions.request")
      case .CashForPayment:
        return L10n.tr("cash.for.payment")
      case .CashPaid:
        return L10n.tr("cash.paid")
      case .CashPoints:
        return L10n.tr("cash.points")
      case .CashierAllPercent:
        return L10n.tr("cashier.all.percent")
      case .CashierAllSumm:
        return L10n.tr("cashier.all.summ")
      case .CashierCancel:
        return L10n.tr("cashier.cancel")
      case .CashierCash:
        return L10n.tr("cashier.cash")
      case .CashierFinish:
        return L10n.tr("cashier.finish")
      case .CashierPay:
        return L10n.tr("cashier.pay")
      case .CashierPrice:
        return L10n.tr("cashier.price")
      case .CashierResBalls:
        return L10n.tr("cashier.res.balls")
      case .CashierResSumm:
        return L10n.tr("cashier.res.summ")
      case .CashierUseBalls:
        return L10n.tr("cashier.use.balls")
      case .CashierUserBalls:
        return L10n.tr("cashier.user.balls")
      case .CashierUserPayBalls:
        return L10n.tr("cashier.user.pay.balls")
      case .CashierVersionNew:
        return L10n.tr("cashier.version.new")
      case .ChainName:
        return L10n.tr("chain.name")
      case .ChainNumber1:
        return L10n.tr("chain.number.1")
      case .ChainNumber234:
        return L10n.tr("chain.number.234")
      case .ChainNumber5:
        return L10n.tr("chain.number.5")
      case .ChooseRole:
        return L10n.tr("choose.role")
      case .ChooseRoleEmail:
        return L10n.tr("choose.role.email")
      case .CityOther:
        return L10n.tr("city.other")
      case .ClientCancelledTransaction:
        return L10n.tr("client.cancelled.transaction")
      case .ClientNotFound:
        return L10n.tr("client.not.found")
      case .CompletionOfTransaction:
        return L10n.tr("completion.of.transaction")
      case .ConfirmationText1:
        return L10n.tr("confirmation.text1")
      case .ConfirmationText2:
        return L10n.tr("confirmation.text2")
      case .ConnectToFb:
        return L10n.tr("connect.to.fb")
      case .ConnectToVk:
        return L10n.tr("connect.to.vk")
      case .ConnectionErrorPleaseTryAgain:
        return L10n.tr("connection.error.please.try.again")
      case .ConnectionInterrupted:
        return L10n.tr("connection.interrupted")
      case .ConnectionLostMsg:
        return L10n.tr("connection.lost.msg")
      case .ConnectionLostTitle:
        return L10n.tr("connection.lost.title")
      case .CustomerDisconnected:
        return L10n.tr("customer.disconnected")
      case .DetailedInfoToConnect:
        return L10n.tr("detailed.info.to.connect")
      case .DialogPhoneNoModule:
        return L10n.tr("dialog.phone.no.module")
      case .DialogPhoneTitle:
        return L10n.tr("dialog.phone.title")
      case .DialogTransaction:
        return L10n.tr("dialog.transaction")
      case .DiscountByQr:
        return L10n.tr("discount.by.qr")
      case .DontHaveAccount:
        return L10n.tr("dont.have.account")
      case .EmailCancelled:
        return L10n.tr("email.cancelled")
      case .EmailIsAlreadyRegisteredInTheSystem:
        return L10n.tr("email.is.already.registered.in.the.system")
      case .EmailIsInvalid:
        return L10n.tr("email.is.invalid")
      case .EmailLoginError:
        return L10n.tr("email.login.error")
      case .EmailNotSent:
        return L10n.tr("email.not.sent")
      case .EmailNotificationSettings:
        return L10n.tr("email.notification.settings")
      case .EmailSaved:
        return L10n.tr("email.saved")
      case .EmailSent:
        return L10n.tr("email.sent")
      case .EmailSignInError:
        return L10n.tr("email.sign.in.error")
      case .EmailTextBody:
        return L10n.tr("email.text.body")
      case .EmailWillBeSent:
        return L10n.tr("email.will.be.sent")
      case .EmptyPromotions:
        return L10n.tr("empty.promotions")
      case .EnterCode:
        return L10n.tr("enter.code")
      case .EnterCodeLogin:
        return L10n.tr("enter.code.login")
      case .EnterEmail:
        return L10n.tr("enter.email")
      case .EnterSmsCode:
        return L10n.tr("enter.sms.code")
      case .EnterTheAmount:
        return L10n.tr("enter.the.amount")
      case .EnterViaFb:
        return L10n.tr("enter.via.fb")
      case .EnterViaVk:
        return L10n.tr("enter.via.vk")
      case .ErrorChainText:
        return L10n.tr("error.chain.text")
      case .ErrorEmptyCode:
        return L10n.tr("error.empty.code")
      case .ErrorFounded:
        return L10n.tr("error.founded")
      case .ErrorInvPass:
        return L10n.tr("error.inv.pass")
      case .ErrorPlaceText:
        return L10n.tr("error.place.text")
      case .ExitApplication:
        return L10n.tr("exit.application")
      case .FailAuthorizationMessage:
        return L10n.tr("fail.authorization.message")
      case .FailNetworkConnectionMessage:
        return L10n.tr("fail.network.connection.message")
      case .FailPlaceId:
        return L10n.tr("fail.place.id")
      case .FailQrCode:
        return L10n.tr("fail.qr.code")
      case .FailServerMessage:
        return L10n.tr("fail.server.message")
      case .FailToParticipate:
        return L10n.tr("fail.to.participate")
      case .FailsToLoadTaskList:
        return L10n.tr("fails.to.load.task.list")
      case .FaqDetailLabelThankforfeedback:
        return L10n.tr("faq.detail.label.thankforfeedback")
      case .FaqDetailLabelUseful:
        return L10n.tr("faq.detail.label.useful")
      case .FaqDetailViewTitle:
        return L10n.tr("faq.detail.view.title")
      case .FaqEmptyMessage:
        return L10n.tr("faq.empty.message")
      case .FaqFeedbackInvalid:
        return L10n.tr("faq.feedback.invalid")
      case .FaqRatingTitle:
        return L10n.tr("faq.rating.title")
      case .FaqSupportFieldMail:
        return L10n.tr("faq.support.field.mail")
      case .FaqSupportFieldMessage:
        return L10n.tr("faq.support.field.message")
      case .FaqSupportFieldName:
        return L10n.tr("faq.support.field.name")
      case .FaqSupportFieldPhone:
        return L10n.tr("faq.support.field.phone")
      case .FaqSupportTitle:
        return L10n.tr("faq.support.title")
      case .FaqTitle:
        return L10n.tr("faq.title")
      case .FaqTitleDescription:
        return L10n.tr("faq.title.description")
      case .FaqTitleHowto:
        return L10n.tr("faq.title.howto")
      case .FaqTitleSupport:
        return L10n.tr("faq.title.support")
      case .FavoritePlacesTitle:
        return L10n.tr("favorite.places.title")
      case .FillInAllTheFields:
        return L10n.tr("fill.in.all.the.fields")
      case .FillProfFill:
        return L10n.tr("fill.prof.fill")
      case .FillProfLater:
        return L10n.tr("fill.prof.later")
      case .FillProfMessage:
        return L10n.tr("fill.prof.message")
      case .FillProfTitle:
        return L10n.tr("fill.prof.title")
      case .FilterReset:
        return L10n.tr("filter.reset")
      case .ForgotPassword:
        return L10n.tr("forgot.password")
      case .FoundTheError:
        return L10n.tr("found.the.error")
      case .GeneralAuthenticated:
        return L10n.tr("general.authenticated")
      case .GeneralButtonOk:
        return L10n.tr("general.button.ok")
      case .GeneralCode:
        return L10n.tr("general.code")
      case .GeneralContinue:
        return L10n.tr("general.continue")
      case .GeneralGuest:
        return L10n.tr("general.guest")
      case .GeneralMessageLocationOff:
        return L10n.tr("general.message.location.off")
      case .GeneralMessageQrscanOff:
        return L10n.tr("general.message.qrscan.off")
      case .GeneralParticipate:
        return L10n.tr("general.participate")
      case .GeneralTextAbout:
        return L10n.tr("general.text.about")
      case .GeneralTextAccept:
        return L10n.tr("general.text.accept")
      case .GeneralTextAddzer:
        return L10n.tr("general.text.addzer")
      case .GeneralTextAgo:
        return L10n.tr("general.text.ago")
      case .GeneralTextApply:
        return L10n.tr("general.text.apply")
      case .GeneralTextAppname:
        return L10n.tr("general.text.appname")
      case .GeneralTextAttention:
        return L10n.tr("general.text.attention")
      case .GeneralTextAuthorization:
        return L10n.tr("general.text.authorization")
      case .GeneralTextBack:
        return L10n.tr("general.text.back")
      case .GeneralTextCancel:
        return L10n.tr("general.text.cancel")
      case .GeneralTextCategoryFilter:
        return L10n.tr("general.text.category.filter")
      case .GeneralTextChangePassword:
        return L10n.tr("general.text.change.password")
      case .GeneralTextClose:
        return L10n.tr("general.text.close")
      case .GeneralTextConfirm:
        return L10n.tr("general.text.confirm")
      case .GeneralTextConnected:
        return L10n.tr("general.text.connected")
      case .GeneralTextDay:
        return L10n.tr("general.text.day")
      case .GeneralTextDayS:
        return L10n.tr("general.text.day.s")
      case .GeneralTextDays:
        return L10n.tr("general.text.days")
      case .GeneralTextDecline:
        return L10n.tr("general.text.decline")
      case .GeneralTextEmail:
        return L10n.tr("general.text.email")
      case .GeneralTextError:
        return L10n.tr("general.text.error")
      case .GeneralTextFrom:
        return L10n.tr("general.text.from")
      case .GeneralTextGetPoints:
        return L10n.tr("general.text.get.points")
      case .GeneralTextHide:
        return L10n.tr("general.text.hide")
      case .GeneralTextHour:
        return L10n.tr("general.text.hour")
      case .GeneralTextHourS:
        return L10n.tr("general.text.hour.s")
      case .GeneralTextHours:
        return L10n.tr("general.text.hours")
      case .GeneralTextInvite:
        return L10n.tr("general.text.invite")
      case .GeneralTextKm:
        return L10n.tr("general.text.km")
      case .GeneralTextLanguage:
        return L10n.tr("general.text.language")
      case .GeneralTextLoader:
        return L10n.tr("general.text.loader")
      case .GeneralTextLogin:
        return L10n.tr("general.text.login")
      case .GeneralTextLogout:
        return L10n.tr("general.text.logout")
      case .GeneralTextM:
        return L10n.tr("general.text.m")
      case .GeneralTextMap:
        return L10n.tr("general.text.map")
      case .GeneralTextMinute:
        return L10n.tr("general.text.minute")
      case .GeneralTextMinuteS:
        return L10n.tr("general.text.minute.s")
      case .GeneralTextMinutes:
        return L10n.tr("general.text.minutes")
      case .GeneralTextMonth:
        return L10n.tr("general.text.month")
      case .GeneralTextMonthS:
        return L10n.tr("general.text.month.s")
      case .GeneralTextMonths:
        return L10n.tr("general.text.months")
      case .GeneralTextName:
        return L10n.tr("general.text.name")
      case .GeneralTextNext:
        return L10n.tr("general.text.next")
      case .GeneralTextOr:
        return L10n.tr("general.text.or")
      case .GeneralTextPassword:
        return L10n.tr("general.text.password")
      case .GeneralTextPasswordConfirm:
        return L10n.tr("general.text.password.confirm")
      case .GeneralTextPayment:
        return L10n.tr("general.text.payment")
      case .GeneralTextProfile:
        return L10n.tr("general.text.profile")
      case .GeneralTextPromoCodes:
        return L10n.tr("general.text.promo.codes")
      case .GeneralTextRegister:
        return L10n.tr("general.text.register")
      case .GeneralTextReload:
        return L10n.tr("general.text.reload")
      case .GeneralTextSave:
        return L10n.tr("general.text.save")
      case .GeneralTextScanner:
        return L10n.tr("general.text.scanner")
      case .GeneralTextSend:
        return L10n.tr("general.text.send")
      case .GeneralTextSettings:
        return L10n.tr("general.text.settings")
      case .GeneralTextSuccessful:
        return L10n.tr("general.text.successful")
      case .GeneralTextTasks:
        return L10n.tr("general.text.tasks")
      case .GeneralTextTill:
        return L10n.tr("general.text.till")
      case .GeneralTextTry:
        return L10n.tr("general.text.try")
      case .GeneralTextUpdating:
        return L10n.tr("general.text.updating")
      case .GeneralTextWeek:
        return L10n.tr("general.text.week")
      case .GeneralTextWeeks:
        return L10n.tr("general.text.weeks")
      case .GeneralTextWorkingHours:
        return L10n.tr("general.text.working.hours")
      case .GeneralTextYear:
        return L10n.tr("general.text.year")
      case .GeneralTextYearS:
        return L10n.tr("general.text.year.s")
      case .GeneralTextYears:
        return L10n.tr("general.text.years")
      case .GeneralTextYes:
        return L10n.tr("general.text.yes")
      case .GeneralTouchToRegister:
        return L10n.tr("general.touch.to.register")
      case .GetCode:
        return L10n.tr("get.code")
      case .GuscomErrorMessage:
        return L10n.tr("guscom.error.message")
      case .GuscomErrorTitle:
        return L10n.tr("guscom.error.title")
      case .GuscomTableNumber:
        return L10n.tr("guscom.table.number")
      case .GuscomTablePlace:
        return L10n.tr("guscom.table.place")
      case .GuscomTableTitle:
        return L10n.tr("guscom.table.title")
      case .HintInputEmail:
        return L10n.tr("hint.input.email")
      case .IncludeUserInfoInviteFriend:
        return L10n.tr("include.user.info.invite.friend")
      case .IncorrectCode:
        return L10n.tr("incorrect.code")
      case .IncorrectQrCode:
        return L10n.tr("incorrect.qr.code")
      case .IncorrectUsernameOrPassword:
        return L10n.tr("incorrect.username.or.password")
      case .InformationMenuTitle:
        return L10n.tr("information.menu.title")
      case .InputCodeToTextfield:
        return L10n.tr("input.code.to.textfield")
      case .InputPhone:
        return L10n.tr("input.phone")
      case .InvalidCode:
        return L10n.tr("invalid.code")
      case .InvalidData:
        return L10n.tr("invalid.data")
      case .InvalidPhoneNumber:
        return L10n.tr("invalid.phone.number")
      case .InviteCommonNetworkText1:
        return L10n.tr("invite.common.network.text1")
      case .InviteCommonNetworkText2:
        return L10n.tr("invite.common.network.text2")
      case .InviteTo:
        return L10n.tr("invite.to")
      case .InviteWith:
        return L10n.tr("invite.with")
      case .ItemActivitySettingsNotifications:
        return L10n.tr("item.activity.settings.notifications")
      case .ItemActivitySettingsSocial:
        return L10n.tr("item.activity.settings.social")
      case .LangSettingsEn:
        return L10n.tr("lang.settings.en")
      case .LangSettingsEt:
        return L10n.tr("lang.settings.et")
      case .LangSettingsRu:
        return L10n.tr("lang.settings.ru")
      case .Launcher1Text:
        return L10n.tr("launcher.1.text")
      case .Launcher1Title:
        return L10n.tr("launcher.1.title")
      case .Launcher2Text:
        return L10n.tr("launcher.2.text")
      case .Launcher2Title:
        return L10n.tr("launcher.2.title")
      case .Launcher3Text:
        return L10n.tr("launcher.3.text")
      case .Launcher3Title:
        return L10n.tr("launcher.3.title")
      case .Launcher4Text:
        return L10n.tr("launcher.4.text")
      case .Launcher4Title:
        return L10n.tr("launcher.4.title")
      case .LearnHowToGetPoints:
        return L10n.tr("learn.how.to.get.points")
      case .LessMinuteAgo:
        return L10n.tr("less.minute.ago")
      case .LoadQrError:
        return L10n.tr("load.qr.error")
      case .LocationAvailibleMsg:
        return L10n.tr("location.availible.msg")
      case .LocationMsg:
        return L10n.tr("location.msg")
      case .LocationUnavailable:
        return L10n.tr("location.unavailable")
      case .LoginAs:
        return L10n.tr("login.as")
      case .LoginDemo:
        return L10n.tr("login.demo")
      case .LoginRegister:
        return L10n.tr("login.register")
      case .LoginWith:
        return L10n.tr("login.with")
      case .LogoutMessage:
        return L10n.tr("logout.message")
      case .LogoutTitle:
        return L10n.tr("logout.title")
      case .MenuDone:
        return L10n.tr("menu.done")
      case .MenuPlaceListFilterTitle:
        return L10n.tr("menu.place.list.filter.title")
      case .MenuPlaceListSearchTitle:
        return L10n.tr("menu.place.list.search.title")
      case .MenuPlaceQrTitle:
        return L10n.tr("menu.place.qr.title")
      case .MenuTitleSupport:
        return L10n.tr("menu.title.support")
      case .MessageConfirmCodeSent:
        return L10n.tr("message.confirm.code.sent")
      case .MessagePhoneNotConfirm:
        return L10n.tr("message.phone.not.confirm")
      case .MessageProfileNotFilled:
        return L10n.tr("message.profile.not.filled")
      case .MessageUserNotActivated:
        return L10n.tr("message.user.not.activated")
      case .MessageUserNotFound:
        return L10n.tr("message.user.not.found")
      case .MessageWrongConfirmCode:
        return L10n.tr("message.wrong.confirm.code")
      case .MessageWrongCreds:
        return L10n.tr("message.wrong.creds")
      case .MustConfirmNotifMsg:
        return L10n.tr("must.confirm.notif.msg")
      case .MustConfirmNotifTitle:
        return L10n.tr("must.confirm.notif.title")
      case .MyInviteEmptyButton:
        return L10n.tr("my.invite.empty.button")
      case .MyInviteEmptyMessage:
        return L10n.tr("my.invite.empty.message")
      case .MyInviteEmptyTitle:
        return L10n.tr("my.invite.empty.title")
      case .MyPlacesEmptyAction:
        return L10n.tr("my.places.empty.action")
      case .MyPlacesEmptyMessage:
        return L10n.tr("my.places.empty.message")
      case .MyPlacesEmptyTitle:
        return L10n.tr("my.places.empty.title")
      case .MyPlacesTitle:
        return L10n.tr("my.places.title")
      case .MyQr:
        return L10n.tr("my.qr")
      case .NavigationDrawerClose:
        return L10n.tr("navigation.drawer.close")
      case .NavigationDrawerOpen:
        return L10n.tr("navigation.drawer.open")
      case .NetworkFailError:
        return L10n.tr("network.fail.error")
      case .NewNotification:
        return L10n.tr("new.notification")
      case .NewVersion:
        return L10n.tr("new.version")
      case .NoAroundPlaces:
        return L10n.tr("no.around.places")
      case .NoInternetConnection:
        return L10n.tr("no.internet.connection")
      case .NoPermissionCamera:
        return L10n.tr("no.permission.camera")
      case .NoResults:
        return L10n.tr("no.results")
      case .NoResultsForPlaces:
        return L10n.tr("no.results.for.places")
      case .NoResultsForTransactionsCashier:
        return L10n.tr("no.results.for.transactions.cashier")
      case .NoTasks:
        return L10n.tr("no.tasks")
      case .NoThanks:
        return L10n.tr("no.thanks")
      case .NotClose:
        return L10n.tr("not.close")
      case .NotificationSettingsAbout:
        return L10n.tr("notification.settings.about")
      case .NotificationSettingsAwarding:
        return L10n.tr("notification.settings.awarding")
      case .NotificationSettingsInvitation:
        return L10n.tr("notification.settings.invitation")
      case .NotificationSettingsNoPush:
        return L10n.tr("notification.settings.no.push")
      case .NotificationSettingsOpportunities:
        return L10n.tr("notification.settings.opportunities")
      case .NotificationSettingsSysStatistics:
        return L10n.tr("notification.settings.sys.statistics")
      case .OrEnterCodeManually:
        return L10n.tr("or.enter.code.manually")
      case .OwnerTitle:
        return L10n.tr("owner.title")
      case .PartnersLink:
        return L10n.tr("partners_link")
      case .PasswordMinCharacters:
        return L10n.tr("password.min.characters")
      case .PasswordMustBeSame:
        return L10n.tr("password.must.be.same")
      case .PasswordUpdated:
        return L10n.tr("password.updated")
      case .PayCash:
        return L10n.tr("pay.cash")
      case .PayDetailsService:
        return L10n.tr("pay.details.service")
      case .PayDoneFail:
        return L10n.tr("pay.done.fail")
      case .PayDoneSuccessMessage:
        return L10n.tr("pay.done.success.message")
      case .PayPoints:
        return L10n.tr("pay.points")
      case .PaymentAmount:
        return L10n.tr("payment.amount")
      case .PaymentPointsEarned:
        return L10n.tr("payment.points.earned")
      case .PaymentPointsSpend:
        return L10n.tr("payment.points.spend")
      case .PaymentPointsTotal:
        return L10n.tr("payment.points_total")
      case .PaymentTotal:
        return L10n.tr("payment.total")
      case .PersonalCode:
        return L10n.tr("personal.code")
      case .PhoneNumberIsAlreadyRegisteredInTheSystem:
        return L10n.tr("phone.number.is.already.registered.in.the.system")
      case .PhoneValidateNumber:
        return L10n.tr("phone.validate.number")
      case .PlaceDeleted:
        return L10n.tr("place.deleted")
      case .PlaceDetailsChain:
        return L10n.tr("place.details.chain")
      case .PlaceIdNotFound:
        return L10n.tr("place.id.not.found")
      case .PlaceMenuFacebookLinkId:
        return L10n.tr("place.menu.facebook.link.id")
      case .PlacePhotoFailedToLoad:
        return L10n.tr("place.photo.failed.to.load")
      case .PlacesAroundTitle:
        return L10n.tr("places.around.title")
      case .PlacesChainTitle:
        return L10n.tr("places.chain.title")
      case .PlacesDetailsScore1:
        return L10n.tr("places.details.score1")
      case .PlacesDetailsScore2:
        return L10n.tr("places.details.score2")
      case .PlacesDetailsScore3:
        return L10n.tr("places.details.score3")
      case .PlacesMenuTitle:
        return L10n.tr("places.menu.title")
      case .PlacesReferralScore:
        return L10n.tr("places.referral.score")
      case .PlacesRestaurants:
        return L10n.tr("places.restaurants")
      case .PointsEarned:
        return L10n.tr("points.earned")
      case .PointsForPayment:
        return L10n.tr("points.for.payment")
      case .PointsInThisPlace:
        return L10n.tr("points.in.this.place")
      case .PointsPaid:
        return L10n.tr("points.paid")
      case .PosTableClose:
        return L10n.tr("pos.table.close")
      case .PriceWithDiscount:
        return L10n.tr("price.with.discount")
      case .PrivacyTerms1:
        return L10n.tr("privacy.terms.1")
      case .PrivacyTerms2:
        return L10n.tr("privacy.terms.2")
      case .PrivacyTerms3:
        return L10n.tr("privacy.terms.3")
      case .PrivacyTerms4:
        return L10n.tr("privacy.terms.4")
      case .ProfileAge:
        return L10n.tr("profile.age")
      case .ProfileAvatarGallery:
        return L10n.tr("profile.avatar.gallery")
      case .ProfileAvatarPhoto:
        return L10n.tr("profile.avatar.photo")
      case .ProfileConfCodeNotMatch:
        return L10n.tr("profile.conf.code.not.match")
      case .ProfileConfirmSent:
        return L10n.tr("profile.confirm.sent")
      case .ProfileConfirmSentError:
        return L10n.tr("profile.confirm.sent.error")
      case .ProfileConfpass:
        return L10n.tr("profile.confpass")
      case .ProfileEnterCode:
        return L10n.tr("profile.enter.code")
      case .ProfileErrorEmptyField:
        return L10n.tr("profile.error.empty.field")
      case .ProfileErrorInvEmail:
        return L10n.tr("profile.error.inv.email")
      case .ProfileErrorInvPhone:
        return L10n.tr("profile.error.inv.phone")
      case .ProfileErrorPassNotMatch:
        return L10n.tr("profile.error.pass.not.match")
      case .ProfileFillrate:
        return L10n.tr("profile.fillrate")
      case .ProfileForgotPassword:
        return L10n.tr("profile.forgot.password")
      case .ProfileFormBirthdayHint:
        return L10n.tr("profile.form.birthday.hint")
      case .ProfileFormBirthdayPlaceholder:
        return L10n.tr("profile.form.birthday.placeholder")
      case .ProfileFormBirthdayTitle:
        return L10n.tr("profile.form.birthday.title")
      case .ProfileFormEmailHint:
        return L10n.tr("profile.form.email.hint")
      case .ProfileFormEmailPlaceholder:
        return L10n.tr("profile.form.email.placeholder")
      case .ProfileFormEmailTitle:
        return L10n.tr("profile.form.email.title")
      case .ProfileFormLastNamePlaceholder:
        return L10n.tr("profile.form.last_name.placeholder")
      case .ProfileFormLastNameTitle:
        return L10n.tr("profile.form.last_name.title")
      case .ProfileFormLyalityCardTitle:
        return L10n.tr("profile.form.lyality.card.title")
      case .ProfileFormNamePlaceholder:
        return L10n.tr("profile.form.name.placeholder")
      case .ProfileFormNameTitle:
        return L10n.tr("profile.form.name.title")
      case .ProfileFormPasswordHint:
        return L10n.tr("profile.form.password.hint")
      case .ProfileFormPasswordTitle:
        return L10n.tr("profile.form.password.title")
      case .ProfileFormPasswordChangeCurrentTitle:
        return L10n.tr("profile.form.password_change.current.title")
      case .ProfileFormPhoneHint:
        return L10n.tr("profile.form.phone.hint")
      case .ProfileFormPhonePlaceholder:
        return L10n.tr("profile.form.phone.placeholder")
      case .ProfileFormPhoneTitle:
        return L10n.tr("profile.form.phone.title")
      case .ProfileNewPassword:
        return L10n.tr("profile.new.password")
      case .ProfilePhone:
        return L10n.tr("profile.phone")
      case .ProfileRegion:
        return L10n.tr("profile.region")
      case .ProfileRegionAll:
        return L10n.tr("profile.region.all")
      case .ProfileResetEmail:
        return L10n.tr("profile.reset.email")
      case .ProfileSaveError:
        return L10n.tr("profile.save.error")
      case .ProfileSendConfAgain:
        return L10n.tr("profile.send.conf.again")
      case .ProfileSetAge:
        return L10n.tr("profile.set.age")
      case .ProfileSetSex:
        return L10n.tr("profile.set.sex")
      case .ProfileSex:
        return L10n.tr("profile.sex")
      case .ProfileSuccessful:
        return L10n.tr("profile.successful")
      case .ProfileSuccessfullyUpdated:
        return L10n.tr("profile.successfully.updated")
      case .ProfileUpdateError:
        return L10n.tr("profile.update.error")
      case .PromoCodesDescription:
        return L10n.tr("promo.codes.description")
      case .PromoCodesInputLengthError:
        return L10n.tr("promo.codes.input.length.error")
      case .PromocodesLabel:
        return L10n.tr("promocodes.label")
      case .PromotionsEmptyMessage:
        return L10n.tr("promotions.empty.message")
      case .PromotionsEmptyTitle:
        return L10n.tr("promotions.empty.title")
      case .PromotionsTitle:
        return L10n.tr("promotions.title")
      case .PushNotificationSettings:
        return L10n.tr("push.notification.settings")
      case .QrCodeInPlace:
        return L10n.tr("qr.code.in.place")
      case .QrCodeReader:
        return L10n.tr("qr.code.reader")
      case .RatingMessage:
        return L10n.tr("rating.message")
      case .RatingNow:
        return L10n.tr("rating.now")
      case .RatingTitle:
        return L10n.tr("rating.title")
      case .RedeemCompleteTitle:
        return L10n.tr("redeem.complete.title")
      case .RedeemConfirmation:
        return L10n.tr("redeem.confirmation")
      case .RegisteredSuccessful:
        return L10n.tr("registered.successful")
      case .RegistrationLogin:
        return L10n.tr("registration.login")
      case .RegistrationNotPossible:
        return L10n.tr("registration.not.possible")
      case .RegistrationPhoneMessage:
        return L10n.tr("registration.phone.message")
      case .RegistrationPrivacyLicense:
        return L10n.tr("registration.privacy.license")
      case .RegistrationPrivacySocial:
        return L10n.tr("registration.privacy.social")
      case .RemindLater:
        return L10n.tr("remind.later")
      case .RequestTimeout:
        return L10n.tr("request.timeout")
      case .RetryConfirmCode:
        return L10n.tr("retry.confirm.code")
      case .RoleCashier:
        return L10n.tr("role.cashier")
      case .RoleClient:
        return L10n.tr("role.client")
      case .RollbackSuccessful:
        return L10n.tr("rollback.successful")
      case .ScanQrCode:
        return L10n.tr("scan.qr.code")
      case .SelectNetwork:
        return L10n.tr("select.network")
      case .SentMail:
        return L10n.tr("sent.mail")
      case .ServerErrorOccurred:
        return L10n.tr("server.error.occurred")
      case .SessionIsOver:
        return L10n.tr("session.is.over")
      case .SetUpEmailClientAndRepeat:
        return L10n.tr("set.up.email.client.and.repeat")
      case .ShareError:
        return L10n.tr("share.error")
      case .ShareLinkSent:
        return L10n.tr("share.link.sent")
      case .SharePlaceCode:
        return L10n.tr("share.place.code")
      case .ShareTwitError:
        return L10n.tr("share.twit.error")
      case .ShowQrCode:
        return L10n.tr("show.qr.code")
      case .ShowQrCodeInOrderToInvite1:
        return L10n.tr("show.qr.code.in.order.to.invite.1")
      case .ShowQrCodeInOrderToInvite2:
        return L10n.tr("show.qr.code.in.order.to.invite.2")
      case .ShowQrCodeLabel1:
        return L10n.tr("show.qr.code.label.1")
      case .ShowQrCodeLabel2:
        return L10n.tr("show.qr.code.label.2")
      case .ShowQrCodeToFriends:
        return L10n.tr("show.qr.code.to.friends")
      case .SignIn:
        return L10n.tr("sign.in")
      case .SignUp:
        return L10n.tr("sign.up")
      case .SocialSettingsFb:
        return L10n.tr("social.settings.fb")
      case .SocialSettingsGp:
        return L10n.tr("social.settings.gp")
      case .SocialSettingsOk:
        return L10n.tr("social.settings.ok")
      case .SocialSettingsTw:
        return L10n.tr("social.settings.tw")
      case .SocialSettingsVk:
        return L10n.tr("social.settings.vk")
      case .SocketClosed:
        return L10n.tr("socket.closed")
      case .StatDialogTitle:
        return L10n.tr("stat.dialog.title")
      case .SuccessfulSettings:
        return L10n.tr("successful.settings")
      case .SupportEmail:
        return L10n.tr("support.email")
      case .TapForContinueScanning:
        return L10n.tr("tap.for.continue.scanning")
      case .TaskCanceled:
        return L10n.tr("task.canceled")
      case .TaskComplete:
        return L10n.tr("task.complete")
      case .TaskConfirmed:
        return L10n.tr("task.confirmed")
      case .TaskExpired:
        return L10n.tr("task.expired")
      case .TaskListEmptyMessage:
        return L10n.tr("task.list.empty.message")
      case .TaskListEmptyTitle:
        return L10n.tr("task.list.empty.title")
      case .ThereNoUserWithSuchEmail:
        return L10n.tr("there.no.user.with.such.email")
      case .TitleActivityCashierComplete:
        return L10n.tr("title.activity.cashier.complete")
      case .TitleActivityCashierPaymentInfo:
        return L10n.tr("title.activity.cashier.payment.info")
      case .TitleActivityEmailLogin:
        return L10n.tr("title.activity.email.login")
      case .TitleActivityEmailSignIn:
        return L10n.tr("title.activity.email.sign.in")
      case .TitleActivityGallery:
        return L10n.tr("title.activity.gallery")
      case .TitleActivityLogin:
        return L10n.tr("title.activity.login")
      case .TitleActivityNewPromotion:
        return L10n.tr("title.activity.new.promotion")
      case .TitleActivityPlaceDetails:
        return L10n.tr("title.activity.place.details")
      case .TitleActivityPlacesQr:
        return L10n.tr("title.activity.places.qr")
      case .TitleActivityPromotionDetails:
        return L10n.tr("title.activity.promotion.details")
      case .TitleActivityQrCode:
        return L10n.tr("title.activity.qr.code")
      case .TitleActivityQrScanner:
        return L10n.tr("title.activity.qr.scanner")
      case .TitleActivitySetting:
        return L10n.tr("title.activity.setting")
      case .TitleActivityShareLink:
        return L10n.tr("title.activity.share.link")
      case .TitleFaq:
        return L10n.tr("title.faq")
      case .TitlePrivacy:
        return L10n.tr("title.privacy")
      case .TitleSettingsLanguage:
        return L10n.tr("title.settings.language")
      case .TitleSettingsNotification:
        return L10n.tr("title.settings.notification")
      case .TitleSettingsSocialNetwork:
        return L10n.tr("title.settings.social.network")
      case .TitleSocialShareLink:
        return L10n.tr("title.social.share.link")
      case .TitleSupport:
        return L10n.tr("title.support")
      case .TitleTerms:
        return L10n.tr("title.terms")
      case .TitleTransactions:
        return L10n.tr("title.transactions")
      case .TitleTransactionsHistory:
        return L10n.tr("title.transactions.history")
      case .TitleTransactionsTabHistory:
        return L10n.tr("title.transactions.tab.history")
      case .ToReenablePermission:
        return L10n.tr("to.reenable.permission")
      case .TransactionCancelled:
        return L10n.tr("transaction.cancelled")
      case .TransactionComplete:
        return L10n.tr("transaction.complete")
      case .TransactionError:
        return L10n.tr("transaction.error")
      case .TransactionNotApply:
        return L10n.tr("transaction.not.apply")
      case .TransactionRate1:
        return L10n.tr("transaction.rate.1")
      case .TransactionRate2:
        return L10n.tr("transaction.rate.2")
      case .TransactionRate3:
        return L10n.tr("transaction.rate.3")
      case .TransactionRate4:
        return L10n.tr("transaction.rate.4")
      case .TransactionRate5:
        return L10n.tr("transaction.rate.5")
      case .TransactionRateDone:
        return L10n.tr("transaction.rate.done")
      case .TransactionRateMessage:
        return L10n.tr("transaction.rate.message")
      case .TransactionStatusApproved:
        return L10n.tr("transaction.status.approved")
      case .TransactionStatusProcessing:
        return L10n.tr("transaction.status.processing")
      case .TransactionStatusRejected:
        return L10n.tr("transaction.status.rejected")
      case .TransactionsFriendsHistory:
        return L10n.tr("transactions.friends.history")
      case .TransactionsIncome:
        return L10n.tr("transactions.income")
      case .TransactionsIncomeCashier:
        return L10n.tr("transactions.income.cashier")
      case .TransactionsListEmptyMessage:
        return L10n.tr("transactions.list.empty.message")
      case .TransactionsMoneyCashier:
        return L10n.tr("transactions.money.cashier")
      case .TransactionsNotFound:
        return L10n.tr("transactions.not.found")
      case .TransactionsOutcome:
        return L10n.tr("transactions.outcome")
      case .TransactionsOutcomeCashier:
        return L10n.tr("transactions.outcome.cashier")
      case .TransactionsPointsCashier:
        return L10n.tr("transactions.points.cashier")
      case .TransactionsRedeemPoints:
        return L10n.tr("transactions.redeem.points")
      case .TryAgain:
        return L10n.tr("try.again")
      case .TryAgainLater:
        return L10n.tr("try.again.later")
      case .Tut11:
        return L10n.tr("tut.1.1")
      case .Tut12:
        return L10n.tr("tut.1.2")
      case .Tut13:
        return L10n.tr("tut.1.3")
      case .Tut14:
        return L10n.tr("tut.1.4")
      case .Tut21:
        return L10n.tr("tut.2.1")
      case .Tut22:
        return L10n.tr("tut.2.2")
      case .Tut31:
        return L10n.tr("tut.3.1")
      case .Tut32:
        return L10n.tr("tut.3.2")
      case .Tut33:
        return L10n.tr("tut.3.3")
      case .Tut34:
        return L10n.tr("tut.3.4")
      case .Tut35:
        return L10n.tr("tut.3.5")
      case .Tut36:
        return L10n.tr("tut.3.6")
      case .Tut41:
        return L10n.tr("tut.4.1")
      case .Tut42:
        return L10n.tr("tut.4.2")
      case .Tut43:
        return L10n.tr("tut.4.3")
      case .Tut44:
        return L10n.tr("tut.4.4")
      case .TutorialBtnPrev:
        return L10n.tr("tutorial.btn.prev")
      case .TutorialTitle:
        return L10n.tr("tutorial.title")
      case .UnknownError:
        return L10n.tr("unknown.error")
      case .UrlPrivacy:
        return L10n.tr("url_privacy")
      case .UrlTerms:
        return L10n.tr("url_terms")
      case .UsePoints:
        return L10n.tr("use.points")
      case .UserAgreements:
        return L10n.tr("user.agreements")
      case .UserNotRegistered:
        return L10n.tr("user.not.registered")
      case .VersionMessage:
        return L10n.tr("version.message")
      case .WantToCancelRegistrationText:
        return L10n.tr("want.to.cancel.registration.text")
      case .WelcomeButtonText:
        return L10n.tr("welcome.button.text")
      case .WelcomeSubjectTitleText:
        return L10n.tr("welcome.subject.title.text")
      case .WelcomeSubtitleText:
        return L10n.tr("welcome.subtitle.text")
      case .WelcomeTitleText:
        return L10n.tr("welcome.title.text")
      case .WouldLikeToInviteYou:
        return L10n.tr("would.like.to.invite.you")
      case .WrongCode:
        return L10n.tr("wrong.code")
      case .YouDontHaveMyPlaces:
        return L10n.tr("you.dont.have.my.places")
      case .YouMustSetupEmailClient:
        return L10n.tr("you.must.setup.email.client")
    }
  }

  private static func tr(_ key: String, _ args: CVarArg...) -> String {
    let format = key.localized
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

func tr(_ key: L10n) -> String {
  return key.string
}
