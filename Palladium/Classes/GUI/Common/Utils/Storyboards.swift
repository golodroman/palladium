// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation
import UIKit

// swiftlint:disable file_length
// swiftlint:disable line_length
// swiftlint:disable type_body_length

protocol StoryboardSceneType {
  static var storyboardName: String { get }
}

extension StoryboardSceneType {
  static func storyboard() -> UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }

  static func initialViewController() -> UIViewController {
    guard let vc = storyboard().instantiateInitialViewController() else {
      fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
    }
    return vc
  }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
  func viewController() -> UIViewController {
    return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
  }
  static func viewController(identifier: Self) -> UIViewController {
    return identifier.viewController()
  }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: StoryboardSegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

enum StoryboardScene {
  enum Authorization: String, StoryboardSceneType {
    static let storyboardName = "Authorization"

    case loginScene = "login"
    static func instantiateLogin() -> Palladium.LoginViewController {
      guard let vc = StoryboardScene.Authorization.loginScene.viewController() as? Palladium.LoginViewController
      else {
        fatalError("ViewController 'login' is not of the expected class Palladium.LoginViewController.")
      }
      return vc
    }

    case smsScene = "sms"
    static func instantiateSms() -> Palladium.SMSViewController {
      guard let vc = StoryboardScene.Authorization.smsScene.viewController() as? Palladium.SMSViewController
      else {
        fatalError("ViewController 'sms' is not of the expected class Palladium.SMSViewController.")
      }
      return vc
    }

    case welcomeScene = "welcome"
    static func instantiateWelcome() -> UINavigationController {
      guard let vc = StoryboardScene.Authorization.welcomeScene.viewController() as? UINavigationController
      else {
        fatalError("ViewController 'welcome' is not of the expected class UINavigationController.")
      }
      return vc
    }
  }
  enum Common: String, StoryboardSceneType {
    static let storyboardName = "Common"

    case webViewScene = "webView"
    static func instantiateWebView() -> UINavigationController {
      guard let vc = StoryboardScene.Common.webViewScene.viewController() as? UINavigationController
      else {
        fatalError("ViewController 'webView' is not of the expected class UINavigationController.")
      }
      return vc
    }
  }
  enum History: StoryboardSceneType {
    static let storyboardName = "History"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }
  }
  enum Main: String, StoryboardSceneType {
    static let storyboardName = "Main"

    static func initialViewController() -> Palladium.MainController {
      guard let vc = storyboard().instantiateInitialViewController() as? Palladium.MainController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case menuScene = "menu"
    static func instantiateMenu() -> Palladium.MenuViewController {
      guard let vc = StoryboardScene.Main.menuScene.viewController() as? Palladium.MenuViewController
      else {
        fatalError("ViewController 'menu' is not of the expected class Palladium.MenuViewController.")
      }
      return vc
    }
  }
  enum Payment: String, StoryboardSceneType {
    static let storyboardName = "Payment"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case paymentDetailsScene = "payment.details"
    static func instantiatePaymentDetails() -> Palladium.PaymentDetailsViewController {
      guard let vc = StoryboardScene.Payment.paymentDetailsScene.viewController() as? Palladium.PaymentDetailsViewController
      else {
        fatalError("ViewController 'payment.details' is not of the expected class Palladium.PaymentDetailsViewController.")
      }
      return vc
    }

    case paymentFailedScene = "payment.failed"
    static func instantiatePaymentFailed() -> Palladium.PaymentFailedViewController {
      guard let vc = StoryboardScene.Payment.paymentFailedScene.viewController() as? Palladium.PaymentFailedViewController
      else {
        fatalError("ViewController 'payment.failed' is not of the expected class Palladium.PaymentFailedViewController.")
      }
      return vc
    }

    case paymentMethodScene = "payment.method"
    static func instantiatePaymentMethod() -> Palladium.PaymentMethodViewController {
      guard let vc = StoryboardScene.Payment.paymentMethodScene.viewController() as? Palladium.PaymentMethodViewController
      else {
        fatalError("ViewController 'payment.method' is not of the expected class Palladium.PaymentMethodViewController.")
      }
      return vc
    }

    case paymentSuccessScene = "payment.success"
    static func instantiatePaymentSuccess() -> Palladium.PaymentSuccessViewController {
      guard let vc = StoryboardScene.Payment.paymentSuccessScene.viewController() as? Palladium.PaymentSuccessViewController
      else {
        fatalError("ViewController 'payment.success' is not of the expected class Palladium.PaymentSuccessViewController.")
      }
      return vc
    }
  }
  enum Places: String, StoryboardSceneType {
    static let storyboardName = "Places"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }

    case listScene = "list"
    static func instantiateList() -> Palladium.PlacesListViewController {
      guard let vc = StoryboardScene.Places.listScene.viewController() as? Palladium.PlacesListViewController
      else {
        fatalError("ViewController 'list' is not of the expected class Palladium.PlacesListViewController.")
      }
      return vc
    }

    case placeScene = "place"
    static func instantiatePlace() -> Palladium.PlaceViewController {
      guard let vc = StoryboardScene.Places.placeScene.viewController() as? Palladium.PlaceViewController
      else {
        fatalError("ViewController 'place' is not of the expected class Palladium.PlaceViewController.")
      }
      return vc
    }

    case placeAboutScene = "place.about"
    static func instantiatePlaceAbout() -> Palladium.PlaceAboutViewController {
      guard let vc = StoryboardScene.Places.placeAboutScene.viewController() as? Palladium.PlaceAboutViewController
      else {
        fatalError("ViewController 'place.about' is not of the expected class Palladium.PlaceAboutViewController.")
      }
      return vc
    }

    case placeGalleryScene = "place.gallery"
    static func instantiatePlaceGallery() -> Palladium.PlaceGalleryViewController {
      guard let vc = StoryboardScene.Places.placeGalleryScene.viewController() as? Palladium.PlaceGalleryViewController
      else {
        fatalError("ViewController 'place.gallery' is not of the expected class Palladium.PlaceGalleryViewController.")
      }
      return vc
    }

    case placeMenuScene = "place.menu"
    static func instantiatePlaceMenu() -> Palladium.PlaceMenuViewController {
      guard let vc = StoryboardScene.Places.placeMenuScene.viewController() as? Palladium.PlaceMenuViewController
      else {
        fatalError("ViewController 'place.menu' is not of the expected class Palladium.PlaceMenuViewController.")
      }
      return vc
    }

    case placeReviewsScene = "place.reviews"
    static func instantiatePlaceReviews() -> Palladium.PlaceReviewsViewController {
      guard let vc = StoryboardScene.Places.placeReviewsScene.viewController() as? Palladium.PlaceReviewsViewController
      else {
        fatalError("ViewController 'place.reviews' is not of the expected class Palladium.PlaceReviewsViewController.")
      }
      return vc
    }
  }
  enum Profile: StoryboardSceneType {
    static let storyboardName = "Profile"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }
  }
  enum Promo: StoryboardSceneType {
    static let storyboardName = "Promo"

    static func initialViewController() -> UINavigationController {
      guard let vc = storyboard().instantiateInitialViewController() as? UINavigationController else {
        fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
      }
      return vc
    }
  }
}

enum StoryboardSegue {
  enum Authorization: String, StoryboardSegueType {
    case license
  }
}

private final class BundleToken {}
