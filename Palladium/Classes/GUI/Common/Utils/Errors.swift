// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import DCFoundation

enum L10nError {
  /// Please make a payment in at least one place of a chain to invite friends to that chain
  case ChainText
  /// Error: empty code
  case EmptyCode
  /// Found an error
  case Founded
  /// Invalid password
  case InvPass
  /// You are not registered in this place to invite other users
  case PlaceText
}

extension L10nError: CustomStringConvertible {
  var description: String { return self.string }

  var string: String {
    switch self {
      case .ChainText:
        return L10nError.tr("error.chain.text")
      case .EmptyCode:
        return L10nError.tr("error.empty.code")
      case .Founded:
        return L10nError.tr("error.founded")
      case .InvPass:
        return L10nError.tr("error.inv.pass")
      case .PlaceText:
        return L10nError.tr("error.place.text")
    }
  }
  var key: String {
    switch self {
      case .ChainText:
        return "error.chain.text"
      case .EmptyCode:
        return "error.empty.code"
      case .Founded:
        return "error.founded"
      case .InvPass:
        return "error.inv.pass"
      case .PlaceText:
        return "error.place.text"
	  default: return ""
    }
  }
  func error() -> NSError {
  	return NSError(localized: self)!
  }
  func error(userInfo: [String:AnyObject]?) -> NSError {
  	return NSError(localized: self, code: nil, domain: nil, userInfo:userInfo)!
  }

  private static func tr(_ key: String, _ args: CVarArg...) -> String {
    let format = key.localized
    return String(format: format, locale: Locale.current, arguments: args)
  }
}


extension NSError {
    convenience init?(localized: L10nError, code: Int? = nil, domain: String? = nil, userInfo aUserInfo:[String:AnyObject]? = nil) {
		var code = code
		var domain = domain
        if domain == nil {
            if let identifier = (Bundle.main.infoDictionary?[kCFBundleIdentifierKey as String] as? String) {
                domain = identifier + ".error"
            } else {
                domain = "com.app.error"
            }
        }
        if code == nil {
            code = -1
        }
		var userInfo = [String:AnyObject]()
		userInfo[NSLocalizedDescriptionKey] = localized.string as AnyObject
		if let aUserInfo = aUserInfo {
			userInfo << aUserInfo
		}
        self.init(domain: domain!, code: -1, userInfo: userInfo)
    }
}
