//
//  Extra.h
//  Palladium
//
//  Created by Igor Danich on 8/17/17.
//  Copyright © 2017 r-style. All rights reserved.
//

#ifndef Extra_h
#define Extra_h

@interface NavigationProperties(Extra)
@property(nonatomic,strong) IBInspectable UIColor* barTintColor;
@property(nonatomic,strong) IBInspectable NSString* backTitle;
@property(nonatomic,strong) IBInspectable UIFont* backFont;
@property(nonatomic,strong) IBInspectable UIImage* backImage;
@property(nonatomic,strong) IBInspectable BOOL barHidden;
@property(nonatomic,strong) IBInspectable BOOL barCleared;
@end

@interface UINavigationController(Extra)
@property (nonatomic, strong) IBOutlet Navigation * _Nullable navigation;
@end

@interface UIViewController (Extra)
@property (nonatomic, strong) IBOutlet NavigationProperties * _Nullable navigationProperties;
@end

#endif /* Extra_h */
