//
//  UIUtils.swift
//  Addzer
//
//  Created by Igor Danich on 4/11/17.
//  Copyright © 2017 r-style. All rights reserved.
//
import DCUI
import Swizzlean

extension NSObject {
    
    func testForObjCLinkerFlag() -> Bool {
        return true
    }
    
}

func PrepareCardIO() {
    CardIOUtilities.preloadCardIO()
}

func InitializeCaches() {
    Cache.initialize("Images", options: CacheOptions(converter: CacheImageConverter(type: .JPEG)))
}

func ImageCache() -> Cache {
    return Cache.named("Images")
}

@IBDesignable
class View: UIView {}

@IBDesignable
class TextField: UITextField {}

@IBDesignable
class EButton: DCUI.Button {}

extension UIViewController {
    
    func makeBackButton(light: Bool = false) {
        navigationItem.leftBarButtonItem = UIBarButtonItem(text: " ", image: UIImage(asset: light ? .Button_Back_Light : .Button_Back_Dark)) { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
}

extension UIImage {
    
    static func orangeGradient(size: CGSize) -> UIImage? {
        return UIImage.draw(size: size) { (size, _) in
            let gradient = Gradient(colors: [
                UIColor(red:0.87, green:0.78, blue:0.55, alpha:1),
                UIColor(red:0.82, green:0.69, blue:0.45, alpha:1)
                ], locations: [0, 1], type: .linear)
            gradient.startPoint = CGPoint(x: 0, y: size.height/2)
            gradient.endPoint = CGPoint(x: size.width, y: size.height/2)
            gradient.draw()
        }
    }
    
}

extension UIStackView {
    
    func clear() {
        let views = arrangedSubviews
        for view in views {
            removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
    
}

extension UIView {
    
    static func fromXIB<T:UIView>(name: String? = nil) -> T {
        let name = name ?? NSStringFromClass(T.classForCoder()).components(separatedBy: CharacterSet(charactersIn: ".")).last!
        let views: [T] = (Bundle.main.loadNibNamed(name, owner: nil, options: nil) ?? []).makeArray { (_, view) in
            return view as? T
        }
        return views.first ?? T()
    }
    
}

extension UIImage {
    
    convenience init(cardType: PaymentCardType) {
        switch cardType {
        case .visa:         self.init(asset: .Payment_Type_Visa)
        case .mastercard:   self.init(asset: .Payment_Type_Master_Card)
        default:            self.init(asset: .Profile_Card_New)
        }
    }
    
}

public func ShowAlert(text: String?, inViewController: UIViewController? = nil) {
    var ctrl = inViewController
    if ctrl == nil {
        var window: UIWindow?
        for item in UIApplication.shared.windows {
            if  !NSStringFromClass(item.classForCoder).contains("UIRemoteKeyboardWindow") &&
                !NSStringFromClass(item.classForCoder).contains("_UIInteractiveHighlightEffectWindow") {
                window = item
            }
        }
        ctrl = window?.rootViewController
    }
    if let ctrl = ctrl {
        let alert = UIAlertController(title: nil, message: text?.localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "general.button.ok".localized, style: .cancel, handler: { _ in}))
        ctrl.present(alert, animated: true, completion: nil)
    }
    
}

public func ShowAlert(text: String?, cancel: String? = nil, style: UIAlertControllerStyle, actions: [UIAlertAction], inViewController: UIViewController? = nil) {
    var ctrl = inViewController
    if ctrl == nil {
        var window: UIWindow?
        for item in UIApplication.shared.windows {
            if !NSStringFromClass(item.classForCoder).contains("UIRemoteKeyboardWindow") {
                window = item
            }
        }
        ctrl = window?.rootViewController
    }
    if let ctrl = ctrl {
        let alert = UIAlertController(title: nil, message: text?.localized, preferredStyle: style)
        var hasCancel = false
        for item in actions {
            if item.style == .cancel {
                hasCancel = true
            }
            alert.addAction(item)
        }
        if !hasCancel {
            if let cancel = cancel {
                alert.addAction(UIAlertAction(title: cancel.localized, style: .cancel, handler: { _ in}))
            }
        }
        ctrl.present(alert, animated: true, completion: nil)
    }
    
}

public func ShowAlert(error: Error?, inViewController: UIViewController? = nil) {
    if let error = error {
        ShowAlert(text: error.localizedDescription, inViewController: inViewController)
    }
}
