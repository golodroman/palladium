// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

import UIKit

extension UIImage {
  enum Asset: String {
    case Button_Back_Dark = "button-back-dark"
    case Button_Back_Light = "button-back-light"
    case Button_Bottom = "button-bottom"
    case Dropdown_Arrow = "dropdown-arrow"
    case Empty_History = "empty-history"
    case Empty_Promo = "empty-promo"
    case Icon_Call = "icon-call"
    case Icon_Map_Selected = "icon-map-selected"
    case Icon_Place = "icon-place"
    case Line_Wave = "line-wave"
    case Map_Pin = "map-pin"
    case Menu_History = "menu-history"
    case Menu_History_Selected = "menu-history-selected"
    case Menu_Offers = "menu-offers"
    case Menu_Offers_Selected = "menu-offers-selected"
    case Menu_Payment = "menu-payment"
    case Menu_Payment_Selected = "menu-payment-selected"
    case Menu_Places = "menu-places"
    case Menu_Places_Selected = "menu-places-selected"
    case Menu_Profile = "menu-profile"
    case Menu_Profile_Selected = "menu-profile-selected"
    case Palladium_Logo = "palladium-logo"
    case Palladium_Placeholder = "palladium-placeholder"
    case Payment_Failed_Logo = "payment-failed-logo"
    case Payment_Failed_Warning = "payment-failed-warning"
    case Payment_Method_Logo = "payment-method-logo"
    case Payment_Success_Logo = "payment-success-logo"
    case Payment_Type_Master_Card = "payment-type-master-card"
    case Payment_Type_Visa = "payment-type-visa"
    case Places_List = "places-list"
    case Places_Map = "places-map"
    case Profile_Card_Camera = "profile-card-camera"
    case Profile_Card_New = "profile-card-new"
    case Profile_Card_Remove = "profile-card-remove"
    case Social_Facebook = "social-facebook"
    case Social_Instagram = "social-instagram"
    case Tutorial_0 = "tutorial-0"
    case Tutorial_1 = "tutorial-1"
    case Tutorial_2 = "tutorial-2"
    case Welcome_Background = "welcome-background"
    case Welcome_Logo = "welcome-logo"

    var image: UIImage {
      return UIImage(asset: self)
    }
  }

  convenience init!(asset: Asset) {
    self.init(named: asset.rawValue)
  }
}
