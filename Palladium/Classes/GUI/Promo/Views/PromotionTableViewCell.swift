//
//  OfferTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 8/24/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PromotionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mediaView: MediaView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var lineHeight: NSLayoutConstraint!
    @IBOutlet weak var detailTxtView: UITextView!
    @IBOutlet weak var arrowView: UIView!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var actionBtn: UIButton!
    
    static func height(for offer: Promotion) -> CGFloat {
        var height: CGFloat = 71
        if let size = (ImageCache()[offer.img?.large ?? offer.img?.thumb] as? UIImage)?.size {
            if size.width < UIScreen.main.bounds.size.width {
                height += size.height*UIScreen.main.bounds.size.width/size.width
            } else {
                height += size.height*size.width/UIScreen.main.bounds.size.width
            }
        } else {
            height += 180
        }
        return height
    }
    
    var expandedHeight: CGFloat {
        return imageHeight.constant + 4.0 + titleLbl.height + 15.0 + detailTxtView.contentSize.height + 11.0
    }
    
    var isExpanded: Bool = false {
        didSet {
            lineHeight.constant = isExpanded ? 4 : 0
            arrowView.transform = isExpanded ? CGAffineTransform(rotationAngle: CGFloat.pi) : CGAffineTransform.identity
        }
    }
    
    var onUpdate: ((CGFloat) -> Void)?
    
    fileprivate func updateImageHeight(size: CGSize) {
        if size.width < width {
            imageHeight.constant = size.height*width/size.width
        } else {
            imageHeight.constant = size.height*size.width/width
        }
    }

    func adjustTo(offer: Promotion, index: Int) {
        titleLbl.text = offer.name
        actionBtn.tag = index
        detailTxtView.text = offer.descFull
        imageHeight.constant = 180
        mediaView.url = offer.img?.large ?? offer.img?.thumb
        if let size = (ImageCache()[mediaView.url] as? UIImage)?.size {
            updateImageHeight(size: size)
        } else {
            mediaView.onImageUpdated = { [weak self]image in
                if let size = image?.size {
                    self?.updateImageHeight(size: size)
                }
                if let height = self?.expandedHeight {
                    self?.onUpdate?(height)
                }
            }
        }
    }

}
