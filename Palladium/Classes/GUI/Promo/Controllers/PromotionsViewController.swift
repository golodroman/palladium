//
//  OffersViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/23/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class PromotionsViewController: UIViewController {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate let refresh = UIRefreshControl()
    fileprivate var hideKeyboardEnabled = true
    fileprivate var selectedIPs = [IndexPath]()
    fileprivate var expandedHeights = [Int:CGFloat]()

    
    fileprivate let service = PromotionsService()
    
    deinit {
        NotificationRemove(observer: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.tabBarItem.title = L10n.PromotionsTitle.string
        navigationController?.tabBarItem.image = UIImage(asset: .Menu_Offers)
        navigationController?.tabBarItem.selectedImage = UIImage(asset: .Menu_Offers_Selected)
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.width, height: 20))
        reload(clear: true)
        refresh.addTarget(self, action: #selector(PromotionsViewController.onRefresh), for: .valueChanged)
        tableView.addSubview(refresh)
    }
    
    @objc func onRefresh() {
        reload(clear: true)
    }
    
    fileprivate func reload(clear: Bool) {
        refresh.endRefreshing()
        if clear {
            selectedIPs = []
            expandedHeights = [:]
            tableView.alpha = 0
            indicator.startAnimating()
        }
        tableView.backgroundView = nil
        service.fetch(clear: clear) { [weak self]error in
            self?.indicator.stopAnimating()
            self?.tableView.reloadData()
            UIView.animate(withDuration: 0.2) {
                self?.tableView.alpha = 1
            }
        }
    }
    
    @IBAction func onToggle(btn: UIButton) {
        let ip = IndexPath(row: btn.tag, section: 0)
        if let index = selectedIPs.index(of: ip) {
            selectedIPs.remove(at: index)
        } else {
            selectedIPs << ip
        }
        tableView.reloadRows(at: [ip], with: .automatic)
        Timer.scheduled(0.25) { [weak self]_ in
            self?.tableView.scrollToRow(at: ip, at: .middle, animated: true)
        }
    }
    
}

// MARK - UITableViewDelegate, UITableViewDataSource

extension PromotionsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return service.hasPromotionsAvailable ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:     return service.promotions.items.count == 0 ? 1 : service.promotions.items.count
        case 1:     return 1
        default:    return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath:  IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if service.promotions.items.count == 0 {
                return tableView.dequeueReusableCell(identifier: "empty", indexPath: indexPath)
            } else {
                let cell: PromotionTableViewCell = tableView.dequeueReusableCell(identifier: "cell", indexPath: indexPath)
                cell.onUpdate = { [weak self]height in
                    self?.expandedHeights[indexPath.row] = height
                    self?.tableView.reloadRows(at: [indexPath], with: .automatic)
                }
                cell.adjustTo(offer: service.promotions.items[indexPath.row], index: indexPath.row)
                if expandedHeights[indexPath.row] == nil {
                    Timer.scheduled(0.05) { _ in
                        self.expandedHeights[indexPath.row] = cell.expandedHeight
                    }
                }
                cell.isExpanded = selectedIPs.contains(indexPath)
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "load")!
            (cell.view(identifier: "indicator") as! UIActivityIndicatorView).startAnimating()
            return cell
        default: return UITableViewCell()
        }
        
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if service.promotions.items.count == 0 {
                return 300
            } else {
                let height = PromotionTableViewCell.height(for: service.promotions.items[indexPath.row])
                return selectedIPs.contains(indexPath) ? (expandedHeights[indexPath.row] ?? height) : height
            }
        case 1:     return 57
        default:    return 0
        }
    }
    
}
