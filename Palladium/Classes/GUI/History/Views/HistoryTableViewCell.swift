//
//  HistoryTableViewCell.swift
//  Palladium
//
//  Created by Igor Danich on 8/23/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var mediaView: MediaView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var changeLbl: UILabel!
    
    fileprivate let changeHeight: CGFloat = 16
    
    func adjustTo(transaction: Transaction) {
        dateLbl.text = transaction.date.string(format: "yyyy-MM-dd HH:mm", timeZone: TimeZone(identifier: "Asia/Almaty"))
        mediaView.url = transaction.place.logo
        titleLbl.text = transaction.place.name?.uppercased()
        amountLbl.text = transaction.after.priceString()
        changeLbl.text = (transaction.change >= 0 ? "+" : "") + transaction.change.priceString()
    }

}
