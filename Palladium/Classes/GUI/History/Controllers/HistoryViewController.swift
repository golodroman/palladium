//
//  HistoryViewController.swift
//  Palladium
//
//  Created by Igor Danich on 8/23/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCUI

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    fileprivate let service = HistoryService()
    
    fileprivate let refresh = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.tabBarItem.title = L10n.TitleTransactionsTabHistory.string
        navigationController?.tabBarItem.image = UIImage(asset: .Menu_History)
        navigationController?.tabBarItem.selectedImage = UIImage(asset: .Menu_History_Selected)
        title = L10n.TitleTransactionsHistory.string
        reload(clear: true)
        refresh.addTarget(self, action: #selector(HistoryViewController.onRefresh), for: .valueChanged)
        tableView.addSubview(refresh)
    }
    
    @objc func onRefresh() {
        reload(clear: true)
    }
    
    fileprivate func reload(clear: Bool) {
        refresh.endRefreshing()
        if clear {
            tableView.alpha = 0
            indicator.startAnimating()
        }
        service.fetch(clear: clear) { [weak self]error in
            ShowAlert(error: error)
            self?.indicator.stopAnimating()
            self?.tableView.reloadData()
            UIView.animate(withDuration: 0.2) {
                self?.tableView.alpha = 1
            }
        }
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return service.canLoadMore ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:     return service.transactions.items.count == 0 ? 1 : service.transactions.items.count
        case 1:     return 1
        default:    return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath:  IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if service.transactions.items.count == 0 {
                return tableView.dequeueReusableCell(withIdentifier: "empty", for: indexPath)
            } else {
                let cell: HistoryTableViewCell = tableView.dequeueReusableCell(identifier: "cell", indexPath: indexPath)
                cell.adjustTo(transaction: service.transactions.items[indexPath.row])
                return cell
            }
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "load")!
            (cell.view(identifier: "indicator") as! UIActivityIndicatorView).startAnimating()
            return cell
        default: return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.reuseIdentifier == "load" {
            reload(clear: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:     return service.transactions.items.count == 0 ? tableView.height : 70
        case 1:     return 30
        default:    return 0
        }
    }

}
