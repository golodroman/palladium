//
//  AppDelegate.swift
//  Addzer
//
//  Created by Eugene Donov on 10/26/16.
//  Copyright © 2016 r-style. All rights reserved.
//

import DCUI
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    class var rootCtrl: MainController {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.window!.rootViewController as! MainController
    }
    
    var window: UIWindow?
    
    var restrictRotation = true
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        Localization.setCurrent(localization: Localization.localizationWithIdentifier("ru"))
        
        GMSServices.provideAPIKey("AIzaSyDnzf2dDt_9opmUVCq4YRSaYFJjfqtZCGg")
        
        InitializeCaches()
        PrepareCardIO()
        LocationService.shared.updateLocationIfNeeded(nil)
        
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil))
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if restrictRotation {
            return .portrait
        } else {
            return .allButUpsideDown
        }
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        UserService.shared.deviceId = deviceToken.pushToken
    }
    
}
