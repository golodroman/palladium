//
//  PlaceImage.swift
//  Addzer
//
//  Created by Eugene Donov on 2/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct PlaceImage: Unboxable {
    
    let id          : String?
    let file        : String?
    let fileThumb   : String?
    
    init(unboxer: Unboxer) throws {
        id          = unboxer.unbox(key: "id")
        file        = unboxer.unbox(key: "file")
        fileThumb   = unboxer.unbox(key: "file_thumb")
    }
        
}
