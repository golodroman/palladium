//
//  PaymentQuoteType.swift
//  Palladium
//
//  Created by Igor Danich on 10/5/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

struct PaymentQuoteType: Unboxable {
    
    let balanceEarnings : Int
    let amount          : Int
    
    init() {
        balanceEarnings = 10000
        amount = 1000000
    }
    
    init(unboxer: Unboxer) throws {
        balanceEarnings = try unboxer.unbox(key: "balance_earnings")
        amount          = try unboxer.unbox(key: "amount")
    }
    
}
