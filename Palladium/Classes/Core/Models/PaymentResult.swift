//
//  PaymentResult.swift
//  Palladium
//
//  Created by Igor Danich on 10/5/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

struct PaymentResult: Unboxable {
    
    let id              : Int
    let placeId         : Int
    let balanceEarnings : Int
    
    init(unboxer: Unboxer) throws {
        id              = try unboxer.unbox(keyPath: "id")
        placeId         = try unboxer.unbox(keyPath: "place_id")
        balanceEarnings = try unboxer.unbox(keyPath: "balance_earnings")
    }
    
}
