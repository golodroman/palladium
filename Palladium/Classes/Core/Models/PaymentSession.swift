//
//  PaymentSession.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

struct PaymentSession: Unboxable {
    
    let dishes  : [PaymentDish]?
    let pay     : PaymentInfo?
    
    init(unboxer: Unboxer) throws {
        dishes  = unboxer.unbox(key: "Dish")
        pay     = unboxer.unbox(key: "Pay")
    }
    
}
