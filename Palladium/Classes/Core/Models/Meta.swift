//
//  Meta.swift
//  Addzer
//
//  Created by Eugene Donov on 2/7/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct Meta: Unboxable {
    
    var totalCount  : Int
    var pageCount   : Int
    var currentPage : Int
    var perPage     : Int
    
    var hasPagesAvailable: Bool {
        return currentPage < pageCount
    }
    
    init(unboxer: Unboxer) throws {
        totalCount  = try unboxer.unbox(key: "totalCount")
        pageCount   = try unboxer.unbox(key: "pageCount")
        currentPage = try unboxer.unbox(key: "currentPage")
        perPage     = try unboxer.unbox(key: "perPage")
    }
    
    init(totalCount: Int, pageCount: Int, currentPage: Int, perPage: Int) {
        self.totalCount = totalCount
        self.pageCount = pageCount
        self.currentPage = currentPage
        self.perPage = perPage
    }
    
}
