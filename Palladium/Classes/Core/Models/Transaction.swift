//
//  Transaction.swift
//  Palladium
//
//  Created by Igor Danich on 9/10/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct Transaction: Unboxable {
    
    let change          : Double
    let after           : Double
    let before          : Double
    let date            : Date
    let type            : Int
    let place           : Place
    let points          : Int?
    let lastVisitDate   : String?
    let changes         : [TransactionChange]
    let clientName      : String?
    let amount          : Double
    
    init(unboxer: Unboxer) throws {
        change          = try unboxer.unbox(key: "change")
        amount          = try unboxer.unbox(keyPath: "transaction.amount")
        after           = try unboxer.unbox(key: "after")
        before          = try unboxer.unbox(keyPath: "transaction.balance_before")
        date            = try unboxer.unbox(key: "date", formatter: DateFormatter(format: "yyyy-MM-dd' 'HH:mm:ss"))
        type            = try unboxer.unbox(key: "type")
        place           = try unboxer.unbox(key: "place")
        points          = unboxer.unbox(key: "points")
        lastVisitDate   = unboxer.unbox(key: "lastVisitDate")
        changes         = try unboxer.unbox(key: "changes")
        clientName      = unboxer.unbox(key: "clientName")
    }
    
}
