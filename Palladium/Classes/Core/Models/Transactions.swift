//
//  Transactions.swift
//  Palladium
//
//  Created by Igor Danich on 9/10/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation

import Unbox
import DCFoundation

class Transactions: Unboxable {
    
    var items       = [Transaction]()
    var meta        : Meta?
    
    init() {}
    
    required init(unboxer: Unboxer) throws {
        items   = try unboxer.unbox(key: "items")
        meta    = unboxer.unbox(key: "_meta")
    }
    
    func mergeTo(transactions: Transactions) {
        items << transactions.items
        meta = transactions.meta
    }
    
}
