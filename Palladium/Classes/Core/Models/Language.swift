//
//  Language.swift
//  Addzer
//
//  Created by Eugene Donov on 3/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct Language: Unboxable {
    
    let tag         : Int
    let serverCode  : String
    let shortCode   : String
    let name        : String
    
    init(unboxer: Unboxer) throws {
        tag         = try unboxer.unbox(key: "tag")
        serverCode  = try unboxer.unbox(key: "server_code")
        shortCode   = try unboxer.unbox(key: "short_code")
        name        = try unboxer.unbox(key: "name")
        
    }
    
}
