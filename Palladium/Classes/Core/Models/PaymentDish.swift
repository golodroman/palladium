//
//  PaymentDish.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

struct PaymentDish: Unboxable {
    
    let id      : Int
    let code    : String
    let name    : String
    let price   : Int
    let amount  : Int
    let quantity: Int
    
    init(unboxer: Unboxer) throws {
        id          = try unboxer.unbox(key: "id")
        code        = try unboxer.unbox(key: "code")
        name        = try unboxer.unbox(key: "name")
        price       = try unboxer.unbox(key: "price")
        amount      = try unboxer.unbox(key: "amount")
        quantity    = try unboxer.unbox(key: "quantity")
    }
    
}
