//
//  Places.swift
//  Addzer
//
//  Created by Eugene Donov on 2/7/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import DCFoundation

class Places: Unboxable {
    
    var items       = [Place]()
    var meta        : Meta?
    var location    : Location?
    
    init() {}
    
    required init(unboxer: Unboxer) throws {
        items       = try unboxer.unbox(key: "items")
        meta        = unboxer.unbox(key: "_meta")
        location    = unboxer.unbox(key: "location")
    }
    
    func mergeTo(places: Places) {
        items << places.items
        location = places.location
        meta = places.meta
    }
    
}
