//
//  TransactionChange.swift
//  Palladium
//
//  Created by Igor Danich on 9/10/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct TransactionChange: Unboxable {
    
    let value: Double
    
    init(unboxer: Unboxer) throws {
        value = try unboxer.unbox(key: "change")
    }
    
}
