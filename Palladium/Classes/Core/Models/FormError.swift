//
//  FormError.swift
//  Palladium
//
//  Created by Igor Danich on 10/3/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

struct FormError: Unboxable {
    
    let field   : String
    let message : String
    
    init(unboxer: Unboxer) throws {
        field   = try unboxer.unbox(key: "field")
        message = try unboxer.unbox(key: "message")
    }
    
}
