//
//  Location.swift
//  Addzer
//
//  Created by Eugene Donov on 2/20/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct Location: Unboxable {
    
    var lat     : Double
    var lon     : Double
    var radius  : Double
    
    init(unboxer: Unboxer) throws {
        lat     = try unboxer.unbox(key: "lat")
        lon     = try unboxer.unbox(key: "lon")
        radius  = try unboxer.unbox(key: "radius")
    }
        
}
