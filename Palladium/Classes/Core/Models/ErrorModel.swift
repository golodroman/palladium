//
//  ErrorModel.swift
//  Addzer
//
//  Created by Eugene Donov on 2/12/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import Foundation

struct ErrorModel: Unboxable {
    
    let field   : String?
    let name    : String?
    let message : String
    let status  : Int?
    
    var error: NSError {
        return NSError(localizedKey: message, code: status)!
    }
    
    init(unboxer: Unboxer) throws {
        field   = unboxer.unbox(key: "field")
        message = try unboxer.unbox(key: "message")
        status  = unboxer.unbox(key: "status")
        name    = unboxer.unbox(key: "name")
    }
    
}
