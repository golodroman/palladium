//
//  Place.swift
//  Addzer
//
//  Created by Eugene Donov on 2/7/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import Foundation

struct Place: Unboxable {
    
    let id              : Int
    let regionId        : Int?
    let cityId          : Int?
    let userId          : Int?
    let name            : String?
    let email           : String?
    let logo            : String?
    let rules           : String?
    let additionalInfo  : String?
    let userBalance     : Double?
    let discount        : Double?
    let allowedBonus    : Double?
    let chain           : Chain?
    let background      : Image?
    let template        : PlaceTemplate?
    let promotion       : Promotion?
    let categories      : [PlaceCategory]?
    let addresses       : [PlaceAddress]?
    let userCheckins    : [PlaceCheckin]?
    let images          : [PlaceImage]?
    let distance        : Double?
    let categoryIcon    : String?
    
    init() {
        id = 1
        regionId = nil
        cityId = nil
        userId = nil
        name = nil
        email = nil
        logo = nil
        rules = nil
        additionalInfo = nil
        userBalance = nil
        discount = 10
        allowedBonus = nil
        chain = nil
        background = nil
        template = nil
        promotion = nil
        categories = nil
        addresses = nil
        userCheckins = nil
        images = nil
        distance = 10
        categoryIcon = nil
    }
    
    var instagramURL: URL {
        return URL(string: "place.menu.instagram.link".localized)!
    }
    
    var facebookURL: URL {
        return URL(string: "place.menu.facebook.link".localized)!
    }
    
    var facebookIdURL: URL {
        return URL(string: "place.menu.facebook.link.id".localized)!
    }

    var workTime: String? {
        return self.addresses?.first?.workTime
    }
    
    var address: String? {
        return self.addresses?.first?.address
    }
    
    var distanceString: String? {
        return "\(String(format: "%3.2f", distance ?? 0)) \(L10n.GeneralTextKm.string)"
    }
    
    var typeName: String? {
        if let categories = self.categories, 0 < categories.count {
            let arr3 = categories.filter() { 3 == $0.level }
            if 0 < arr3.count {
                return self.typeName(with: arr3)
            }
            let arr2 = categories.filter() { 2 == $0.level }
            if 0 < arr2.count {
                return self.typeName(with: arr2)
            }
            let arr1 = categories.filter() { 1 == $0.level }
            if 0 < arr1.count {
                return self.typeName(with: arr1)
            }
        }
        return nil
    }
    
    var balanceValue: Double {
        if let chainValue = self.chain?.balanceValue, 0 < chainValue {
            return chainValue
        } else if let placeValue = self.userCheckins?.first?.balanceValue, 0 < placeValue {
            return placeValue
        }
        return 0
    }

    var numberOfReferral2: Int {
        if let chainNum = self.chain?.numberOfReferral2, 0 < chainNum {
            return chainNum
        } else if let placeNum = self.userCheckins?.first?.numberOfReferral2, 0 < placeNum {
            return placeNum
        }
        return 0
    }

    var numberOfReferral3: Int {
        if let chainNum = self.chain?.numberOfReferral2, 0 < chainNum {
            return chainNum
        } else if let placeNum = self.userCheckins?.first?.numberOfReferral3, 0 < placeNum {
            return placeNum
        }
        return 0
    }

    var referralPercent1: String {
        if let template = self.template {
            if let ref = template.ref1 {
                return ref
            }
        } else {
            if let rules = self.rules?.components(separatedBy: ","), 0 < rules.count {
                return rules[0]
            }
        }
        return "0"
    }

    var referralPercent2: String {
        if let template = self.template {
            if let ref = template.ref2 {
                return ref
            }
        } else {
            if let rules = self.rules?.components(separatedBy: ","), 1 < rules.count {
                return rules[1]
            }
        }
        return "0"
    }
    
    var referralPercent3: String {
        if let template = self.template {
            if let ref = template.ref3 {
                return ref
            }
        } else {
            if let rules = self.rules?.components(separatedBy: ","), 2 < rules.count {
                return rules[2]
            }
        }
        return "0"
    }
    
    init(unboxer: Unboxer) throws {
        id              = try unboxer.unbox(key: "id")
        categoryIcon    = unboxer.unbox(key: "category_icon")
        regionId        = unboxer.unbox(key: "region_id")
        rules           = unboxer.unbox(key: "rules")
        additionalInfo  = unboxer.unbox(key: "additional_info")
        cityId          = unboxer.unbox(key: "city_id")
        userId          = unboxer.unbox(key: "user_id")
        name            = unboxer.unbox(key: "name")
        discount        = unboxer.unbox(key: "discount")
        allowedBonus    = unboxer.unbox(key: "allowed_bonus")
        email           = unboxer.unbox(key: "email")
        logo            = unboxer.unbox(key: "logo")
        userBalance     = unboxer.unbox(key: "user_balance")
        categories      = unboxer.unbox(key: "categories")
        addresses       = unboxer.unbox(key: "place_addresses")
        userCheckins    = unboxer.unbox(key: "userCheckins")
        chain           = unboxer.unbox(key: "chain")
        template        = unboxer.unbox(key: "template")
        background      = unboxer.unbox(key: "background")
        images          = unboxer.unbox(key: "images")
        promotion       = unboxer.unbox(key: "promotion")
        distance        = unboxer.unbox(key: "distance")
    }
    
    // MARK: - Private
    
    fileprivate func typeName(with categories: [PlaceCategory]) -> String {
        var strings = [String]()
        for category in categories {
            if let name = category.nameCategoryBrand, let addresses = self.addresses, (0 < name.length && 1 < addresses.count) {
                strings.append(name)
            }else if let name = category.name, 0 < name.length {
                strings.append(name)
            }
        }
        var result = ""
        for i in 0..<strings.count {
            let str = strings[i]
            if 0 < i {
                result += ", "
            }
            result += str
        }
        return result
    }
    
}
