//
//  Promotion.swift
//  Addzer
//
//  Created by Eugene Donov on 2/24/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct Promotion: Unboxable {
    
    let id          : Int
    let placeId     : Int?
    let name        : String?
    let img         : Image?
    let desc        : String?
    let descFull    : String?
    let distance    : Double?
    
    var distanceString: String? {
        guard let distance = self.distance else {return nil}
        var km = Int(distance)/1000
        let m = Int(distance) - km*1000
        if m >= 500 {km += 1}
        return km == 0 ? "\(m) \(L10n.GeneralTextM.string)" : "\(km) \(L10n.GeneralTextKm.string)"
    }
    
    init(unboxer: Unboxer) throws {
        id          = try unboxer.unbox(key: "id")
        placeId     = unboxer.unbox(key: "place_id")
        name        = unboxer.unbox(key: "name")
        img         = unboxer.unbox(key: "img")
        desc        = unboxer.unbox(key: "description")
        descFull    = unboxer.unbox(key: "description_full")
        distance    = unboxer.unbox(key: "distance")
    }
    
}
