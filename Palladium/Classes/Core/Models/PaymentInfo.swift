//
//  PaymentInfo.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

struct PaymentInfo: Unboxable {
    
    let id      : Int
    let name    : String
    let amount  : Double
    
    init(unboxer: Unboxer) throws {
        id      = try unboxer.unbox(key: "id")
        name    = try unboxer.unbox(key: "name")
        amount  = try unboxer.unbox(key: "amount")
    }
    
}
