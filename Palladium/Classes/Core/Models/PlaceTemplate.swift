//
//  PlaceTemplate.swift
//  Addzer
//
//  Created by Eugene Donov on 2/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct PlaceTemplate: Unboxable {
    
    let ref1 : String?
    let ref2 : String?
    let ref3 : String?
    
    init(unboxer: Unboxer) throws {
        ref1 = unboxer.unbox(key: "ref_1")
        ref2 = unboxer.unbox(key: "ref_2")
        ref3 = unboxer.unbox(key: "ref_3")
    }
        
}
