//
//  PlaceCheckin.swift
//  Addzer
//
//  Created by Eugene Donov on 2/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import Foundation

struct PlaceCheckin: Unboxable {
    
    let balance             : String?
    let referStats          : String?
    let discount            : Double?
    let invitedThisMonth    : Int?
    let invitedPrevMonth    : Int?
    
    var balanceValue: Double {
        if let balance = (self.balance as NSString?)?.doubleValue, 0 < balance {
            return balance
        }
        return 0
    }
    
    var numberOfReferral2: Int {
        if let components = self.referStats?.components(separatedBy: ","), 1 < components.count {
            let num = (components[0] as NSString).integerValue
            if 0 < num {
                return num
            }
        }
        return 0
    }

    var numberOfReferral3: Int {
        if let components = self.referStats?.components(separatedBy: ","), 1 < components.count {
            let num = (components[1] as NSString).integerValue
            if 0 < num {
                return num
            }
        }
        return 0
    }
    
    init(unboxer: Unboxer) throws {
        balance             = unboxer.unbox(key: "balance")
        referStats          = unboxer.unbox(key: "refer_stats")
        discount            = unboxer.unbox(key: "discount")
        invitedPrevMonth    = unboxer.unbox(key: "invited_prev_month")
        invitedThisMonth    = unboxer.unbox(key: "invited_this_month")
    }

}
