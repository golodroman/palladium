//
//  PaymentCard.swift
//  Palladium
//
//  Created by Igor Danich on 9/15/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

enum PaymentCardType {
    case visa
    case mastercard
    case unknown
}

struct PaymentCard: Unboxable {
    
    let id              : Int
    let number          : String
    let expirationMonth : Int?
    let expirationYear  : Int?
    let cvc             : String?
    let holder          : String?
    
    var type: PaymentCardType {
        if number.hasPrefix("4") {
            return .visa
        } else if number.hasPrefix("5") {
            return .mastercard
        } else {
            return .unknown
        }
    }
    
    init(unboxer: Unboxer) throws {
        id              = try unboxer.unbox(key: "card_id")
        number          = try unboxer.unbox(key: "card_number")
        cvc             = unboxer.unbox(key: "cvc")
        expirationMonth = unboxer.unbox(key: "month_exp")
        expirationYear  = unboxer.unbox(key: "month_year")
        holder          = unboxer.unbox(key: "holder")
    }
    
    init(number: String, expirationMonth: Int, expirationYear: Int, cvc: String, holder: String) {
        self.number = number
        self.expirationMonth = expirationMonth
        self.expirationYear = expirationYear
        self.cvc = cvc
        self.holder = holder
        self.id = -1
    }
    
}
