//
//  User.swift
//  Addzer
//
//  Created by Eugene Donov on 3/2/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct User: Unboxable {
    
    let id          : Int
    let name        : String?
    let lastName    : String?
    let email       : String?
    let accessToken : String
    let dob         : String?
    let locale      : String?
    let photo       : String?
    let points      : Int?
    let phones      : String?
    
    var fullName: String {
        if let name = name {
            if let lastName = lastName {
                return name + " " + lastName
            } else {
                return name
            }
        } else if let lastName = lastName {
            return lastName
        }
        return ""
    }
    
    init(name: String?, lastName: String?, email: String?, dob: String?) {
        id = -1
        accessToken = ""
        locale = nil
        photo = nil
        points = 0
        phones = nil
        self.name = name
        self.lastName = lastName
        self.email = email
        self.dob = dob
    }
    
    init(unboxer: Unboxer) throws { 
        id          = try unboxer.unbox(key: "id")
        name        = unboxer.unbox(key: "name")
        email       = unboxer.unbox(key: "email")
        dob         = unboxer.unbox(key: "dob")
        locale      = unboxer.unbox(key: "locale")
        photo       = unboxer.unbox(key: "photo")
        phones      = unboxer.unbox(key: "phones")
        if unboxer.dictionary.allKeys.contains("last_name") {
            lastName    = unboxer.unbox(key: "last_name")
            accessToken = try unboxer.unbox(key: "access_token")
            points      = unboxer.unbox(key: "total_balance")
        } else {
            lastName    = unboxer.unbox(key: "lastName")
            accessToken = try unboxer.unbox(key: "accessToken")
            points      = unboxer.unbox(key: "points")
        }
    }

}
