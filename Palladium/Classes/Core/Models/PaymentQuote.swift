//
//  TransactionQuote.swift
//  Palladium
//
//  Created by Igor Danich on 10/3/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

struct PaymentQuote: Unboxable {
    
    let priceDiscount   : Int
    let type            : PaymentQuoteType
    let currency        : String
    
    var currencySymbol  : String {
        return currency.currencySymbol
    }
    
    var bonus: Double {
        return type.amount <= 0 ? 0 : Double(type.balanceEarnings)/Double(type.amount)
    }
    
    init() {
        priceDiscount = -200
        type = PaymentQuoteType()
        currency = "$"
    }
    
    init(unboxer: Unboxer) throws {
        priceDiscount   = try unboxer.unbox(key: "price_discount")
        type            = try unboxer.unbox(keyPath: "types.3")
        currency        = try unboxer.unbox(keyPath: "currency")
    }
    
}
