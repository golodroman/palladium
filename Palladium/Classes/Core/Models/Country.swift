//
//  Country.swift
//  Addzer
//
//  Created by Eugene Donov on 3/17/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct Country: Unboxable {
    
    let name        : String
    let dialCode    : String
    let code        : String
    
    var flag        : String {
        return "CountryPicker.bundle/\(code)"
    }
    
    init(unboxer: Unboxer) throws {
        name        = try unboxer.unbox(key: "name")
        dialCode    = try unboxer.unbox(key: "dial_code")
        code        = try unboxer.unbox(key: "code")
    }
        
}
