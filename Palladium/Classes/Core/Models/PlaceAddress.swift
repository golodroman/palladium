//
//  PlaceAddress.swift
//  Addzer
//
//  Created by Eugene Donov on 2/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct PlaceAddress: Unboxable {
    
    let id          : Int
    let address     : String?
    let workTime    : String?
    let distance    : Double?
    let latitude    : Double?
    let longitude   : Double?
    let phones      : [String]?
    
    init(unboxer: Unboxer) throws {
        id          = try unboxer.unbox(key: "id")
        address     = unboxer.unbox(key: "address")
        workTime    = unboxer.unbox(key: "work_time")
        distance    = unboxer.unbox(key: "distance")
        latitude    = unboxer.unbox(key: "geo_lat")
        longitude   = unboxer.unbox(key: "geo_lon")
        phones      = unboxer.unbox(key: "phones")
    }
    
}
