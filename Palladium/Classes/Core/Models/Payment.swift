//
//  Payment.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCFoundation
import Unbox

struct Payment: Unboxable {
    
    let id          : Int
    let place       : Place
    let promocode   : String
    let date        : Date
    let detail      : PaymentDetail?
    let guid        : String
    let quote       : PaymentQuote
    let amount      : Int
    let status      : String?
    
    var orderSum    : Int {
        return amount > 0 ? amount : (detail?.orderSum ?? 0)
    }
    
    var discountSum: Int {
        return detail?.discountSum ?? 0
    }
    
    static func test() -> Payment {
        return try! unbox(data: try! Data(contentsOf: Bundle.main.url(forResource: "test", withExtension: "json")!))
    }
        
    init(unboxer: Unboxer) throws {
        id          = try unboxer.unbox(key: "id")
        place       = try unboxer.unbox(key: "place")
        promocode   = try unboxer.unbox(key: "promocode")
        date        = try unboxer.unbox(key: "timestamp", formatter: DateFormatter(format: "YYYY-MM-dd dd:mm:ss"))
        detail      = unboxer.unbox(key: "detail")
        guid        = try unboxer.unbox(key: "guid")
        quote       = try unboxer.unbox(key: "quote")
        amount      = try unboxer.unbox(key: "amount")
        status      = unboxer.unbox(key: "status")
    }
    
    init() {
        id = 0
        place = Place()
        promocode = "asd"
        date = Date()
        detail = PaymentDetail()
        guid = "asdasd"
        quote = PaymentQuote()
        amount = 1
        status = nil
    }
    
}
