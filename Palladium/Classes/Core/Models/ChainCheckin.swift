//
//  ChainCheckin.swift
//  Addzer
//
//  Created by Eugene Donov on 2/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct ChainCheckin: Unboxable {
    
    let balance     : Double
    let refer2      : Int
    let refer3      : Int
    let isCheckin   : Bool?
    
    init(unboxer: Unboxer) throws {
        balance     = try unboxer.unbox(key: "balance")
        refer2      = try unboxer.unbox(key: "refer2")
        refer3      = try unboxer.unbox(key: "refer3")
        isCheckin   = unboxer.unbox(key: "isCheckin")
    }
    
}
