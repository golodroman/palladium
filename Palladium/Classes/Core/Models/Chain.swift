//
//  Chain.swift
//  Addzer
//
//  Created by Eugene Donov on 2/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct Chain: Unboxable {
    
    let id              : Int
    let name            : String?
    let regionId        : Int?
    let referralSystem  : Int?
    let places          : [Places]?
    let userCheckin     : ChainCheckin?
    
    var quotationName: String {
        if let name = self.name {
            return "«\(name)»"
        }
        return ""
    }
    
    var countString: String {
        var count = 0
        var key = "chain.number.5"
        if let aCount = self.places?.count {
            count = aCount
        }
        if 5 > count && count > 0 {
            key = "chain.number.234"
            if 2 > count {
                key = "chain.number.1"
            }
        }
        return "\(count) \(key.localized)"
    }
    
    var balanceValue: Double {
        return userCheckin?.balance ?? 0
    }
    
    var numberOfReferral2: Int {
        return userCheckin?.refer2 ?? 0
    }

    var numberOfReferral3: Int {
        return userCheckin?.refer3 ?? 0
    }
    
    init(unboxer: Unboxer) throws {
        id              = try unboxer.unbox(key: "id")
        name            = unboxer.unbox(key: "name")
        regionId        = unboxer.unbox(key: "region_id")
        referralSystem  = unboxer.unbox(key: "referral_system")
        places          = unboxer.unbox(key: "places_in_chain")
        userCheckin     = unboxer.unbox(key: "userCheckin")
    }   
    
}
