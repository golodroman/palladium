//
//  PlaceCategory.swift
//  Addzer
//
//  Created by Eugene Donov on 2/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct PlaceCategory: Unboxable {
    
    let id                  : Int
    let name                : String?
    let logo                : String?
    let lft                 : Int?
    let rgt                 : Int?
    let level               : Int?
    let root                : Int?
    let rootOrder           : Int?
    let nameCategoryBrand   : String?
    
    init(unboxer: Unboxer) throws {
        id                  = try unboxer.unbox(key: "id")
        name                = unboxer.unbox(key: "name")
        logo                = unboxer.unbox(key: "logo")
        lft                 = unboxer.unbox(key: "lft")
        rgt                 = unboxer.unbox(key: "rgt")
        level               = unboxer.unbox(key: "level")
        root                = unboxer.unbox(key: "root")
        rootOrder           = unboxer.unbox(key: "root_order")
        nameCategoryBrand   = unboxer.unbox(key: "name_category_brand")
    }
    
}
