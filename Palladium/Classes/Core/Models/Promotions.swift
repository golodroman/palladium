//
//  Promotions.swift
//  Addzer
//
//  Created by Eugene Donov on 2/24/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import DCFoundation

class Promotions: Unboxable {
    
    var items       = [Promotion]()
    var meta        : Meta?
    
    init() {}
    
    required init(unboxer: Unboxer) throws {
        items       = try unboxer.unbox(key: "items")
        meta        = unboxer.unbox(key: "_meta")
    }
    
    func mergeTo(promotions: Promotions) {
        items << promotions.items
        meta = promotions.meta
    }
    
}
