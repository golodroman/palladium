//
//  Image.swift
//  Addzer
//
//  Created by Eugene Donov on 2/24/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox

struct Image: Unboxable {
    
    let thumb: String?
    let large: String?
    
    init(unboxer: Unboxer) throws {
        thumb = unboxer.unbox(key: "thumb")
        large = unboxer.unbox(key: "large")
    }
    
}
