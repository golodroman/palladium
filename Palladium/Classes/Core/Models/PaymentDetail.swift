//
//  PaymentDetail.swift
//  Palladium
//
//  Created by Igor Danich on 9/22/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

struct PaymentDetail: Unboxable {
    
    let paid    : Int
    let sessions: [PaymentSession]
    let name    : String
    
    let orderSum: Int
    let discountSum: Int
    
    init() {
        paid = 0
        name = "asddsa"
        orderSum = 100000000
        discountSum = 10000
        sessions = []
    }
    
    init(unboxer: Unboxer) throws {
        paid        = try unboxer.unbox(key: "paid")
        sessions    = try unboxer.unbox(key: "Session")
        name        = try unboxer.unbox(key: "orderName")
        orderSum    = try unboxer.unbox(key: "orderSum")
        discountSum    = try unboxer.unbox(key: "discountSum")
    }
    
}
