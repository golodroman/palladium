//
//  String+Network.swift
//  Addzer
//
//  Created by Eugene Donov on 1/12/17.
//  Copyright © 2017 r-style. All rights reserved.
//


extension String {
    
//    func URLEncodedString() -> String? {
//        let customAllowedSet =  CharacterSet.urlQueryAllowed
//        let escapedString = self.addingPercentEncoding(withAllowedCharacters: customAllowedSet)
//        return escapedString
//    }
    
    static func query(with parameters: [String:String]) -> String? {
        if (parameters.count == 0)
        {
            return nil
        }
        var queryString : String? = nil
        for (key, value) in parameters {
            let encodedKey = key.URLEncodedString()
            let encodedValue = value.URLEncodedString()
            if queryString == nil {
                queryString = "?"
            } else {
                queryString! += "&"
            }
            queryString! += encodedKey + "=" + encodedValue
        }
        return queryString
    }
    
}
