//
//  HTTPService.swift
//  Palladium
//
//  Created by Igor Danich on 9/6/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import DCFoundation

typealias ServiceHandler = ((_ error: NSError?) -> Void)?

fileprivate var NetworkAuthorizationToken: String?

class NetworkModel: Unboxable {
    
    required init(unboxer: Unboxer) throws {
    }

}

protocol NetworkService {}

extension NetworkService {
    
    static var authToken: String? {
        get {return NetworkAuthorizationToken}
        set {NetworkAuthorizationToken = newValue}
    }
    
    var httpHeaders: [String:String] {
        var fields = [String:String]()
        fields["Accept"] = "application/json; version=\(apiVersion)"
        fields["Accept-Language"] = LanguageService.shared.language.serverCode
        if let token = NetworkAuthorizationToken {
            let str = "\(token):"
            let data = str.data(using: .utf8)
            if let base64 = data?.base64EncodedString() {
                fields["Authorization"] = "Basic \(base64)"
            }
        }
        return fields
    }
    
    var apiServer: String {
//        return "https://bonus.palladium.kz/v1"
        return UseAddzer ? "http://dev.addzer.com/v1" : "https://palladium.addzer.com/v1"
    }
    
    var apiVersion: String {
        return "1.1.0"
    }
    
    func cancel(requestId: String?) {
        HTTPSession.shared.cancel(requestId: requestId)
    }
    
    @discardableResult
    func request(path: String, method: HTTPRequest.Method = .get, query: [String:Any] = [:], body: Any? = nil, contentType: HTTPRequest.ContentType = .json, _ handler: ((NetworkResponse<NetworkModel>) -> Void)?) -> String? {
        return request(path: path, method: method, query: query, body: body, contentType: contentType, type: NetworkModel.self, handler)
    }
    
    @discardableResult
    func request<T>(path: String, method: HTTPRequest.Method = .get, query: [String:Any] = [:], body: Any? = nil, contentType: HTTPRequest.ContentType = .json, type: T.Type, _ handler: ((NetworkResponse<T>) -> Void)?) -> String? {
        let request = HTTPRequest(url: URL(string: apiServer + path)!, method: method)
        request.query = query
        request.contentType = contentType
        request.body = body
        request.headers = httpHeaders
        HTTPSession.shared.send(request: request, handler)
        return request.id
    }

}
