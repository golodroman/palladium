//
//  NetworkResponse.swift
//  Palladium
//
//  Created by Igor Danich on 9/6/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import DCFoundation

class NetworkResponse<T:Unboxable>: HTTPResponse {
    
    var dictionary: [String:Any]? {
        if let data = data {
            if let info = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
                return info
            }
        }
        return nil
    }
    
    var error: NSError? {
        if responseError != nil {
            return NSError(localizedKey: L10n.FoundTheError.string)
        } else if let error = errorModel {
            return NSError(localizedKey: error.message, code: error.status)
        } else if model == nil && list == nil {
            return NSError(localizedKey: L10n.FoundTheError.string)
        }
        return nil
    }
    
    var errorModel: ErrorModel? {
        if let data = data {
            if let value: ErrorModel = try? unbox(data: data) {
                return value
            } else if let values: [ErrorModel] = try? unbox(data: data) {
                return values.first
            }
            return nil
        }
        return nil
    }
    
    var string: String? {
        if let data = data {
            return String(data: data, encoding: .utf8) ?? nil
        }
        return nil
    }

    var model: T? {
        if let data = data {
            do {
                let value: T = try unbox(data: data)
                return value is NetworkModel ? nil : value
            } catch {
                print((error as? UnboxError) ?? "")
            }
        }
        return nil
    }
    
    var list: [T]? {
        if let data = data {
            do {
                let value: [T] = try unbox(data: data)
                return value is [NetworkModel] ? nil : value
            } catch {
                print((error as? UnboxError) ?? "")
            }
        }
        return nil
    }
    
}
