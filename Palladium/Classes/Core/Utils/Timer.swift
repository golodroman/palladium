//
//  Timer.swift
//  Call
//
//  Created by Igor Danich on 29.09.16.
//

import Foundation

class Timer: NSObject {
    
    fileprivate var handler: ((_ timer: Timer) -> Void)?
    fileprivate var repeats: Bool
    fileprivate var time: TimeInterval
    fileprivate weak var timer: Foundation.Timer?
    
    @discardableResult open static func scheduled(_ ti: TimeInterval, repeats: Bool = false, handler: ((_ timer: Timer) -> Void)?) -> Timer? {
        let timer = Timer(ti: ti, repeats: repeats, handler: handler)
        timer.fire()
        return timer
    }
    
    public init(ti: TimeInterval, repeats: Bool, handler: ((_ timer: Timer) -> Void)?) {
        time = ti
        self.handler = handler
        self.repeats = repeats
        super.init()
    }
    
    open func fire() {
        timer?.invalidate()
        timer = Foundation.Timer.scheduledTimer(timeInterval: time, target: self, selector: #selector(Timer.onTimer(_:)), userInfo: ["timer":self], repeats: repeats)
    }
    
    open func invalidate() {
        timer?.invalidate()
        timer = nil
    }
    
    @objc func onTimer(_ aTimer: Foundation.Timer) {
        if !repeats {
            timer?.invalidate()
        }
        handler?(self)
    }
    
}
