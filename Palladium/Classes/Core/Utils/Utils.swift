//
//  Utils.swift
//  Addzer
//
//  Created by Eugene Donov on 10/26/16.
//  Copyright © 2016 r-style. All rights reserved.
//

import DCFoundation
import Unbox
import Wrap

let UseAddzer = false

@discardableResult func DirectoryRemove(atPath: String) -> Bool {
    do {
        try FileManager.default.removeItem(atPath: atPath)
        return true
    } catch {
        print(error)
    }
    return false
}

extension String {
    
    var addzerStyledRepresentation: String? {
        let serverDate = (self as String).date(format: "yyyy-MM-dd HH:mm:ss")
        if let clientDate = serverDate?.dateByAddingSecondsFromGMT {
            var format = "dd MMMM yyyy HH:mm"
            if clientDate.year == Date().year {
                format = "dd MMMM HH:mm"
            }
            let locale = Locale(identifier: LanguageService.shared.language.shortCode)
            return clientDate.string(format: format, locale: locale)
        }
        return nil
    }
    
    var phoneCallURL: URL {
        var string = trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        string = string.replacingOccurrences(of: "(", with: "")
        string = string.replacingOccurrences(of: ")", with: "")
        string = string.replacingOccurrences(of: " ", with: "")
        string = string.replacingOccurrences(of: "-", with: "")
        string = string.replacingOccurrences(of: "+", with: "")
        return URL(string: "tel://+" + string)!
    }
    
    func replacing(range: NSRange, string: String) -> String {
        return (self as NSString).replacingCharacters(in: range, with: string)
    }
    
    func substring(range: NSRange) -> String {
        return (self as NSString).substring(with: range)
    }
    
    var formattedPaymentCardNumber: String {
        let string = replacingOccurrences(of: " ", with: "")
        var items = [String]()
        var i = 0
        while i <= string.length {
            items << string.substring(range: NSMakeRange(i, i + 4 < string.length ? 4 : string.length - i))
            i += 4
        }
        return (items.count < 3 ? items : (items.subArray(range: NSMakeRange(0, 3)))).joinedBy(separator: " ")
    }
    
}

extension Unboxable {
    
    var dictionary: [String:Any]? {
        return try? wrap(self)
    }
    
}

extension DateFormatter {
    
    convenience init(format: String) {
        self.init()
        dateFormat = format
    }
    
}

extension Double {
    
    func priceString(currency: String = "") -> String {
        let nf = NumberFormatter()
        nf.currencySymbol = currency
        nf.numberStyle = .currency
        nf.currencyGroupingSeparator = " "
        nf.currencyDecimalSeparator = "."
        return nf.string(from: NSNumber(value: self/100)) ?? ""
    }
    
}

extension Int {
    
    func priceString(currency: String = "") -> String {
        let nf = NumberFormatter()
        nf.currencySymbol = currency
        nf.numberStyle = .currency
        nf.currencyGroupingSeparator = " "
        nf.currencyDecimalSeparator = "."
        return (nf.string(from: NSNumber(value: self/100)) ?? "").components(separatedBy: ".").first ?? ""
    }
    
}

extension String {
    
    var currencySymbol: String {
        return (Locale.availableIdentifiers.makeArray { (_, identifier) in
            let locale = Locale(identifier: identifier)
            return locale.currencyCode == self ? locale : nil
        } as [Locale]).first?.currencySymbol ?? ""
    }
    
}

extension Date {
    
    init(day: Int, month: Int, year: Int) {
        var comps = DateComponents()
        comps.day = day
        comps.year = year
        comps.month = month
        if let date = NSCalendar.current.date(from: comps) {
            self.init(timeIntervalSince1970: date.timeIntervalSince1970)
        } else {
            self.init()
        }
    }
    
    var month: Int {
        return NSCalendar.current.component(.month, from: self)
    }
    
    var year: Int {
        return NSCalendar.current.component(.year, from: self)
    }
    
    func string(style: DateFormatter.Style) -> String {
        let df = DateFormatter()
        df.dateStyle = style
        return df.string(from: self)
    }
    
}

