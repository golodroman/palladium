//
//  CountryService.swift
//  Addzer
//
//  Created by Eugene Donov on 3/17/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import Foundation

class CountryService: NetworkService {

    fileprivate var country: Country?
    
    var defaultCountry: Country {
        return country ?? self.list0.first!
    }
    
    var lists: [[Country]] {
        return [self.list0, self.list1]
    }
    
    var countries: [Country] {
        return self.list0 + self.list1
    }
    
    func countryBy(countryCode: String) -> Country? {
        for country in self.countries {
            if country.code == countryCode {
                return country
            }
        }
        return nil
    }
    
    func countryBy(phone: String) -> Country? {
        for country in self.countries {
            if 0 < country.dialCode.length {
                if phone.hasPrefix(country.dialCode) {
                    return country
                }
            }
        }
        return nil
    }
    
    func fetchDefault(handler: ServiceHandler) {
        request(path: "/users/geoiplookup") { response in
            if let countryCode = response.string?.replacingOccurrences(of: "\"", with: "") {
                if let country = self.countryBy(countryCode: countryCode) {
                    self.country = country
                }
            }
            handler?(response.error)
        }
    }
    
    // MARK: - Private
    
    fileprivate lazy var list0: [Country] = {
        return self.list(with: "CountryList0")
    }()

    fileprivate lazy var list1: [Country] = {
        return self.list(with: "CountryList1").sorted(by: { $0.name < $1.name })
    }()
    
    fileprivate func list(with name: String) -> [Country] {
        if let url = Bundle.main.url(forResource: name, withExtension: "json") {
            if let data = try? Data(contentsOf: url) {
                return (try? unbox(data: data)) ?? []
            }
        }
        return []
    }
    
}
