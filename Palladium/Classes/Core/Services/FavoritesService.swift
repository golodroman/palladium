//
//  FavoritesService.swift
//  Palladium
//
//  Created by Igor Danich on 10/30/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation

class FavoritesService: NetworkService {
    
    private(set) var places = Places()
    
    func fetch(_ handler: ServiceHandler) {
        request(path: "/places?is_favorite=1", method: .get, type: Places.self) { (response) in
            if let places = response.model {
                self.places = places
            }
            handler?(response.error)
        }
    }
    
}
