//
//  LocationService.swift
//  Addzer
//
//  Created by Eugene Donov on 1/29/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import CoreLocation
import DCFoundation

let kLocationServiceDidChangeAvailabilityStatus = "LocationServiceDidChangeAvailabilityStatus"

class LocationService: NSObject {
    
    static let shared = LocationService()
    
    fileprivate(set) var isAvailable = true {
        didSet {
            if oldValue != self.isAvailable {
                NotificationPost(name: kLocationServiceDidChangeAvailabilityStatus)
            }
        }
    }
    
    var location: CLLocation? {
        return self.manager.location
    }
    
    override init() {
        super.init()
        isAvailable = isAvailable(with: CLLocationManager.authorizationStatus())
    }
    
    func updateLocationIfNeeded(_ handler: (() -> Void)?) {
        if .notDetermined == CLLocationManager.authorizationStatus() {
            self.handler = handler
            if #available(iOS 8.0, *) {
                manager.requestWhenInUseAuthorization()
            }
            manager.startMonitoringSignificantLocationChanges()
        } else {
            handler?()
        }
    }
    
    // MARK: - Private
    
    fileprivate var handler: (() -> Void)?
    
    fileprivate lazy var manager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyKilometer
        manager.distanceFilter = 8.1
        return manager
    }()
    
    fileprivate func isAvailable(with status: CLAuthorizationStatus) -> Bool {
        if .denied == status || .restricted == status {
            return false
        }
        return true
    }
    
}

extension LocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        isAvailable = isAvailable(with: status)
        if let _ = self.handler, .notDetermined != status {
            handler?()
            handler = nil
        }
    }
    
}
