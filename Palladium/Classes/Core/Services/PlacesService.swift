//
//  PlaceService.swift
//  Addzer
//
//  Created by Eugene Donov on 2/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation

class PlacesService: NetworkService {
    
    var hasPlacesAvailable: Bool {
        return places.meta?.hasPagesAvailable ?? true
    }
    
    private(set) var places     = Places()
    fileprivate var requestId   : String?
    
    func fetch(clear: Bool, _ handler: ServiceHandler) {
        var params = [String:Any]()
        params["latitude"] = LocationService.shared.location?.coordinate.latitude
        params["longitude"] = LocationService.shared.location?.coordinate.longitude
        if let perPage = places.meta?.perPage, false == clear {
            params["per-page"] = perPage
        } else {
            params["per-page"] = 20
        }
        if let currentPage = self.places.meta?.currentPage, false == clear {
            params["page"] = currentPage + 1
        } else {
            params["page"] = 1
        }
        cancel(requestId: requestId)
        requestId = request(path: "/places", method: .get, query: params, type: Places.self) { (response) in
            if let places = response.model {
                if clear {
                    self.places = places
                } else {
                    self.places.mergeTo(places: places)
                }
            }
            handler?(response.error)
        }
    }
    
    func fetchPlace(id: Int, _ handler: ((NSError?, Place?) -> Void)?) {
        var params = [String: Any]()
        params["latitude"] = LocationService.shared.location?.coordinate.latitude
        params["longitude"] = LocationService.shared.location?.coordinate.longitude
        request(path: "/places/\(id)", query: params, type: Place.self) { response in
            handler?(response.error, response.model)
        }
    }

}
