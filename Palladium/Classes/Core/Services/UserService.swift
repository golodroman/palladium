//
//  UserService.swift
//  Addzer
//
//  Created by Eugene Donov on 2/27/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCFoundation
import Wrap
import Unbox

class UserService: NSObject, NetworkService {
    
    var deviceId: String?
    static let shared = UserService()
    
    
    static let didLogin                 = "UserServiceDidLogin"
    static let didLogout                = "UserServiceDidLogout"
    fileprivate static let userDataKey  = "UserData"
    
    var isAuthorized: Bool {
        return UserService.shared.user?.id != nil
    }
    
    fileprivate var _user: User?
    var user: User? {
        get {return _user}
        set {
            _user = newValue
            UserService.authToken = newValue?.accessToken
            saveUser()
        }
    }
    
    var tokenBase64: String? {
        if let token = self.user?.accessToken, 0 < token.length {
            let str = "\(token):"
            let data = str.data(using: .utf8)
            if let base64 = data?.base64EncodedString() {
                return "Basic \(base64)"
            }
        }
        return nil
    }
    
    override init() {
        super.init()
        if let userInfo = UserDefaults()[UserService.userDataKey] as? [String:Any] {
            _user = try? unbox(dictionary: userInfo)
            UserService.authToken = _user?.accessToken
        }
    }
    
    func saveUser() {
        if let user = user {
            let data: [String:Any]? = try? wrap(user)
            UserDefaults()[UserService.userDataKey] = data
        } else {
            UserDefaults()[UserService.userDataKey] = nil
        }
        UserDefaults().synchronize()
    }
        
    func login(phone: String, code: String?, handler: ServiceHandler) {
        var object = [String:Any]()
        object["identifier"] = phone
        object["confirmation"] = code
        object["device_type"] = 1
        object["type"] = 1
        object["device_id"] = deviceId
        request(path: "/users/join-phone", method: .post, body: object, type: User.self) { (response) in
            var error = response.error
            if let user = response.model {
                self.user = user
            } else if let model = response.errorModel {
                if model.error.code != 403 {
                    error = NSError(localizedKey: model.message, code: 10)
                }
            }
            handler?(error)
            if self.isAuthorized {
                NotificationPost(name: UserService.didLogin)
            }
        }
    }
    
    func login(handler: ServiceHandler) {
        var object = [String:Any]()
        object["email"] = "a.svadkovskiy@addzer.com123"
        object["password"] = "qwerty"
        object["device_type"] = 1
        object["device_id"] = deviceId
        request(path: "/users/login", method: .post, body: object, type: User.self) { (response) in
            if let user = response.model {
                self.user = user
            }
            handler?(response.error)
            if self.isAuthorized {
                NotificationPost(name: UserService.didLogin)
            }
        }
    }
    
    func update(profile: User? = nil, _ handler: ServiceHandler) {
        if let profile = profile {
            var info = [String:Any]()
            info["name"] = profile.name
            info["last_name"] = profile.lastName
            info["email"] = profile.email
            info["dob"] = profile.dob
            request(path: "/users/profile", method: .put, body: info, type: User.self) { (response) in
                if let profile = response.model {
                    self.user = profile
                }
                handler?(response.error)
            }
        } else {
            request(path: "/users/me", method: .get, body: nil, type: User.self) { (response) in
                if let profile = response.model {
                    self.user = profile
                }
                handler?(response.error)
            }
        }
    }
    
    func update(photo: Data, type: String, _ handler: ServiceHandler) {
        let body = [MultipartItem(key: "photo", value: photo, mimeType: type, fileName: "photo")]
        request(path: "/user/profile-photo", method: .post, body: body, contentType: .multipartFormData, type: User.self) { (response) in
            if let user = response.model {
                self.user = user
            }
            handler?(response.error)
        }
    }
    
    func logout(_ handler: (() -> Void)?) {
        request(path: "/users/logout", method: .post) { _ in
            self.user = nil
            handler?()
            NotificationPost(name: UserService.didLogout)
        }
    }
    
}
