//
//  PaymentService.swift
//  Palladium
//
//  Created by Igor Danich on 9/21/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation
import Unbox

class PaymentService: NetworkService {
    
    var payment: Payment!
    private(set) var result : PaymentResult?
    
    var maxBonus: Int {
        guard let payment = payment else {return 0}
        var value = payment.quote.priceDiscount*Int(payment.place.allowedBonus ?? 0)
        value = value < payment.orderSum ? value : payment.orderSum
        return value > (UserService.shared.user?.points ?? 0) ? (UserService.shared.user?.points ?? 0) : value
    }
    
    func apply(code: String, _ handler: ServiceHandler) {
        request(path: "/rkeeper/promocode-check", method: .post, body: ["promocode":code], type: Payment.self) { (response) in
            var error = response.error
            if let payment = response.model {
                self.payment = payment
            } else if error == nil {
                error = NSError(localizedKey: L10n.FoundTheError.string)
            }
            handler?(response.error)
        }
    }
    
    func pay(card: PaymentCard?, bonuses: Int, _ handler: ServiceHandler) {
        var info = [String:Any]()
        info["card_id"] = card?.id
        info["bonuses"] = bonuses
        info["guid"] = payment?.guid
        request(path: "/rkeeper/pay", method: .post, body: info, type: PaymentResult.self) { (response) in
            var error = response.error
            if let result = response.model {
                self.result = result
            } else if let items: [FormError] = try? unbox(data: response.data ?? Data()) {
                error = NSError(localizedKey: (items.makeArray { (_, error) in
                    return error.message
                } as [String]).joined(separator: "\n"))
            }
            handler?(error)
        }
    }
    
}
