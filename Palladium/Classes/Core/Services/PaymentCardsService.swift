//
//  PaymentCardsService.swift
//  Palladium
//
//  Created by Igor Danich on 9/15/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCFoundation
import Unbox

class PaymentCardsService: NetworkService {

    private(set) var items = [PaymentCard]()
    
    struct PaymentCardList: Unboxable {
        let list: [PaymentCard]
        init(unboxer: Unboxer) throws {
            list = try unboxer.unbox(key: "list")
        }
    }
    
    func fetch(_ handler: ServiceHandler) {
        guard let id = UserService.shared.user?.id else {return}
        items = []
        request(path: "/credit-card/list", method: .post, body: ["user_id":id], type: PaymentCardList.self) { (response) in
            if let model = response.model {
                self.items = model.list
            }
            handler?(response.error)
        }
    }
    
    func addCard(card: PaymentCard, _ handler: ServiceHandler) {
        var info = [String:Any]()
        info["card_number"] = card.number
        info["cvc"] = card.cvc
        info["month_exp"] = card.expirationMonth
        info["year_exp"] = card.expirationYear
        info["cardholder"] = card.holder
        request(path: "/credit-card/register", method: .post, body: info) { response in
            handler?(response.error)
        }
    }
    
    func removeCard(card: PaymentCard, _ handler: ServiceHandler) {
        request(path: "/credit-card/remove", method: .post, body: ["card_id":card.id]) { (response) in
            self.fetch { _ in
                handler?(response.error)
            }
        }
    }
    
}
