//
//  PromotionService.swift
//  Addzer
//
//  Created by Eugene Donov on 2/24/17.
//  Copyright © 2017 r-style. All rights reserved.
//

class PromotionsService: NetworkService {
    
    var hasPromotionsAvailable: Bool {
        if let meta = self.promotions.meta {
            return meta.hasPagesAvailable
        }
        return false
    }
    
    private(set) var promotions = Promotions()
    fileprivate var requestId   : String?
    
    func clear() {
        self.promotions = Promotions()
    }
    
    func fetch(clear: Bool = false, _ handler: ServiceHandler) {
        var params = [String: Any]()
        params["latitude"] = LocationService.shared.location?.coordinate.latitude
        params["longitude"] = LocationService.shared.location?.coordinate.longitude
        if let perPage = self.promotions.meta?.perPage, false == clear {
            params["per-page"] = perPage
        } else {
            params["per-page"] = 20
        }
        if let currentPage = promotions.meta?.currentPage, false == clear {
            params["page"] = currentPage + 1
        } else {
            params["page"] = 1
        }
        cancel(requestId: requestId)
        requestId = request(path: "/promotions", query: params, type: Promotions.self) { (response) in
            if let promotions = response.model {
                if clear {
                    self.promotions = promotions
                } else {
                    self.promotions.mergeTo(promotions: promotions)
                }
            }
            handler?(response.error)
        }
    }
    
    func show(with ids: String, _ handler: ((Error?) -> Void)?) {
        clear()
        cancel(requestId: requestId)
        requestId = request(path: "/promotions/show", method: .post, body: ["ids": ids], type: Promotions.self) { (response) in
            if let promotions = response.model {
                let count = promotions.items.count
                promotions.meta = Meta(totalCount: count, pageCount: 1, currentPage: 1, perPage: count)
                self.promotions = promotions
            }
            handler?(response.error)
        }
    }
    
    func apply(code: String, _ handler: ServiceHandler) {
        request(path: "/promo-code/apply", method: .post, body: ["promo_code":code, "user_id": UserService.shared.user!.id]) { (response) in
            handler?(response.error)
        }
    }
    
}
