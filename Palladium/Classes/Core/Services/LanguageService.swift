//
//  LanguageService.swift
//  Addzer
//
//  Created by Eugene Donov on 3/8/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Unbox
import Foundation

class LanguageService: NSObject {
    
    static let shared = LanguageService()
    
    lazy var languages: [Language] = {
        return try! unbox(dictionaries: NSArray(contentsOf: Bundle.main.url(forResource: "language", withExtension: "plist")!) as! [UnboxableDictionary])
    }()
    
    var language: Language {
        let languages = self.languages
        if let code = UserService.shared.user?.locale {
            if let language = languages.filter({ $0.serverCode == code || $0.shortCode == code }).first {
                return language
            }
        }
        if let code = Bundle.main.preferredLocalizations.first {
            if let language = languages.filter({ $0.serverCode == code || $0.shortCode == code }).first {
                return language
            }
        }
        return languages.first!
    }

}
