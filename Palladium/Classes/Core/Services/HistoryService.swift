//
//  HistoryService.swift
//  Palladium
//
//  Created by Igor Danich on 8/23/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import DCFoundation

class HistoryService: NetworkService {
    
    private(set) var transactions = Transactions()
    
    var canLoadMore: Bool {
        return transactions.meta?.hasPagesAvailable ?? true
    }
    fileprivate var requestId: String?
    
    func fetch(clear: Bool, handler: ServiceHandler) {
        var info = [String:Any]()
        info["mix"] = 1
        
        if let perPage = self.transactions.meta?.perPage, false == clear {
            info["per-page"] = perPage
        } else {
            info["per-page"] = 20
        }
        if let currentPage = transactions.meta?.currentPage, false == clear {
            info["page"] = currentPage + 1
        } else {
            info["page"] = 1
        }
        
        cancel(requestId: requestId)
        requestId = request(path: "/users/balance-changes", query: info, type: Transactions.self) { (response) in
            if let transactions = response.model {
                if clear {
                    self.transactions = transactions
                } else {
                    self.transactions.mergeTo(transactions: transactions)
                }
            }
            handler?(response.error)
        }
        
    }

}
