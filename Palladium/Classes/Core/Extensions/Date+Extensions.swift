//
//  Date+Extensions.swift
//  Addzer
//
//  Created by Eugene Donov on 1/19/17.
//  Copyright © 2017 r-style. All rights reserved.
//

import Foundation

extension Date {
    
    func string(format: String, locale: Locale? = nil, timeZone: TimeZone? = nil) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        if let timeZone = timeZone {
            formatter.timeZone = timeZone
        }
        if let locale = locale {
            formatter.locale = locale
        }
        return formatter.string(from: self)
    }
    
    var dateByAddingSecondsFromGMT: Date {
        let timeZoneSecods = Double(TimeZone.current.secondsFromGMT())
        return self.addingTimeInterval(timeZoneSecods)
    }
    
}
