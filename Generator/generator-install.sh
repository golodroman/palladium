# /bin/sh

# Brew
BREW_LOCATION="/usr/local/bin/brew"
if [ -f $BREW_LOCATION ];
    then
        echo "Brew update"
        brew update
    else
        echo "Installing Brew..."
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# wget
WGET_LOCATION="/usr/local/bin/wget"
if [ ! -f $WGET_LOCATION ];
    then
    echo "Installing wget..."
        brew install wget
fi

# swiftgen
SWIFTGEN_LOCATION="/usr/local/bin/swiftgen"
if [ ! -f $SWIFTGEN_LOCATION ];
    then
        echo "Installing swiftgen..."
        brew install swiftgen
fi

# carthage
CARTHAGE_LOCATION="/usr/local/bin/carthage"
if [ ! -f $CARTHAGE_LOCATION ];
    then
        echo "Installing carthage..."
        brew install carthage
fi

echo "Installing templates..."
HOME=~
SWIFTGEN_LOCATION="$HOME/Library/Application Support/SwiftGen"

if [ ! -d "$SWIFTGEN_LOCATION" ];
    then
		mkdir "$SWIFTGEN_LOCATION"
fi
cp -R "templates" "$SWIFTGEN_LOCATION"
